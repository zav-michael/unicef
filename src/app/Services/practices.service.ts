import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { environment } from './../../environments/environment';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import {Club} from './../models/clubs.model'

@Injectable()
export class PracticesService  {
    
    registerClub: Club;



    constructor(private http: Http, private router:Router) {}

    getActionTypes = (options: RequestOptions) => {
        
              return this.http.get(environment.api+`actionTypes`, options)
               .map( (response: Response) => {
                 let commitment = response.json();
                 return commitment;
               });
    }


    savePractice = (titleCl: string, idCommitmentCl: number,DescriptionCl: string, BenefitsCl: string, ChallengesCl: string, extraInfoCl: string,options: RequestOptions) => {
            return this.http.post(environment.api+'practices',
            JSON.stringify({title: titleCl, idCommitment: idCommitmentCl, description: DescriptionCl, benefits: BenefitsCl,
            challenges: ChallengesCl, extraInfo: extraInfoCl }),options)
                .map((response: Response) => {
                    let data = response.json();
    
                    return data;
                });
    
    }


    getClubs = (options: RequestOptions) => {
      
            return this.http.get(environment.api+`clubs`, options)
             .map( (response: Response) => {
               let clubs = response.json();
               return clubs;
             });
      }


    getPracticeByClub = (id:number,options: RequestOptions) => {
      
            return this.http.get(environment.api+'practices?id='+id,options)
             .map( (response: Response) => {
               let list_p = response.json();
               return list_p;
             });
      }


}