import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from './../../environments/environment';

import {User} from './../models/users.model'
import {Club} from './../models/clubs.model'

@Injectable()
export class RegistrationService  {
    registerUser: User;
    registerClub: Club;
    constructor(private http: Http) { }

    // tslint:disable-next-line:max-line-length
    registrationUser = (contact_name: string, role: string, area: string, phoneNumber: string, mobileNumber: string, conditions: number ,
        email: string, Clubs_id: number , Profiles_id: number) => {
        return this.http.post(environment.api+'users',
        JSON.stringify({contact_name: contact_name, role: role, area: area, phoneNumber: phoneNumber, mobileNumber: mobileNumber,
        conditions: conditions, email: email, Clubs_id: Clubs_id, Profiles_id: Profiles_id }))
            .map((response: Response) => {
                let user = response.json();
                user = user.User;
                if (user && user.created) {
                    localStorage.setItem('registerUser', JSON.stringify(user));
                    this.registerUser = user;
                }

                return this.registerUser;
            });

    }

    updateUser = ( user: User, options: RequestOptions ) => {
        return this.http.put(environment.api+'users/update/', JSON.stringify(user))
            .map((response: Response) => {
                let user = response.json();
                user = user.User;
                localStorage.setItem('registerUser', JSON.stringify(user));
                if(user){
                    console.log(user);
                }
                
            })
    }

    registrationClub = (name: string , address: string, city: string, uf: number, cnpj: number, empQuantity: number , website: string,
         file: string) => {
          return this.http.post(environment.api + 'clubs',
       JSON.stringify({ name: name, address: address, city: city, uf: uf, cnpj:
        cnpj, employeesQuantity: empQuantity, website: website , logo: file})).map((response: Response) => {
                const club = response.json();

                const stat = club.status;
                if (stat && club) {
                    localStorage.setItem('currentClub', JSON.stringify(club));

                    this.registerClub = club.Club;
                } else {

                }

                return this.registerClub;
            });
    }

    updateClub = ( club: Club, options: RequestOptions ) => {
        return this.http.put(environment.api+'clubs/index/', JSON.stringify(club))
            .map((response: Response) => {
                let club = response.json();
                localStorage.setItem('currentClub', JSON.stringify(club));
                if(club){
                    console.log(club);
                }
            })
    }

    getClub = (id: number, options: RequestOptions) => {
        return this.http.get(environment.api + `clubs?id=${id}`,
        options).map((response: Response) => {
               const club = response.json();
               const stat = club.status;
               if (stat && club) {
                   localStorage.setItem('currentClub', JSON.stringify(club));

                   this.registerClub = club.Club;
               } else {

               }

               return this.registerClub;
           });
   }

}
