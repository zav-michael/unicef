/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { Monitoring.tsService } from './monitoring.ts.service';

describe('Service: Monitoring.ts', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Monitoring.tsService]
    });
  });

  it('should ...', inject([Monitoring.tsService], (service: Monitoring.tsService) => {
    expect(service).toBeTruthy();
  }));
});