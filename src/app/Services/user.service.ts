import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, RequestOptionsArgs, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from './../../environments/environment';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { User } from './../models/users.model'

@Injectable()
export class UserService {

    constructor(private http: Http) {}

    updateUser = ( user: User ) => {
        return this.http.put(environment.api+'users/update/', JSON.stringify(user))
            .map((response: Response) => {
                let user = response.json();
                user = user.User;
                if(user){
                    console.log(user);
                }
                
            })
    }

    JWT( ){

        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if(currentUser && currentUser.token){
            let headers = new Headers({'Authorization': currentUser.token});      
            return new RequestOptions({headers: headers});
            
        }

    }


    JWTGET(param:any = {}) 
    {
        let result = new RequestOptions();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if(currentUser && currentUser.token){
            let myHeaders = new Headers();
            myHeaders.append('Authorization', currentUser.token);
            result = new RequestOptions({ headers: myHeaders, params: param });
        }
        return result;
    }

    JWTPUT() 
    {
        let result = new RequestOptions();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if(currentUser && currentUser.token){
            let myHeaders = new Headers();
            myHeaders.append('Authorization', currentUser.token);
            myHeaders.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            result = new RequestOptions({ headers: myHeaders });
        }
        return result;
    }

    serialize = function(obj) {
        var str = [];
        for(var p in obj)
          if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
          }
        return str.join("&");
    }

}