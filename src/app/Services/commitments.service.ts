
import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';


@Injectable()
export class CommitmentsService {


  constructor(private http: Http) { }
  
  getCommitments = (options: RequestOptions) => {

      return this.http.get(environment.api+`commitments/all`, options)
       .map( (response: Response) => {
         let commitment = response.json();
         return commitment;
       });
  }



  saveProgress = (commitment:number, options: RequestOptions) => {
    return this.http.post(environment.api+'diagnostics/saveProgress', JSON.stringify({commitment:commitment}) ,options)
    .map( (response) => {
      let result = response.json();
      return result.status;
    });
  }


  
}
