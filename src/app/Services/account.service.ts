import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions} from '@angular/http';
import { environment } from './../../environments/environment';
import { UserService } from './user.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AccountService {

constructor(private http: Http, private user: UserService) {   }  


  content = ( options:RequestOptions ) => {

    return this.http.get(environment.api+'contents',options)
    .map((response: Response) => {
      let result = response.json();  
      return result; 
    })      
    
  }

  contentType = ( options:RequestOptions ) => {

    return this.http.get(environment.api+'contents/contentType',options)
    .map((response: Response) => {
      let result = response.json();  
      return result; 
    })      
    
  }

  putContentType = (body , options:RequestOptions) => {

    return this.http.put(environment.api+'contents/contentType', body, options)
    .map((response: Response) => {
      let result = response.json();
      return result;
    })

  }

  delete = ( options:RequestOptions ) => {

    return this.http.delete(environment.api+'contents',options)
    .map((response: Response) => {
      let result = response.json();  
      return result; 
    })  

  }

  putOrder = ( body, options:RequestOptions ) => {
    
    return this.http.put(environment.api+'contents/order', body ,options)
    .map((response: Response) => {
      let result = response.json();  
      return result; 
    })     
    
  }

  put = (body , options:RequestOptions) => {

    return this.http.put(environment.api+'contents', body, options)
    .map((response: Response) => {
      let result = response.json();
      return result;
    })

  }

  club = ( options:RequestOptions ) => {
    
    return this.http.get(environment.api+'clubs/my', options)
    .map( (response: Response) => {
      return response.json();
    })

  }

}