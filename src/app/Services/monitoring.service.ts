import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class MonitoringService {

    constructor(private http: Http) { }


    getClubs = ( options: RequestOptions ) => {
        
        return this.http.get(environment.api+`clubs`, options)
        .map( (response: Response) => {
    
            return  response.json();
    
        });          
        
    }
    
    commitmentsPagination = ( options: RequestOptions ) => {

        return this.http.get(environment.api+`commitments/pagination`, options)
        .map( (response: Response) => {
    
            return  response.json();
    
        });

    }

    commitmentStep = ( step: number, options: RequestOptions, club:number = 0 ) => {
        
        var getData = `commitments/steps?id=${step}`;
        if(club != 0){
            getData += `&club=${club}`;
        }

        return this.http.get(environment.api+getData, options)
        .map( (response: Response) => {
    
            let step = response.json();
    
            return step;
    
        });
        
    }

    saveProgress = (data:any, options: RequestOptions) => {
        return this.http.post(environment.api+'commitments/saveProgress', JSON.stringify(data) ,options)
        .map( (response: Response) => {

        });
    }

    upload = (data:FormData, options:RequestOptions) => {
      
        return this.http.post(environment.api+'multimedia', data, options)
        .map( (response: Response) => {
            
        })

    }

    getIndicator = (indicator: number, options: RequestOptions) => {
        return this.http.get(environment.api+`commitments/indicator?id=${indicator}`, options)
        .map( (response: Response) => {
    
            let indicatorData = response.json();
    
            return indicatorData;
    
        });
    }

    saveComment = (data:any, options: RequestOptions) => {
        return this.http.post(environment.api+'comments', JSON.stringify(data) ,options)
        .map( (response: Response) => {
            return response.json();
        });
    }

    getComments = ( indicator, options: RequestOptions, club:number = 0 ) => {
        var getData = `comments?indicator=${indicator}`;
        if(club != 0){
            getData += `&club=${club}`;
        }
        return this.http.get(environment.api+getData, options)
        .map( (response: Response) => {    
            return response.json();
        })
    }

    deleteComment = ( id:number, options: RequestOptions ) => {
        return this.http.delete(environment.api+`comments?id=${id}`, options )
        .map( (response: Response) => {
            return response.json();
        })
    }

    getMultimedia = ( indicator, options: RequestOptions, club:number = 0 ) => {
        var getData = `multimedia?indicator=${indicator}`;
        if(club != 0){
            getData += `&club=${club}`;
        }
        return this.http.get(environment.api+getData, options)
        .map( (response: Response) => {    
            return response.json();
        })
    }

    deleteMultimedia = ( id:number, options: RequestOptions ) => {
        return this.http.delete(environment.api+`multimedia?id=${id}`, options )
        .map( (response: Response) => {
            return response.json();
        })
    }
    
    generalProgress = ( options: RequestOptions, club:number = 0 ) => {
        var getData = `commitments/generalProgress`;
        if(club != 0){
            getData += `?club=${club}`;
        }
        return this.http.get(environment.api+getData, options)
        .map( (response: Response) => {
            return response.json();
        })
    }

}