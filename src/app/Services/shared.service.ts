import {Injectable} from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import { Observable } from "rxjs/Observable";
import { User } from './../models/users.model';
import { Pagination } from './../models/pagination.model';

@Injectable()

export class SharedService {



    private closeLogin = new BehaviorSubject<boolean>(false);
    private closeRestore = new BehaviorSubject<boolean>(false);
    private collapseSource = new BehaviorSubject<boolean>(true);
    private userSource = new BehaviorSubject<User>( new User() ) ;
    private loginSource = new BehaviorSubject<boolean>(false);
    private sliderHomeSource =  new BehaviorSubject<any[]>([]);
    private logoSource = new BehaviorSubject<any[]>([]);
    private paginationSource = new BehaviorSubject<Pagination>( new Pagination() ) ;


    currentCloseLogin = this.closeLogin.asObservable();
    currentcloseRestore = this.closeRestore.asObservable();
    currentUser = this.userSource.asObservable();
    currentCollapse = this.collapseSource.asObservable();
    currentLogin = this.loginSource.asObservable();
    currentSliderHome = this.sliderHomeSource.asObservable();
    currentLogo = this.logoSource.asObservable();
    currentPagination = this.paginationSource.asObservable();

    constructor(){  }


    handleCloseLogin(state : boolean){
        this.closeLogin.next(state);

    }
    handleCloseRestore(state : boolean){
        this.closeRestore.next(state);

    }

    handleSliderHome( content : any[] ){
        this.sliderHomeSource.next(content);
    }

    handleCollapse( collapse: boolean){
        this.collapseSource.next(collapse);
    }

    handleUser( user: User ){
        this.userSource.next(user);
    }

    handleLogos( content: any[] ) {
        this.logoSource.next(content)
    }

    handleLogin( loggedIn: boolean ){
        this.loginSource.next(loggedIn);
    }

    handlePagination( content: Pagination ){
        this.paginationSource.next(content);
    }


   

    
}