import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import { environment } from './../../environments/environment';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { SharedService } from './shared.service'

import {User} from './../models/users.model';
import {ContactForm} from './../models/contactForm.model';

@Injectable()
export class AuthenticationService {
    loggedUser: User;
    constructor(private http: Http, private router:Router, private sharedService: SharedService) { }

    login = (email: string, password: string) => {

        return this.http.post(environment.api + 'users/login/', JSON.stringify({email: email, password: password}))
            .map((response: Response) => {
                let user = response.json();
                user = user.User;
                if (user && user.token) {
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.loggedUser = user;
                }

                return this.loggedUser;
            });

    }

    restore = (email:string) => {
        return this.http.post(environment.api+'users/restore', JSON.stringify({email: email}))
            .map((response: Response) => {
                let result = response.json();
                
                if(result && result.status){
                    return result;
                }
                return false;
            });
    }

    verifyRestoreToken = ( token: string ) => {
        
        return this.http.get(environment.api+'users/verifyrestore?token='+token)
            .map((response) => {
                let result = response.json();
                
                if(result && result.status){
                    return result.User;
                }
                
                return new User();

            })

    }

    updateUser = ( user: number, password:string, verifypassword: string, passwordtoken: string ) => {

        return this.http.put(environment.api+'users/updatetokenpassword/', JSON.stringify({
            user: user, password:password,verifypassword: verifypassword, restoretoken: passwordtoken
        })).map((response: Response) => {
            let result = response.json();
            return result;
        });

    }

    createPasss = ( user: number, password:string, verifypassword: string, passwordtoken: string ) => {
        
                return this.http.put(environment.api+'users/updatetokenpassword/', JSON.stringify({
                    user: user, password:password,verifypassword: verifypassword, restoretoken: passwordtoken
                })).map((response: Response) => {
                    let result = response.json();
                    return result;
                });
        
            }

    contactForm = ( contact: ContactForm ) => {
        
        return this.http.post(environment.api+'contactForm', JSON.stringify(contact))
            .map( (response: Response) => {
                let result = response.json();
                if(result && result.ContactForm){
                    return result.ContactForm;
                }
                return new ContactForm();
            })

    }

    logout = () => {
        localStorage.removeItem('currentUser');
        this.sharedService.handleLogin( false );
        this.router.navigate(['/']);
    }
}