import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';


@Injectable()
export class AutodiagnosticService {


  constructor(private http: Http) { }
  
  getStep = ( step: number, options: RequestOptions ) => {

    return this.http.get(environment.api+`diagnostics/steps?id=${step}`, options)
    .map( (response: Response) => {

        let step = response.json();

        return step;

    });

  }


  getPaso = ( options: RequestOptions ) => {
    
        return this.http.get(environment.api+`commitments`, options)
        .map( (response: Response) => {
    
            let paso = response.json();
    
            return paso;
    
        });
    
  }

  get = ( options: RequestOptions ) => {
    
        return this.http.get(environment.api+`sections/all`, options)
        .map( (response: Response) => {
    
            let step = response.json();
    
            return step;
    
        });
    
    }

    getRecommend = ( options: RequestOptions ) => {
      
          return this.http.get(environment.api+`recommendations`, options)
          .map( (response: Response) => {
      
              let step = response.json();
      
              return step;
      
          });
      
    }

    getRecommendStep = ( step: number, options: RequestOptions ) => {
      
          return this.http.get(environment.api+`recommendations/steps?id=${step}`, options)
          .map( (response: Response) => {
      
              let step = response.json();
      
              return step;
      
          });
      
    }
    getRecommendStepWithTitle = ( step: number, options: RequestOptions ) => {
      
          return this.http.get(environment.api+`recommendations/stepsWithTitle?id=${step}`, options)
          .map( (response: Response) => {
      
              let step = response.json();
      
              return step;
      
          });
      
    }

  
    allPut = (params , options: RequestOptions) => {

      return this.http.put(environment.api+'/questions/all/', params, options)
      .map((response: Response) => {
        let result = response.json();
        return result;
      })

    }

  post = (params , goal: string , options: RequestOptions) => {
      
      return this.http.post(environment.api+ goal , params, options)
      .map((response: Response) => {
        let result = response.json();
        return result;
      });      
  }

  put = (params , goal: string , options: RequestOptions) => {
      
      return this.http.put(environment.api+ goal , params, options)
      .map((response: Response) => {
        let result = response.json();
        return result;
      });      
   }



  saveAnswer = ( answer: any, options: RequestOptions ) => {

    return this.http.post(environment.api+'diagnostics/saveAnswer', JSON.stringify(answer) ,options)
    .map( (response) => {

      //let result = response?response.json():{};

    });

  }

  saveProgress = (step:number, options: RequestOptions) => {
    return this.http.post(environment.api+'diagnostics/saveProgress', JSON.stringify({step:step}) ,options)
    .map( (response) => {
      
      let result = response.json();
      return result.status;

    });
  }

  currentStep = (options: RequestOptions) => {
    return this.http.get(environment.api+`diagnostics/currentStep`, options)
    .map( (response:Response) => {
        return response.json();
    })
  }

  graphicData = (options: RequestOptions) => {
    return this.http.get(environment.api+`diagnostics/graphic`, options)
    .map( response => {
        return response.json();
    });
  }

  graphicData2 = (id: number, options: RequestOptions) => {
    return this.http.get(environment.api+`diagnostics/graphicSingle?id=${id}`, options)
    .map( response => {
        return response.json();
    });
  }
  
}
