import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import { environment } from './../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import {NgbModal, ModalDismissReasons, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {Banners} from './../models/banners.model';
 
@Injectable()

export class ModalService {
  
    constructor(private modalService: NgbModal, private http: Http) {
        
    }

    uploadBanner = (data: any) => {

      return this.http.post(environment.api+'/content/', JSON.stringify(data))
      .map((response: Response) => {
         
          return "";
      })

    }

    open(content, params: NgbModalOptions = {}) {
        this.modalService.open(content, params).result.then((result) => {
         
        }, (reason) => {
         
        });
      }
    
      private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
          return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
          return 'by clicking on a backdrop';
        } else {
          return  `with: ${reason}`;
        }
      }
 
}