import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class ContentService {


  constructor(private http: Http) { }

  getContent= (Slug: string, Param: string, Name: string) => {

    var url = environment.api + `contents/?slug=${Slug}&custom[${Param}]=${Name}`;
    return this.http.get(url)
         .map((response => response.json()));

  }

  get = (Slug: string) => {
    
     var url = environment.api + `contents/?slug=${Slug}&order=DESC`;
     return this.http.get(url).map((response => response.json()));
    
  }


 

  getTaxonomy = ( options:RequestOptions ) => {

    return this.http.get(environment.api+'taxonomy',options)
    .map((response: Response) => {
      return response.json();
    });      

  }


  getUri = ( uri: string, options:RequestOptions ) => {
    
        return this.http.get(environment.api+uri,options)
        .map((response: Response) => {
          return response.json();
        });      
    
   }


  sendContent = ( request: string, formData: any ) => {
    
    return this.http.post(environment.api+request, formData).map((response: Response) => {
      return response.json();
    });

   }


   saveFields = ( request: string, formData: any ) => {
    
    return this.http.post(environment.api+request, formData).map((response: Response) => {
      return response.json();
    });

   }



}
