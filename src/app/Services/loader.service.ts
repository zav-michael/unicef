import { Injectable } from '@angular/core';
import { LoaderComponent } from './../components/loader/loader.component';

@Injectable()
export class LoaderService  {
    
    private loaderCache = new Set<LoaderComponent>();
    constructor() { }

    _register(spinner: LoaderComponent): void {
        this.loaderCache.add(spinner);
      }

      show(spinnerName: string): void {
        this.loaderCache.forEach(spinner => {
          if (spinner.name === spinnerName) {
            spinner.show = true;
          }
        });
      }
    
      hide(spinnerName: string): void {
        this.loaderCache.forEach(spinner => {
          if (spinner.name === spinnerName) {
            spinner.show = false;
          }
        });
      }




}
