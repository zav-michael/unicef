import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions} from '@angular/http';
import { environment } from './../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
const URL = environment.api+'/contents/';

@Injectable()
export class FileUploaderService {

formData = new FormData();

constructor(private http: Http) {   }  

  upload = ( formData: any ) => {
    return this.http.post(URL, formData).map((response: Response) => {
      return response.json();
    });
  }

  updateUpload = ( formData: any ) => {
    return this.http.post(URL+'updateSlider', formData).map((response: Response) => {
      return response.json();
    });
  }

  updateUploadR = (  urlDe: string,  formData: any ) => {
    return this.http.post(environment.api+urlDe, formData).map((response: Response) => {
      return response.json();
    });
  }


  private errorHandler(error: Response){
    console.error(error+" Esto")
    return Observable.throw(error || "Some error");
  }


}