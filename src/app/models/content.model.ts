export class Content {
    id: number;
    title: string;
    body: string;
    created: string;
    password: string;
    modified: string;
    content_type_id: number;
    fields: any [] = [];
}
