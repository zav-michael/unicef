export class Practice {
    title: string;
    idCommitment: number;
    idAction: number;
    Description : string;
    Benefits: string;
    Challenges: string;
    extraInfo: string;
}