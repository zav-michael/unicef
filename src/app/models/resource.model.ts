export class Resource {
    title: string;
    tags: string;
    file: any;
    constructor(){
    }
}
export class ResourceRequired {
    title: boolean;
    tags: boolean;
    file: boolean;
    constructor(){
        this.title = true;
        this.tags = true;
        this.file = true;
    }
}