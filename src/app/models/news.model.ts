export class User {
    id: number;
    contact_name: string;
    role: string;
    email: string;
    password: string;
    area: string;
    created: number;
    phoneNumber: number;
    mobileNumber: number;
}