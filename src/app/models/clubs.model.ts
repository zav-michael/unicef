export class Club {
    id: number;
    logo: string;
    name: string;
    address: string;
    city : string;
    uf: number;
    cnpj: number;
    employeesQuantity: number;
    website: string;
}