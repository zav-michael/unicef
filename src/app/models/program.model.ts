export class Program {
    title: string;
    value: string;
    constructor(){
    }
}
export class ProgramRequired {
    title: boolean;
    value: boolean;
    constructor(){
        this.title = true;
        this.value = true;
    }
}