export class Pagination {
    totalItems: number;
    maxSize: number;
    items: Array<Page>;
}
export class Page {
    id : any;
    name: any;
    title: any;
    steps_id?: string;
}