export class Banners {
    title: string;
    body: string;
    image1: any;
    image2: any;
    idImage1:number;
    idImage2:number;     
    contentId: number;
    constructor(){
        this.idImage1 = 0;
        this.idImage2 = 0;
    }
}
export class BannersRequired {
    title: boolean;
    body: boolean;
    image1: boolean;
    image2: boolean;
    constructor(){
        this.title = true;
        this.body = true;
        this.image1 = true;
        this.image2 = true;
    }
}
export class BannersO {
    title: string;
    body: string;
    id:number;
    contentId: number;
    idImage1:number;
    idImage2:number;
    constructor(){

    }
}
export class Noticia {
    title: string;
    body: string;
    encontro: string;
    link: string;
    id:number;
    contentId: number;
    idImage1:number;
    constructor(){

    }
}
export class NoticiaRequired {
    title: boolean;
    body: boolean;
    encontro: boolean;
    link: boolean;
    image1: boolean;
    constructor(){
        this.title = true;
        this.body = true;
        this.encontro = false;
        this.link = false;
        this.image1 = true;
    }
}
