export class ContactForm {
    id: number;
    club: string;
    contact: string;
    role : string;
    phone: string;
    email: string;
    message: string;
}