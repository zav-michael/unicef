import { Content } from './../account/inc/content/a-bank/a-bank.component';
import { Component, OnInit } from '@angular/core';
import {ContentService} from './../../Services/content.service';
import { UserService } from "./../../Services/user.service";
import { Pagination, Page } from './../../models/pagination.model';
import { PagerService } from './../../Services/pager.service';
import * as _ from 'underscore';
declare var jquery:any;
declare var $ :any;


@Component({
  selector: 'app-center-resources',
  templateUrl: './center-resources.component.html',
  styleUrls: ['./center-resources.component.css'],
  providers: [ UserService]
})
export class CenterResourcesComponent implements OnInit {


  public loading: boolean=true;
  title: string;
  titleFiles: string;
  body: string;
  boxes: any[] = [];
  cont: any[] = [];
  contentData: Array<{term: string, content: any[]}> = [];

  compName = 'crecursos';
  compFileView = true;
  filtered;
  pager: any = {};
  pagedItems: any[];
  content =false;
  indexBox = null;

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
        return;
    }
  
    // get pager object from service
    this.pager = this.pagerService.getPager(this.contentData[0].content.length, page);
  
    // get current page of items
    this.pagedItems = this.contentData[0].content.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
  //sorting
  key: string = 'DIREITOS DA CRIANÇAS E DO ADOLESCENTE';
  reverse: boolean = false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }
  p: number = 1;
  
  


  constructor(private contentService: ContentService,  private userService: UserService, private pagerService: PagerService) { }


  ngAfterViewInit(){
  }



  toggleTitle()
  {

    $(".sidebar-menu > li.have-children a").on("click", function(i){
      i.preventDefault();
    if( ! $(this).parent().hasClass("active") ){
     // $(".sidebar-menu li ul").slideUp();
      $(this).next().slideToggle();
      $(".sidebar-menu li").removeClass("active");
      $(this).parent().addClass("active");
    }
    else{
      $(this).next().slideToggle();
      $(".sidebar-menu li").removeClass("active");

      }
    });


  }
  ngOnInit() {
    window.scrollTo(0,0);

    this.menuAnim();
    this.contentService.getTaxonomy(this.userService.JWTGET()).subscribe( data => {
      this.title = data.name;
      this.body = data.description;
      this.boxes  = data.taxonomy;
      console.log("JWGET "+ data.taxonomy );
      this.loading = false;
      //Carga todo el contenido por defecto paginado para usar en la busqueda
      this.openAllContent(); 
    });

  }

  menuAnim()
  {
  }


  openContent(idRes: string)
  {
    var find = false;
    this.content =true;
    this.indexBox=null;

    // alert(idRes);

    for(var i=0;i<this.boxes.length;i++)
    {
      if(this.boxes[i].id == idRes )
      {
        this.compFileView = true;
        find=true;
        this.titleFiles = this.boxes[i].name; 
        this.contentData.push({term:this.boxes[i].term,content:this.boxes[i].content});
        this.contentData[0].term =this.boxes[i].term;
        this.contentData[0].content=this.boxes[i].content;
        var aa = JSON.stringify(this.contentData[0]);
        console.log("SDDS=="+aa); 

        if( this.contentData[0].content.length> 0)
        {
          this.setPage(1);
        }
        else{
          this.compFileView = false;
        }
     
      }
    }
  }



  openContent2(idRes: string)
  {
    var find = false;
    this.content =true;
    this.indexBox = null;

    //alert(idRes);

    for(var i=0;i<this.boxes.length;i++)
    {
      if(this.boxes[i].id == idRes )
      {
        this.compFileView = true;
        this.indexBox = i;
        find=true;
        this.titleFiles = this.boxes[i].name; 
        this.contentData.push({term:this.boxes[i].term,content:this.boxes[i].content});
        this.contentData[0].term =this.boxes[i].term;
        this.contentData[0].content=this.boxes[i].content;
        var aa = JSON.stringify(this.contentData[0]);
        console.log("SDDS=="+aa); 

        if( this.contentData[0].content.length> 0)
        {
          this.setPage(1);
        }
        else{
          this.compFileView = false;
        }
      }
    }

    setTimeout(() => {
      this.toggleTitle();
    
    }, 2000);

    

  }



  openContentByTab(e,i)
  {

   //alert("--"+e+"--"+i);
    var find = false;
    this.cont = [];
    console.log(i);
    console.log(e);
    for(var num=0;num<this.boxes[i].content.length;num++)
    { console.log("Uno "+ num);
       if(this.boxes[i].content[num].terms_id == e){
         console.log("se evcdc"); 
         find=true;
         this.compFileView = true;
         this.cont.push(this.boxes[i].content[num]);   
      }
    }
    
     if(this.cont.length  >0) { 
      this.contentData.push({term:"",content:[]});
      this.contentData[0].term ="tem--def";
      this.contentData[0].content = this.cont;
      this.setPage(1);}
      else{
       // alert("vacio"); 
        this.compFileView = false;
      
      }

      var aa = JSON.stringify(this.cont);
      console.log("SAAA=="+aa); 
    
     // alert("--*[["+ this.compFileView);
  }

  openAllContent()
  {
    var find = false;
    // alert(idRes);
    let myTerms = new Array();
    
    for(var i=0;i<this.boxes.length;i++){
      this.compFileView = true;
      find=true;

      for(var t in this.boxes[i].term){
        myTerms[this.boxes[i].term[t].id] = {'parent' : this.boxes[i].name, 'my_self' : this.boxes[i].term[t].name};
      }

      for(var c in this.boxes[i].content){
        this.boxes[i].content[c].term = myTerms[this.boxes[i].content[c].terms_id];
      }

      if(this.contentData[0] != undefined){
        this.contentData[0].term =(this.contentData[0].term).concat(this.boxes[i].term);
        this.contentData[0].content=(this.contentData[0].content).concat(this.boxes[i].content);
      }else{
        this.contentData.push({term:this.boxes[i].term,content:this.boxes[i].content});
        //this.contentData[0].term =this.boxes[i].term;
        //this.contentData[0].content=this.boxes[i].content;
      }
      var aa = JSON.stringify(this.contentData[0]);
      console.log("SDDS=="+aa); 

    }
    if( this.contentData[0].content.length> 0){
      this.setPage(1);
    }
  }

  goBack(){
    this.content = false;
  }

}
