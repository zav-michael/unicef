import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CenterResourcesComponent } from './center-resources.component';

describe('CenterResourcesComponent', () => {
  let component: CenterResourcesComponent;
  let fixture: ComponentFixture<CenterResourcesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CenterResourcesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CenterResourcesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
