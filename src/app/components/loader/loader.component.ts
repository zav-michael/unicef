import { LoaderService } from './../../Services/loader.service';
import { Component, OnInit, Input } from '@angular/core';



@Component({
  selector: 'app-loader',
  template: `
  <a>Cragadndpod</a>
`,
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {


  @Input() name: string;
  @Input() loadingImage: string;
  @Input() show = false


  constructor(private loaderService: LoaderService) { }

  ngOnInit() {
    if (!this.name) throw new Error("EL cargador debe tener un nombre .");
    this.loaderService._register(this);
  }



  ngOnDestroy(): void {
    
  }


  

}
