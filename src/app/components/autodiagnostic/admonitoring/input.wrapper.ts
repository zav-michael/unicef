import { Component } from '@angular/core';
import { FieldType } from 'ng-formly';

@Component({
  selector: 'formly-field-input',
  template: `
    <div class="cuadro-general" [formGroup]="form">
        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 datos-internos">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 dato-izquierdo">
                <p class="parrafo color-grisoscuro">{{to.label}}</p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 dato-centro">
                <p class="indicador-anterior color-azul-basico">
                    
                </p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 dato-derecha">
                <input [name]="id" [type]="type()" [formControl]="formControl" [formlyAttributes]="field" class="form-control">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 acciones">
            <div class="icono-estado">
                <img src="assets/images/icono-aprobado.svg" alt="">
            </div>
            <p class="anuncio color-azul-basico">Arquivos</p>
            <div class="desplgable">
                <a href="" data-toggle="modal" data-target="#adicionar"><span class="flecha"><svg class="flecha-abajo" viewBox="0 0 4 8"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="assets/images/sprites.svg#flecha-derecha"></use></svg></span></a>
            </div>
        </div>
    </div>
    `,
})
export class FormlyFieldInputMonitoring extends FieldType {

    

  type(): string {
    return this.to.type || 'text';
  }
  
  get inputClass(): string {
    return this.to.inputClass || '';
  }
}