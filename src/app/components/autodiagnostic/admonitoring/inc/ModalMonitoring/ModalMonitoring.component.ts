import { Component, OnInit, Input } from '@angular/core';
import { MonitoringModel } from './monitoringModel';
import { UserService } from "./../../../../../Services/user.service";
import { MonitoringService } from './../../../../../Services/monitoring.service';
import { AlertService } from './../../../../../Services/alert.service';

@Component({
  selector: 'modalmonitoring',
  templateUrl: './ModalMonitoring.component.html',
  styleUrls: ['./ModalMonitoring.component.css'],
  providers: [UserService, MonitoringService]
})
export class ModalMonitoringComponent implements OnInit {
  @Input() promise;
  @Input() promiseTitle;
  @Input() numeral;
  monitoringModel:MonitoringModel;
  _idClub: number;
  _indicator: any;
  _comments:any;
  _selectedImage:any;
  _selectedVideo:any;
  loading:boolean;
  _multimedia:any = {
    images: undefined,
    videos: undefined,
    documents: undefined
  };
  

  constructor(
    private user: UserService,
    private alertService: AlertService,
    private monitoring: MonitoringService
  ){
    this.monitoringModel = new MonitoringModel();
   }
  
  ngOnInit() {
  }

  upload = ( e ) => {
    //Lanzar loader
    this.loading=true;
    var file:File = e.target.files[0];
    let formData:FormData = new FormData();
    formData.append('file', file);
    formData.append('indicators_id', this._indicator);

    this.monitoring.upload(formData, this.user.JWT())
    .subscribe(
      response => {
       //Parar loader
       this.loading=false;
       this.loadMultimedia();
       console.log(response);
       //Mensaje carga exitosa
       this.alertService.success('Archivo cargado exitosamente');
    },
    error => {
      console.log(error);
      // alert(error);
      let errorR = JSON.parse(error._body);
      this.alertService.error(errorR.error.message);
      this.loading = false;
    });
  
  }

  addComment = ( comment ) => {
      
    this.monitoring.saveComment({comment: comment, indicator_id: this._indicator}, this.user.JWT())
    .subscribe(
      response => {
        this.loadComments();
      }
    )

  }

  loadComments = () => {
    if(this._indicator){
      this.monitoring.getComments(this._indicator, this.user.JWTGET(), this._idClub)
      .subscribe(
        response => {
          this._comments = response.Comments;
        }
      );
    }
  }

  loadMultimedia = () => {
    if(this._indicator){
      this.monitoring.getMultimedia(this._indicator, this.user.JWTGET(), this._idClub)
      .subscribe(
        response => {
          this._multimedia = response.Multimedia;
          this._selectedImage = undefined;
          this._selectedVideo = undefined;
        }
      );
    }
  }

  processsImage = ( input ) => {
      this.monitoring.getMultimedia(this._indicator, this.user.JWTGET())
      .subscribe(
        response => {
          
        }
      )
  }

  resetZone = () => {
    this._selectedImage = undefined;
    this._selectedVideo = undefined;
  }

  handleSelectedImage = (image:any) => {
    this._selectedImage = image;
  }

  handleSelectedVideo = (video:any) => {
    this._selectedVideo = video;
   
  }

  deleteImage = (id:number) => {

      this.monitoring.deleteMultimedia(id, this.user.JWTGET())
      .subscribe( response => {
        console.log(response);
        //Mensaje eliminacion correcta
        this.alertService.success('Archivo eliminado exitosamente');
        this.loadMultimedia();
      })

  }

  deleteComment = (id:number) => {

    this.monitoring.deleteComment(id, this.user.JWTGET())
    .subscribe( response => {
      this.loadComments();
    })

}

  loadIndicator = () => {
    
  }

  @Input()
  set indicator(indicator: number){
    this.loadIndicator();
    this._indicator = indicator;
    this.loadMultimedia();
    this.loadComments();
  }
  @Input() set club(club: number)  
  {
    this._idClub = club;
    this.loadIndicator();
    this.loadMultimedia();
    this.loadComments();
  }

}
