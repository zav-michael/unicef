import { Component, ViewContainerRef, ViewChild } from '@angular/core';
import { FieldWrapper } from 'ng-formly';
@Component({
  selector: 'formly-wrapper-panel',
  template: `
    <div class="bloque-secciones">
    
        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 titulos-superiores">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <p class="titulo-accion">{{to.title}}</p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                <p class="titulo-anterior">Ano<br>anterior</p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                <p class="titulo-actual">Ano<br>atual</p>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 fondo-divisor"></div>

        <ng-template #fieldComponent></ng-template>

    </div>
  `,
})
export class FormlyPanelSectionWrapper extends FieldWrapper {
  @ViewChild('fieldComponent', {read: ViewContainerRef}) fieldComponent: ViewContainerRef;
  
  index:number;

  getIndex = (field:any)  => {
    let indexArr = field.id.split('_');
    this.index = indexArr[1];
    return this.index;
  }
}
