import { AutodiagnosticComponent as Autodiagnostic } from './autodiagnostic.component';
import { AdhomeComponent as AdHome } from './adhome/adhome.component';
import { AdmenuComponent as Admenu } from './admenu/admenu.component';
import { AdMonitoringComponent as AdMonitoring } from './admonitoring/admonitoring.component';
import { AdRecomendationsComponent as AdRecommedations } from './adrecomendations/adrecomendations.component'
import { AdResultsComponent as AdResults } from './adresults/adresults.component';

export { AdResults, AdRecommedations, AdMonitoring, Admenu, AdHome, Autodiagnostic};