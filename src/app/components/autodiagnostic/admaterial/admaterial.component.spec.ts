import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmaterialComponent } from './admaterial.component';

describe('AdmaterialComponent', () => {
  let component: AdmaterialComponent;
  let fixture: ComponentFixture<AdmaterialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmaterialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
