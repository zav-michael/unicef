import { Component, OnInit } from '@angular/core';
import {ContentService} from './../../../Services/content.service';

import { PagerService } from './../../../Services/pager.service';

@Component({
  selector: 'app-admaterial',
  templateUrl: './admaterial.component.html',
  styleUrls: ['./admaterial.component.css']
})
export class AdmaterialComponent implements OnInit {


  pager: any = {};
  pagedItems: any[];
  files: any[] = [];
  compName = 'material_apoyo';

  constructor(private contentService: ContentService,private pagerService: PagerService) { 


    this.contentService.getContent(this.compName, 'type', 'pdf').subscribe( data => {
      // tslint:disable-next-line:forin
      for (const key$ in data.Contents[0].fields) {
         this.files.push(data.Contents[0].fields[key$]);
      }

      this.setPage(1);
   });


  }



setPage(page: number) {
  if (page < 1 || page > this.pager.totalPages) {
      return;
  }

  // get pager object from service
  this.pager = this.pagerService.getPager(this.files.length, page);

  // get current page of items
  this.pagedItems = this.files.slice(this.pager.startIndex, this.pager.endIndex + 1);
}





  ngOnInit() {
    window.scrollTo(0,0);
  }

}
