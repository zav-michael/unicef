import { Component, OnInit } from '@angular/core';
import { UserService } from './../../../Services/user.service'
import { AuthenticationService } from './../../../Services/authentication.service'
import { AccountService } from './../../../Services/account.service'

@Component({
  selector: 'app-admenu',
  templateUrl: './admenu.component.html',
  styleUrls: ['./admenu.component.css'],
  providers: [UserService, AccountService, AuthenticationService]
})
export class AdmenuComponent implements OnInit {

  club:any;

  constructor(
    private user: UserService,
    private account: AccountService,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
  
    this.account.club(this.user.JWTGET())
    .subscribe( response => {
      this.club = response.Club
    });

  }

  logout = () => {
    
    this.authenticationService.logout();

  }

}
