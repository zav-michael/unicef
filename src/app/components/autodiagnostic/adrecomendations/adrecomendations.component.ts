import { Component, OnInit, ElementRef } from '@angular/core';
import { AutodiagnosticService } from './../../../Services/autodiagnostic.service';
import { UserService } from './../../../Services/user.service';
import { SharedService } from './../../../Services/shared.service';
import { Pagination, Page } from './../../../models/pagination.model';
import { environment } from './../../../../environments/environment';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, Response, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-adrecomendations',
  templateUrl: './adrecomendations.component.html',
  styleUrls: ['./adrecomendations.component.css'],
  providers: [UserService,SharedService, AutodiagnosticService, Pagination]
})
export class AdRecomendationsComponent implements OnInit {
  
  section: any;
  step:any;
  pagination: Pagination;
  autodiagnostic:Array<any>;
  currentPage:number = 1;
  title:string;
  api: string;
  loading: boolean = false;
  empty: boolean = true;

  constructor(
    private user: UserService,
    private sharedService:SharedService, private autodiagnosticService: AutodiagnosticService
  ) {
    this.pagination = new Pagination();
    this.pagination.totalItems = 1;
    this.pagination.maxSize = 10;
    this.autodiagnostic = []
    this.sharedService.handlePagination(this.pagination);
    this.api = environment.api;
  }

  ngOnInit() {
    
    this.autodiagnosticService.getRecommend(this.user.JWTGET())
        .subscribe(
          response => {
            this.pagination.totalItems = response.questions.length * this.pagination.maxSize;
            this.pagination.items = response.questions;
            this.sharedService.handlePagination(this.pagination);
            this.render();
          }
        );

  }

  render = () => {
    this.loading = !this.loading;
    this.autodiagnosticService.getRecommendStepWithTitle(this.currentPage + 1, this.user.JWTGET())
    .subscribe(
      response => {
        this.section = response.Recommendations.recommendations;
        this.title = response.Recommendations.section.name;
        if(response.Recommendations.recommendations.length > 0 && this.currentPage + 1 == 2){
            this.empty = false;
        }
        this.loading = !this.loading;
      }
    )
  }

  public pageChanged(event:any):void {
    this.render();
  }

}
