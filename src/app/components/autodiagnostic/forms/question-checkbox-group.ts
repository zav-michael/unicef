import { QuestionBase } from './question-base';

export class CheckboxGroup extends QuestionBase<string> {
  controlType = '';
  options: {key: string, value: string}[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || [];
  }
}