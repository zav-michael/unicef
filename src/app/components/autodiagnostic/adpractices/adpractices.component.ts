import { Component, OnInit } from '@angular/core';
import { CommitmentsService } from './../../../Services/commitments.service';
import { UserService } from "./../../../Services/user.service";
import { PracticesService } from "./../../../Services/practices.service";
import { AlertService } from './../../../Services/alert.service';
import { Practice } from  './../../../models/practice.model';
import { FilterPipeModule } from 'ngx-filter-pipe';

@Component({
  selector: 'app-adpractices',
  templateUrl: './adpractices.component.html',
  styleUrls: ['./adpractices.component.css'],
  providers: [CommitmentsService, UserService,PracticesService]
})


export class AdpracticesComponent implements OnInit {


  model: any = {};
  comp = 0;
  commitments: any = [];
  actionType: any = [];
  clubs: any = [];
  comm_list: any = [];
  loading: boolean = false;

  clubsFilter: any = {city:''};

  constructor(private commitmentsService: CommitmentsService, private user: UserService ,private practices: PracticesService ,private alertService: AlertService) { }

  ngOnInit() {
    window.scrollTo(0,0);
    this.commitmentsService.getCommitments(this.user.JWTGET()).subscribe( data => {
      console.log("JWGET "+ data );
      var aa = JSON.stringify(data.Commitments);
      this.commitments = data.Commitments;
    });
      
    this.practices.getClubs(this.user.JWTGET()).subscribe( data => {
      console.log("JWGET "+ data );
      var aa = JSON.stringify(data.Clubs);
      this.clubs = data.Clubs;
    });

    this.loading = true;

    this.practices.getActionTypes(this.user.JWTGET()).subscribe( data => {
      console.log("JWGET "+ data );
      var aa = JSON.stringify(data.Action_types); 
      this.actionType = data.Action_types;   
      this.loading = false;
    });

  }



  savePractice = (titleCl: string, idCommitmentCl: number,  DescriptionCl: string, BenefitsCl: string, ChallengesCl: string, extraInfoCl: string) => 
  {

    //(this.model.idCommitmentCl  );  
  //  alert(this.model.idActionCl  );  

  this.loading = true;

       this.practices.savePractice(this.model.titleCl,this.model.idCommitmentCl,this.model.DescriptionCl,this.model.BenefitsCl,this.model.ChallengesCl,this.model.extraInfoCl,this.user.JWT()).subscribe(
        data => {
          var aa = JSON.stringify(data.Commitments);
         console.log("Respuesta Guardar  =="+aa);  

         this.alertService.success("Armazenada corretamente");
         this.model.titleCl = '';
         this.model.DescriptionCl = '';
         this.model.BenefitsCl = '';
         this.model.ChallengesCl = '';
         this.model.extraInfoCl = '';
         this.comp = 0;

         this.loading = false;
         },
         error => {
          console.log(error);
          this.alertService.error("Erro, tente novamente")    
          
          this.loading = false;
         }
       );

     }


  visibility1() {
    if(this.comp != 1)
      this.comp = 1;
    else
      this.comp = 0;
  }

  visibility() {
    if(this.comp != 2)
      this.comp = 2;
    else
      this.comp = 0;
  }


  onChange(deviceValue) {
    //this.comp = true;
   
    console.log(deviceValue);

    this.practices.getPracticeByClub(deviceValue,this.user.JWTGET()).subscribe( data => {
      console.log("JWGET "+ data );
      var aa = JSON.stringify(data.Practice);
      this.comm_list = data.Practice;
      console.log("practica =="+aa);    
    });


   }

  
}
