import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdpracticesComponent } from './adpractices.component';

describe('AdpracticesComponent', () => {
  let component: AdpracticesComponent;
  let fixture: ComponentFixture<AdpracticesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdpracticesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdpracticesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
