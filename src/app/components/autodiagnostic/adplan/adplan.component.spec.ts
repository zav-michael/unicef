import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdplanComponent } from './adplan.component';

describe('AdplanComponent', () => {
  let component: AdplanComponent;
  let fixture: ComponentFixture<AdplanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdplanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdplanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
