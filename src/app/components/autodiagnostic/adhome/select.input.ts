import { Component } from '@angular/core';
import { FieldType } from 'ng-formly';

@Component({
  selector: 'formly-field-input',
  template: `
    <div [formGroup]="form" >
        <p class="parrafo color-azulprofundo izquierdo col-xs-12 col-sm-12 col-md-7 col-lg-7">
            {{to.label}}
        </p>
    
        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">

            <select [name]="id" [formControl]="formControl"  [formlyAttributes]="field" class="form-control">
                <option *ngFor="let option of to.options" [value]="option.value">{{option.label}}</option>
            </select>

        </div>
    </div>
    `,
})
export class FormlyFieldSelect extends FieldType {
  get type(): string {
    return this.to.type || 'select';
  }
  
  get inputClass(): string {
    return this.to.inputClass || '';
  }
}