import { Component } from '@angular/core';
import { FieldType } from 'ng-formly';

@Component({
  selector: 'formly-field-input',
  template: `
 
    <ul [formGroup]="form" class="opciones">
        <div class="pieza">
            <li *ngFor="let option of to.options" class="row">
                <div class="col-xs-10">
                    <span class="parrafo color-negro"><label class="custom-control custom-radio">{{ option.value }}</label></span>
                </div>
                <div class="col-xs-2">
                    <input [name]="id" type="radio" [value]="option.key" [formControl]="formControl"
                    [formlyAttributes]="field" class="custom-control-input">
                </div>
            </li>
        </div>
        
    </ul>

    `,
})
export class FormlyFieldRadio extends FieldType {
  get type(): string {
    return this.to.type || 'radio';
  }
  
  get inputClass(): string {
    return this.to.inputClass || '';
  }
}