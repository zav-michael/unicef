import { Component, ViewContainerRef, ViewChild } from '@angular/core';
import { FieldWrapper } from 'ng-formly';
@Component({
  selector: 'formly-wrapper-question',
  template: `
    <div class="grupo">
      <div class="pregunta pieza">
        <span class="numeral">{{getIndex(field)}}</span>  
        <template #fieldComponent></template>
      </div>
    </div>
    
  `,
})
export class FormlyQuestionWrapper extends FieldWrapper {
  @ViewChild('fieldComponent', {read: ViewContainerRef}) fieldComponent: ViewContainerRef;
  
  index:number;

  constructor(){
    super();
    
  }

  getIndex = (field:any)  => {
    let indexArr = field.key.split('_');
    this.index = indexArr[indexArr.length - 1];
    return this.index;
  }
}
