import { Component } from '@angular/core';
import { FieldType } from 'ng-formly';

@Component({
  selector: 'formly-field-input',
  template: `
    <div [formGroup]="form" >
        <p class="parrafo color-azulprofundo izquierdo col-xs-12 col-sm-12 col-md-7 col-lg-7">
            {{to.label}}
        </p>
    
        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
            <span *ngFor="let option of to.options">
                <label class="radio-inline"><input [name]="id" type="radio" [value]="option.key" [formControl]="formControl"
                [formlyAttributes]="field" class="custom-control-input">{{option.value}}</label>
            </span>
        </div>
    </div>
    `,
})
export class FormlyFieldRadioS1 extends FieldType {
  get type(): string {
    return this.to.type || 'radio';
  }
  
  get inputClass(): string {
    return this.to.inputClass || '';
  }
}