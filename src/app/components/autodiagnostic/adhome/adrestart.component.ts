import { Component, Input, OnInit }  from '@angular/core';
// FORMS
import { FormsModule, ReactiveFormsModule, Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { FormlyModule, FormlyFieldConfig, FormlyBootstrapModule } from 'ng-formly';

import { QuestionBase }              from './../forms/question-base';

import { AutodiagnosticService } from "./../../../Services/autodiagnostic.service";
import { UserService } from "./../../../Services/user.service";
import { AccountService } from "./../../../Services/account.service";

import { FormlyPanelWrapper } from './radio-wrapper';
import { FormlyQuestionWrapper } from './question-wrapper';
import { Router, ActivatedRoute } from '@angular/router';

import 'rxjs/add/operator/map';

@Component({
  selector: 'app-restart',
  templateUrl: './adhome.component.html',
  styleUrls: ['./adhome.component.css'],
  providers: [AccountService, AutodiagnosticService, UserService]
})
export class AdRestartComponent implements OnInit {

  step: number = 1;
  totalSteps: number = 1;
  section: any;
  description: string;

  private sub: any;
  flag: boolean;

  public loading: boolean=true;

  @Input() questions: QuestionBase<any>[] = [];
  form: FormGroup;
  payLoad = '';
  options;
  autodiagnosticModel: any = {};
  private autFields: Array<FormlyFieldConfig> = [];

  constructor(
    private service: AccountService,
    private autodiagnosticService: AutodiagnosticService, 
    private userService: UserService,
    private fb: FormBuilder,
    private router: Router,
  ) {}

  ngOnInit() {
    window.scrollTo(0,0);
    this.form = this.fb.group({});
    this.service.content(this.userService.JWTGET({ id: 241 })).subscribe(response => {
      if (response.status) { 
        if(response.Contents.length > 0){
          this.description = response.Contents[0];
        }        
      }
    });

    this.autodiagnosticService.currentStep(this.userService.JWTGET()).subscribe(
      response => {
        this.step = 1;
        this.totalSteps = response.Step.totalSteps || 1;
        this.getStep();
      }
    );
    
  }

  getStep = () => {
    this.loading = true;
    this.autodiagnosticService.getStep(this.step, this.userService.JWTGET()).subscribe( response => {
      this.section = response.Step[0].sections[0];
      this.autFields = this.getQuestions( this.section );
    });
  }

  checkType = (answers: any, type: string) => {

    return answers.filter(answers => {
      return answers.type === type;
    })[0];

  }

  getObj = ( answers: any, type: string ) => {
    let options = [];
    for( var i = 0; i < answers.length; i++){
      if(answers[i].type === type){
        options.push(answers[i]);
      }
    }
    return options;
  }

  checkResponse(answer:any, response:any){
    response = response.response;

    let res = response.filter(res => {
      return res.Answers_id == answer.id;
    })[0];
    
    return res?true:false;

  }

  onChangeChbx = ( value: any ) => {
    
  }
  
  saveOption = ( answer:any ) => {
    if(answer.Questions_id == "4" && answer.value == "141"){
      $('input[id$="_inputq_q_5_4"]').val("");
      this.autFields[4].templateOptions.change(null,"",null);
      $('select[id$="_selectq_q_6_5"]').val("");
      this.autFields[5].templateOptions.change(null,"",null);
      $('select[id$="_selectq_q_7_6"]').val("");
      this.autFields[6].templateOptions.change(null,"",null);
      $('input[id$="_multicheckbox_q_8_7"]').prop("checked", false);
      for( var i = 0; i < this.autFields[7].templateOptions.options.length; i++){
        if(this.autFields[7].templateOptions.options[i].defaultValue){
          this.autFields[7].templateOptions.options[i].defaultValue = false;
        }else{
          this.autFields[7].templateOptions.options[i].defaultValue = null;
          this.autFields[7].templateOptions.options[i].checked = null;
        }
      }
      this.autFields[7].templateOptions.change(null, this.autFields[7].templateOptions.options, this.autFields[7].formControl);
      
      $('input[id$="_inputq_q_5_4"]').prop('disabled', true);
      $('select[id$="_selectq_q_6_5"]').prop('disabled', true);
      $('select[id$="_selectq_q_7_6"]').prop('disabled', true);
      $('input[id$="_multicheckbox_q_8_7"]').prop('disabled', true);
    }else if(answer.Questions_id == "4"){
      $('input[id$="_inputq_q_5_4"]').prop('disabled', false);
      $('select[id$="_selectq_q_6_5"]').prop('disabled', false);
      $('select[id$="_selectq_q_7_6"]').prop('disabled', false);
      $('input[id$="_multicheckbox_q_8_7"]').prop('disabled', false);
    }

    if(answer.Questions_id == "16" && answer.value == "931"){
      $('input[id$="_radios1_q_17_16"]').prop("checked", false);
      this.autFields[16].templateOptions.change(null,"",null);//aqui
      $('input[id$="_multicheckbox_q_18_17"]').prop("checked", false);
      for( var i = 0; i < this.autFields[16].templateOptions.options.length; i++){
        if(this.autFields[17].templateOptions.options[i].defaultValue){
          this.autFields[17].templateOptions.options[i].defaultValue = false;
        }else{
          this.autFields[17].templateOptions.options[i].defaultValue = null;
        }
      }
      this.autFields[17].templateOptions.change(null,this.autFields[17].templateOptions.options, this.autFields[17].formControl);

      $('input[id$="_radios1_q_17_16"]').prop('disabled', true);
      $('input[id$="_multicheckbox_q_18_17"]').prop('disabled', true);
    }else if (answer.Questions_id == "16"){
      $('input[id$="_radios1_q_17_16"]').prop('disabled', false);
      $('input[id$="_multicheckbox_q_18_17"]').prop('disabled', false);
    }
    
    if(answer.Questions_id == "19" && answer.value == "1021"){
      console.log(this.autFields);
      $('input[id$="_inputq_q_20_19"]').val("");
      this.autFields[19].templateOptions.change(null,"",null);
      $('input[id$="_inputq_q_21_20"]').val("");
      this.autFields[20].templateOptions.change(null,"",null);
      $('input[id$="_multicheckbox_q_22_21"]').prop("checked", false);
      for( var i = 0; i < this.autFields[21].templateOptions.options.length; i++){
        if(this.autFields[21].templateOptions.options[i].defaultValue){
          this.autFields[21].templateOptions.options[i].defaultValue = false;
        }else{
          this.autFields[21].templateOptions.options[i].defaultValue = null;
        }
      }
      this.autFields[21].templateOptions.change(null,this.autFields[21].templateOptions.options, this.autFields[21].formControl);
      $('input[id$="_radios1_q_23_22"]').prop("checked", false);
      console.log(this.autFields);
      this.autFields[22].templateOptions.change(null, "", null);
      $('input[id$="_inputq_q_20_19"]').prop('disabled', true);
      $('input[id$="_inputq_q_21_20"]').prop('disabled', true);
      $('input[id$="_multicheckbox_q_22_21"]').prop('disabled', true);
      $('input[id$="_radios1_q_23_22"]').prop('disabled', true);
    }else if(answer.Questions_id == "19"){
      $('input[id$="_inputq_q_20_19"]').prop('disabled', false);
      $('input[id$="_inputq_q_21_20"]').prop('disabled', false);
      $('input[id$="_multicheckbox_q_22_21"]').prop('disabled', false);
      $('input[id$="_radios1_q_23_22"]').prop('disabled', false);
    }

    this.autodiagnosticService.saveAnswer(answer, this.userService.JWT())
    .subscribe( response => {
        
    })
  }
  
  nextStep = (step: number) => {
      
    this.options.formState.submitted = true;
    if(!this.form.invalid){
      this.loading = true;
      this.autodiagnosticService.saveProgress(this.step, this.userService.JWT())
      .subscribe( response => {
        this.step = step <= 1 ? 1: step;
        if(this.step <= this.totalSteps){
          this.getStep();
        }else{
          this.loadResults();
        }
      });
    }
    
  }

  loadResults = () => {
    this.router.navigate(['/autodiagnostic/test/results']);
  }

  previousStep(step:number){

    this.loading = true;
    this.options.formState.submitted = false;
    this.step = step <= 1 ? 1: step;
    this.form = this.fb.group({});
    this.getStep();

  }

  submit(model:any){
    console.log(model);
    window.scrollTo(0, 0);
  }

  getQuestions( section: any ) {
    
        var autFields: Array<FormlyFieldConfig> = [];
    
        section.Questions.forEach(question => {
            let type:string = question.answers[0].type; 

            let myRequired = true;
            if(question.id == 5 || question.id == 6 || question.id == 7 || question.id == 8 || question.id == 17 || question.id == 18 || question.id == 20 || question.id == 21 || question.id == 22 || question.id == 23){
              myRequired = false;
            }
            let myValidator = null;

            if(type == 'select'){
                if(myRequired){
                  myValidator = {validation: Validators.compose([Validators.required])};
                }

                autFields.push({
                  key: "q_"+question.id,
                  type: 'selectq',
                  wrappers: ['question'],
                  defaultValue: this.getResponse(question.response),
                  validators: myValidator,
                  templateOptions: {
                    options: this.getOptions(question.answers, type),
                    label: question.question,
                    change: (e, f, g) => {
                      let response = {
                        'id': f._value,
                        'Questions_id': question.id,
                        'type': 'select',
                        'value': f._value,
                      }
                      this.saveOption(response);
                    }
                  }
                });

            }

          
    
            if(type == 'number' || type == 'text'){

              if(myRequired){
                myValidator = {validation: Validators.compose([Validators.required])};
              }
              
              autFields.push({
                key: "q_"+question.id,
                type: 'inputq',
                wrappers: ['question'],
                defaultValue: question.response[0]?question.response[0].response:null,
                validators: myValidator,
                templateOptions:{
                  type: type,
                  label: question.question,
                  required: myRequired,
                  change: (e, f, g) => {
                    let response = {
                      'id': question.answers[0].id,
                      'Questions_id': question.id,
                      'type': 'input',
                      'value': f._value,
                    }
                    this.saveOption(response);
                  }
                }
              });

            }

      
            
            if(type == 'checkbox'){

              if(myRequired){
                myValidator = {validation: ['mcbRequired']};
              }
              
              autFields.push({
                key: "q_"+question.id,
                type: 'multicheckbox',
                wrappers: ['question'],
                defaultValue: this.getResponse(question.response, true),
                validators: myValidator,
                templateOptions:{
                  required: myRequired,
                  label: question.question,
                  options: this.getOptions(question.answers, type, question.response),
                  change: (e, f, g) => {
                    let options:any = {type: type, options:[]};
                    if(e==null){
                      Object.keys(f).map((key) => {
                        if(g != null){
                          var myKey = ''+Object.keys(g.controls)[key];
                        }else{
                          var myKey = key;
                        }
                        options.options.push({
                          'id': myKey,
                          'Questions_id': question.id,
                          'type': 'checkbox',
                          'value': myKey,
                          'checked': f[key].defaultValue,
                        })
                      });
                    }else{
                      let controls = f._parent.controls;
                      Object.keys(controls).map((key) => {
                        options.options.push({
                          'id': key,
                          'Questions_id': question.id,
                          'type': 'checkbox',
                          'value': key,
                          'checked': controls[key]._value,
                        })
                      });
                    }
                    
                    this.saveOption(options);
                  }
                }
              });

            }
    
            if(type == 'radio' && this.step <= 1){
                
              if(myRequired){
                myValidator = {validation: Validators.compose([Validators.required])};
              }

              autFields.push({
                key: "q_"+question.id,
                type: 'radios1',
                defaultValue: this.getResponse(question.response),
                wrappers: ['question'],
                validators: myValidator,
                templateOptions:{
                  required: myRequired,
                  rows:1,
                  cols:2,
                  label: question.question,
                  options: this.getOptions(question.answers, type, question.response),
                  name:"q_"+question.id,
                  change: (e, f, g) => {
                    let response = {
                      'id': f._value,
                      'Questions_id': question.id,
                      'type': 'radio',
                      'value': f._value,
                    }
                    this.saveOption(response);
                  }
                }
              });
                
            }else if(type == 'radio' && this.step > 1){

              if(myRequired){
                myValidator = {validation: Validators.compose([Validators.required])};
              }

              autFields.push({
                className: 'row',
                wrappers: ['panel'],
                templateOptions : {
                  title: question.question
                },
                fieldGroup : [{
                  className: 'pieza',
                  type: 'radioq',
                  key: "q_"+question.id,
                  validators: myValidator,
                  defaultValue: this.getResponse(question.response),
                  templateOptions: {
                    options: this.getOptions(question.answers, type, question.response),
                    name:"q_"+question.id,
                    title: question.question,
                    change: (e, f, g) => {
                      let response = {
                        'id': f._value,
                        'Questions_id': question.id,
                        'type': 'radio',
                        'value': f._value,
                      }
                      this.saveOption(response);
                    }
                  }
                }]
              });
            }
            
    
        });

        this.options = {
          formState: {
            readOnly: true,
            submitted: false,
          },
        };
        this.loading = !this.loading;
        return autFields;
        //return questions.sort((a, b) => a.order - b.order);
  }

    
  getOptions = (answers: any, type:string, response?:any) => {
    var options: any[]= [];
    
    answers.forEach(answer => {
        if(type == 'checkbox'){
          options.push({key:answer.id, value: answer.label, defaultValue: this.getChecked(response, answer.id)});
        }else if(type == "select"){
          options.push({value:answer.id, label: answer.label});
        }else{
          options.push({key:answer.id, value: answer.label});
        }
        
    });
    if(type == "checkbox") options.pop();
    return options;

  }
  
  getChecked = (response:any, answerId: string) => {
    
    var checked:boolean = false;
    response.forEach(res => {
      if(res.Answers_id == answerId && !checked){
        checked = true;
      };
    });
    
    return checked;
  }

  getResponse = (response: any, multi?:boolean) => {
    if(!multi){
      var result:any; 
      response.forEach(res => {
            result = res.Answers_id
      });
    }else{
      var result:any = [];
      response.forEach(res => {
        result[res.Answers_id]= this.getChecked(response, res.Answers_id); console.log(res.Answers_id+' = '+this.getChecked(response, res.Answers_id)) ;
      });
    }
    return result;
  }


}
