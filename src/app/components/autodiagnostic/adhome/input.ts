import { Component } from '@angular/core';
import { FieldType } from 'ng-formly';

@Component({
  selector: 'formly-field-input',
  template: `
    <div [formGroup]="form" >
        <p class="parrafo color-azulprofundo izquierdo col-xs-12 col-sm-12 col-md-7 col-lg-7">
            {{to.label}}
        </p>
    
        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
            
        <input [name]="id" [type]="to.type" [formControl]="formControl" [formlyAttributes]="field" class="form-control">

        </div>
    </div>
    `,
})
export class FormlyFieldInput extends FieldType {
  get type(): string {
    return this.to.type || 'text';
  }
  
  get inputClass(): string {
    return this.to.inputClass || '';
  }
}