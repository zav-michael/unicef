import { Component, ViewContainerRef, ViewChild } from '@angular/core';
import { FieldWrapper } from 'ng-formly';
@Component({
  selector: 'formly-wrapper-panel',
  template: `
    <div class="grupo">
        <div class="pregunta pieza">
        <span class="numeral">{{getIndex(field)}}</span>  
          <p class="parrafo color-azulprofundo izquierdo">{{templateOptions.title}}</p>
        </div>

        <template #fieldComponent></template>
      
    </div>
  `,
})
export class FormlyPanelWrapper extends FieldWrapper {
  @ViewChild('fieldComponent', {read: ViewContainerRef}) fieldComponent: ViewContainerRef;
  
  index:number;

  getIndex = (field:any)  => {
    let indexArr = field.id.split('_');
    this.index = parseInt(indexArr[4])+1;
    return this.index;
  }
}
