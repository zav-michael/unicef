export function ValidateMulticheckbox (control:any){
    if (control.value) {
        for (let key in control.value) {
            if (control.value[key] === true) {
            return null;
            }
        }
    }
    return { required: true };
}

export function required(err?, field?) {
    let cField = field.templateOptions.label ? field.templateOptions.label: field.templateOptions.title;
    return `${cField} É necessário.`;
}

export function minLenght(err?){
    return `Should have atleast ${err.requiredLength} Characters`;
}

export function showError(field?: any){
    return (field.formState.submitted || field.formControl.touched) && !field.formControl.valid;
}
