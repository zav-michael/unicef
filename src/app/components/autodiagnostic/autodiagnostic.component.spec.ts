import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutodiagnosticComponent } from './autodiagnostic.component';

describe('AutodiagnosticComponent', () => {
  let component: AutodiagnosticComponent;
  let fixture: ComponentFixture<AutodiagnosticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutodiagnosticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutodiagnosticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
