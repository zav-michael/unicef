import { Component, OnInit } from '@angular/core';
import {ContentService} from './../../Services/content.service';
import { environment } from './../../../environments/environment';

import { LoaderService } from './../../Services/loader.service'

@Component({
  selector: 'app-program',
  templateUrl: './program.component.html',
  styleUrls: ['./program.component.css']
})
export class ProgramComponent implements OnInit {


  public loading: boolean=true;
  title: string;  
  body: string;
  steps: any[] = [];
  videoUrl: string;
  compName = 'programas';
  
 
  
  constructor(private contentService: ContentService,private loaderService: LoaderService) { 


   this.contentService.getContent(this.compName, 'type', 'text').subscribe( data => {
      this.steps = data.Contents[0].fields;
   });



   this.contentService.getContent(this.compName, 'type', 'video').subscribe( data => {
     
     this.title = data.Contents[0].title;
     this.body = data.Contents[0].body;
     this.videoUrl = environment.api + data.Contents[0].fields[0].value;
     console.log("URL-VIDEO" + this.videoUrl);
     console.log(this.title);
     console.log(this.body);
     this.loading = false;
    
   });

  }
  ngAfterViewInit()
  {
    this.loading = true;

  }
  
  ngOnInit() 
  {
    window.scrollTo(0,0);
  }
  changeCollapse(index1, index2){
    let flecha
    if(index2 == null){
      flecha = $('#flecha-'+index1);
    }else{
      flecha = $('#flecha-'+index2+'--'+index1);
    }
    flecha.toggleClass("abajo arriba");
  }

}
