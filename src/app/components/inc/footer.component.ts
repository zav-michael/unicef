import { Component, OnInit } from '@angular/core';
import { environment } from './../../../environments/environment';
import {ContentService} from './../../Services/content.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent {

  logosService = 'logos';
  logos_cont: any[] = [];
  urlApi:string;

  constructor(
    private contentService: ContentService
  ) {
    this.urlApi = environment.api;
   }

  ngOnInit() {
    this.contentService.get(this.logosService).subscribe( data => {   
      var aa = JSON.stringify(data.Contents);
      console.log("logos=="+aa); 

      for(var i = 0; i < data.Contents.length; i++){
        this.logos_cont.push({value: data.Contents[i].fields[0].value, title: data.Contents[i].title});   
      }
      console.log(this.logos_cont);
    });
  }

}
