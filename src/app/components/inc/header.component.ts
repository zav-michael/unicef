import { Subject } from 'rxjs/Subject';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { SharedService } from './../../Services/shared.service';
import { AuthenticationService } from './../../Services/authentication.service';
import { AlertService } from './../../Services/alert.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

import { User } from  './../../models/users.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  providers: [AuthenticationService]
})
export class HeaderComponent implements OnInit {

    model: any = {};
    user: User;
    isCollapsed:boolean;
    returnUrl: string;
    loggedIn: boolean;
    openlogin:boolean = false;
    restore: boolean = false;
    alert:string;
    passwordfields: boolean = false;
    alertClass: boolean = false;
    closeLogin: boolean;
    closeRestore: boolean;

    constructor( 
      private sharedService: SharedService,
      private alertService: AlertService,
      private http: Http,
      private route: ActivatedRoute,
      private router: Router,
      private authenticationService: AuthenticationService,
    ){}
    
    ngOnInit(){

      this.user = JSON.parse(localStorage.getItem('currentUser'));

      if( this.user && this.user.id && this.user.contact_name ){
        this.sharedService.handleLogin( true );
      }
      this.sharedService.currentcloseRestore.subscribe(closeRestore => this.closeRestore = closeRestore);
      this.sharedService.currentCloseLogin.subscribe(closeLogin => this.closeLogin = closeLogin);
      this.sharedService.currentLogin.subscribe( loggedIn => this.loggedIn = loggedIn );
      this.sharedService.currentCollapse.subscribe( isCollapsed => this.isCollapsed = isCollapsed );
      
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    }



    ngAfterViewInit () {

      this.route.queryParams.subscribe( 
        params => { 
          if(params['restore']){
            this.authenticationService.verifyRestoreToken( params['restore'] )
            .subscribe(
              data => {
                  if( data && data.id){
                    this.model.id = data.id;
                    this.model.passwordToken = params['restore'];
                    this.passwordfields = !this.passwordfields;
                  }
              }
            )
          }
          
      
      });

    }

    login = (email: string, password: string) => {
      this.authenticationService.login(this.model.email, this.model.password)
          .subscribe(
            data => {
              if(data.contact_name){
                this.user = data;
                this.sharedService.handleLogin( true );
                this.model.email = "";this.model.password = "";
                window.location.href = '/jogue_limpo/minha-conta';
              }
            },
            error => {
              let errorR = JSON.parse(error._body);
              this.alertService.error(errorR.error.message);
            }
          )
    }

    logout = () => {

      this.authenticationService.logout();

    }

    restorePassword = (email: string) => {
      this.authenticationService.restore( this.model.email )
          .subscribe(
            data => {
              
              if( !data.status ){
                this.alertClass = false;
                this.alert = data.error.message;
              }else{
                this.alertClass = true;
                this.alert = data.message;
                this.model.password = "";
                this.openlogin = true;
                this.showRestore();
              }
            }
          )
    }

    changePassword = (password: string, verifypassword: string, passwordToken: string) => {
      
        this.authenticationService.updateUser(this.model.id, this.model.password, this.model.verifypassword, this.model.passwordToken)
        .subscribe(
          data => {

            this.alertClass = true;
            this.alert = data.message;
            this.passwordfields = !this.passwordfields;
            this.model.password = "";
            this.openlogin = true;
        
          },
          error => {
            let data = JSON.parse(error._body);
            this.alertClass = false;
            this.alert = data.error.message;
          }
        )
      
    }

    showRestore = () => {
      this.restore = !this.restore;
    }
    
    showLogin = () => {
      this.restore = false;
      this.passwordfields = false;
      this.openlogin = true;
    }

}
