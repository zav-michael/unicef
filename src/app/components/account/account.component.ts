import { Component, OnInit }  from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html'
})
export class AccountComponent implements OnInit {

  constructor(private router: Router) { 
    
  }

  ngOnInit() {
    let user = JSON.parse(localStorage.getItem('currentUser'));

    if( user && user.perfil != 'Unicef' ){
      this.router.navigate(['home']);
    }
    console.log(user);
    
  }

}
