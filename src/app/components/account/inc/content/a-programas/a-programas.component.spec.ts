import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AProgramasComponent } from './a-programas.component';

describe('AProgramasComponent', () => {
  let component: AProgramasComponent;
  let fixture: ComponentFixture<AProgramasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AProgramasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AProgramasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
