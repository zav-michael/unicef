import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { environment } from './../../../../../../environments/environment';
import { AlertService } from './../../../../../Services/alert.service';
import { SharedService } from './../../../../../Services/shared.service';
import { AccountService } from './../../../../../Services/account.service';
import { AutodiagnosticService } from './../../../../../Services/autodiagnostic.service';
import { UserService } from './../../../../../Services/user.service';
import { DialogService } from "ng2-bootstrap-modal";
import { AcceptComponent } from './../../../../modal/accept/accept.component';
import { Pagination, Page } from './../../../../../models/pagination.model';
import { Program, ProgramRequired } from './../../../../../models/program.model';
import { FileUploaderService } from './../../../../../Services/file-uploader.service';
import { ContentService } from './../../../../../Services/content.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';

export class Content{

   title: string;
   body: string;
   id: string;
   fields: Array<any>;

   constructor(){
     this.title = "";
     this.body = "";
     this.id = "";  
     this.fields = [];
   }

}



@Component({
  selector: 'app-a-programas',
  templateUrl: './a-programas.component.html',
  styleUrls: ['./a-programas.component.css'],
  providers: [AccountService, UserService, AutodiagnosticService, Pagination, Content]
})
export class AProgramasComponent implements OnInit {

  content: Content;
  steps: any[] = [];
  urlApi:string;
  compName = 'programas';
  programForm:any = [];
  required: any;
  sizeFile: number = 3288493;
  edConfig: Object = {
    toolbarInline: true,
    charCounterCount: false,
  }

  constructor(private contentService: ContentService, private service: AccountService, private user: UserService,
    private sharedService:SharedService, private dialogService: DialogService, private autodiagnosticService: AutodiagnosticService,
    private alertService: AlertService, private elem: ElementRef) { 

      this.content = new Content();
      this.urlApi = environment.api;

  }

  ngOnInit() {

    this.programForm = new Program();
    this.required = new ProgramRequired(); 

    this.loadContent();

  }

  loadContent() 
  {
    this.contentService.get(this.compName).subscribe( data => {
      
        this.steps = data.Contents[0].fields;
        this.content = data.Contents[0];
      
    });
  }


  handleVideo(event, idInput, item)
  {

    let formData = new FormData();
    let files = this.elem.nativeElement.querySelector("#guide-"+idInput).files;
    if(files.length > 0){
      if(files[0].size > this.sizeFile){
        this.alertService.error("Error, video can't be upload by size.");
      }else{
        formData.append('content_id', this.content.id );
        formData.append('custom['+item.id+']', files[0], files[0].name);
        let data = this.contentService.saveFields('Contents/video', formData).subscribe(
          response => {
            if (response.status) { 
             this.loadContent();
             this.alertService.success("Video update.");
             }else{
              this.alertService.error("Video can't be update, try again (It can be by size or type file.)");
             }
          },
          error => {
            var response = error.json();
            this.alertService.error(response.error.message);
          });
      }
    }else{
      this.alertService.error("Choose a file.");
    }

  }


  save(){
    let formData = new FormData();
    formData.append('title', this.programForm.title );
    formData.append('value', this.programForm.value );
    formData.append('content_id', this.content.id );
    formData.append('custom_fields_id', '2');
    let data = this.contentService.saveFields('Contents/fields', formData).subscribe(response => {
      if (response.status) { 
        this.programForm = new Program();
        this.required = new ProgramRequired();
        this.elem.nativeElement.querySelector("#form-ri").reset();
        this.loadContent();
        this.alertService.success("Content save.");
       }else{
         this.alertService.error("Content can't be save, try again.");
       }
    });
  }

  saveContent(){
    
      var param = this.user.serialize(this.content);
      this.service.put( param, this.user.JWTPUT()).subscribe((response) => {
        if(response.status){
          this.alertService.success("Content update.")
        }else{
          this.alertService.error("Content can't be update.")
        }
      });
  
    }

    saveAnswEditableCustom(field){
      
          var data = field; 
          var param = this.user.serialize(data);
          this.autodiagnosticService.put( param, '/Contents/fields' , this.user.JWTPUT()).subscribe((response) => {
            if(response.status){
              this.alertService.success("Content update.")
            }else{
              this.alertService.error("Content can't be update.")
            }
          });
      
     }  
     changeCollapse(index1, index2){
      let flecha
      if(index2 == null){
        flecha = $('#flecha-'+index1);
      }else{
        flecha = $('#flecha-'+index2+'--'+index1);
      }
      flecha.toggleClass("abajo arriba");
    }

  showSureVideo(id){
    let disposable = this.dialogService.addDialog(AcceptComponent, {
      title: 'Excluir video',
      message: 'Você tem certeza de executar a seguinte ação.'
    }).subscribe((isConfirmed) => {
        //We get dialog result
        if (isConfirmed) {
          this.deleteContentVideo(id);
        } 
      });
  }

  deleteContentVideo(id){
    this.service.delete(this.user.JWTGET({ idCustom: id , video: true})).subscribe( (response) => {
      //console.log(response);
      // if(response.status){
      //   this.alertService.success("Exclusão do Video.");
      //   this.service.content(this.user.JWTGET({ slug: this.compName })).subscribe(response => {
      //     if (response.status) {
      //       this.sharedService.handleLogos(response.Contents);
      //     }
      //   });
      // }else{
      //   this.alertService.error("O logotipo não pode ser excluído.")
      // }
    }); 
  }

  deleteContentIssue(id){
    this.service.delete(this.user.JWTGET({ idCustom: id })).subscribe( (response) => {
      //console.log(response);
      if (response.status) { 
        this.programForm = new Program();
        this.required = new ProgramRequired();
        this.elem.nativeElement.querySelector("#form-ri").reset();
        this.loadContent();
        this.alertService.success("Content deleted.");
      }else{
        this.alertService.error("Content can't be deleted, try again.");
      }
    }); 
  }
}
