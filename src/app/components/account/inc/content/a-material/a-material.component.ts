import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { environment } from './../../../../../../environments/environment';
import { AlertService } from './../../../../../Services/alert.service';
import { SharedService } from './../../../../../Services/shared.service';
import { AccountService } from './../../../../../Services/account.service';
import { AutodiagnosticService } from './../../../../../Services/autodiagnostic.service';
import { UserService } from './../../../../../Services/user.service';
import { Pagination, Page } from './../../../../../models/pagination.model';
import { Resource, ResourceRequired } from './../../../../../models/resource.model';
import { FileUploaderService } from './../../../../../Services/file-uploader.service';
import { ContentService } from './../../../../../Services/content.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-a-material',
  templateUrl: './a-material.component.html',
  styleUrls: ['./a-material.component.css'],
  providers: [AccountService, UserService, AutodiagnosticService, Pagination, FileUploaderService, ContentService]
})
export class AMaterialComponent implements OnInit {

  data: any;
  resourceForm:any = [];
  required: any;
  content: any;
  isLoading = false;
  titleContent:any;

  constructor(private service: AccountService, private user: UserService,
    private sharedService:SharedService, private autodiagnosticService: AutodiagnosticService,
    private elem: ElementRef, private file: FileUploaderService, private alertService: AlertService, private contentService: ContentService) { 

      this.content = [];

  }

  ngOnInit() {

    this.resourceForm = new Resource();
    this.required = new ResourceRequired();
    this.loadContent();

    this.service.contentType(this.user.JWTGET({ slug: "material_apoyo" })).subscribe(response => {
      if (response.status) {
        this.titleContent = response.Contents[0];
      }
    });
  }


  loadContent(){
    this.contentService.get('material_apoyo').subscribe(response => {
      if (response.status) { 
        this.content = response.Contents;       
      }
    });
  }


  saveAnswEditable(item){
    var data = { id: item.id, title: item.title, fields: [] }; 
    var param = this.user.serialize(data);
    this.autodiagnosticService.put( param, '/Contents/' , this.user.JWTPUT()).subscribe((response) => {
      if(response.status){
        this.alertService.success("Content update.")
      }else{
        this.alertService.error("Content can't be update.")
      }
    });
  }


  delete(id){
    this.service.delete(this.user.JWTGET({ id: id })).subscribe( (response) => {
      if(response.status){
        this.alertService.success("Content delete.");
        this.loadContent(); 
      }else{
        this.alertService.error("The Content can't be delete.")
      }
    }); 
  }


  save(){  
    let formData = new FormData();
    let files = this.elem.nativeElement.querySelector("#file").files;
    formData.append('title', this.resourceForm.title );
    formData.append('body', 't');
    formData.append('content_type_id', "31");
    formData.append('custom[]', files[0], files[0].name);
    this.isLoading = true;
    let data = this.contentService.sendContent('contents', formData).subscribe(response => {
      if (response.status) { 
        this.resourceForm = new Resource();
        this.required = new ResourceRequired();
        this.elem.nativeElement.querySelector("#form-ri").reset();
        this.loadContent();
        this.alertService.success("Material save.");
       }else{
         this.alertService.error("Material can't be save, try again.");
       }
       this.isLoading = false;
    });
  }

  saveTitleContent(){
    var param = this.user.serialize(this.titleContent);
      this.service.putContentType( param, this.user.JWTPUT()).subscribe((response) => {
        if(response.status){
          this.alertService.success("Content update.")
        }else{
          this.alertService.error("Content can't be update.")
        }
    });
  }

}
