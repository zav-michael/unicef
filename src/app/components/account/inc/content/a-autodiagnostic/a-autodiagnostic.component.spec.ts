import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AAutodiagnosticComponent } from './a-autodiagnostic.component';

describe('AAutodiagnosticComponent', () => {
  let component: AAutodiagnosticComponent;
  let fixture: ComponentFixture<AAutodiagnosticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AAutodiagnosticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AAutodiagnosticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
