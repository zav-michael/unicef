import { Component, OnInit, Input } from '@angular/core';
import { environment } from './../../../../../../environments/environment';
import { AlertService } from './../../../../../Services/alert.service';
import { SharedService } from './../../../../../Services/shared.service';
import { AccountService } from './../../../../../Services/account.service';
import { AutodiagnosticService } from './../../../../../Services/autodiagnostic.service';
import { UserService } from './../../../../../Services/user.service';
import { Pagination, Page } from './../../../../../models/pagination.model'
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';


export class Content{
  id: string;
  title: string;
  body: string;
  fields: any;
  constructor(){
    this.id = "";
    this.title = "";
    this.body = "";
    this.fields = [];
  }
}

@Component({
  selector: 'app-a-autodiagnostic',
  templateUrl: './a-autodiagnostic.component.html',
  styleUrls: ['./a-autodiagnostic.component.css'],
  providers: [AccountService, UserService, AutodiagnosticService, Pagination]
})



export class AAutodiagnosticComponent implements OnInit {

  content: Content;
  section: any;
  numberStep: number;
  pagination: Pagination;
  titleSection: string;
  descriptionSection: string;
  autodiagnostic:Array<any>;
  edConfig: Object = {
    toolbarInline: true,
    charCounterCount: false,
  }

  public currentPage:number = 1;

  constructor(
    private service: AccountService, private user: UserService,
    private sharedService:SharedService, private autodiagnosticService: AutodiagnosticService,
    private alertService: AlertService) {

      this.titleSection = "";
      this.descriptionSection = "";
      this.content = new Content();
      this.pagination = new Pagination();
      this.pagination.totalItems = 1;
      this.pagination.maxSize = 10;
      this.autodiagnostic = []
      this.sharedService.handlePagination(this.pagination);

  }

  ngOnInit() {


    this.service.content(this.user.JWTGET({ id: 241 })).subscribe(response => {
      if (response.status) { 
        if(response.Contents.length > 0){
          this.content = response.Contents[0];
        }        
      }
    });

    this.loadSection();   
    this.loadPages(); 

  }

  saveEditable(model){
     this.autodiagnostic[model.id] = model.question;
  }

  saveAnswEditable(anw){
    var data = { id: anw.id , label: anw.label }; 
    var param = this.user.serialize(data);
    this.autodiagnosticService.put( param, '/questions/answer/' , this.user.JWTPUT()).subscribe((response) => {
      if(response.status){
        this.alertService.success("Answer update.")
      }else{
        this.alertService.error("Answer can't be update.")
      }
    });
  }

  loadPages(){
    this.autodiagnosticService.get(this.user.JWTGET()).subscribe( response => {
      if(response.status){
        this.pagination.totalItems = response.Sections.length * this.pagination.maxSize;
        this.pagination.items = response.Sections;
        this.sharedService.handlePagination(this.pagination);
      }
    });
  }
  
  loadSection()
  {
    this.autodiagnosticService.getStep(this.currentPage, this.user.JWTGET()).subscribe( response => {
      this.section = [];
      if(response.Step.length > 0){
        if(response.Step[0].sections.length > 0){
          this.titleSection = response.Step[0].sections[0].name;
          this.descriptionSection = response.Step[0].sections[0].description;
          this.section = response.Step[0].sections[0].Questions;
        }
      }      
    });
  }

  saveContent(){
  
    var param = this.user.serialize(this.content);
    this.service.put( param, this.user.JWTPUT()).subscribe((response) => {
      if(response.status){
        this.alertService.success("Content update.")
      }else{
        this.alertService.error("Content can't be update.")
      }
    });

  }

  public setPage(pageNo:number):void {
    this.currentPage = pageNo;    
    
  }

  public pageChanged(event:any):void {
    this.loadSection();
  }

  public saveAutodiagnostic():void {
    if(this.autodiagnostic.length <= 0){
      this.alertService.error("You must do changes.")
    }else{
      var param = this.user.serialize(this.autodiagnostic);
      this.autodiagnosticService.allPut( param, this.user.JWTPUT()).subscribe((response) => {
        if(response.status){
          this.alertService.success("Section update.")
        }else{
          this.alertService.error("Section can't be update.")
        }
      });
    }

  }

}
