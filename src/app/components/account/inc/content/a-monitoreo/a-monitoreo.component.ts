import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormsModule, ReactiveFormsModule, Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { FormlyModule, FormlyFieldConfig, FormlyBootstrapModule } from 'ng-formly';
import { MonitoringService } from './../../../../../Services/monitoring.service';
import { UserService } from "./../../../../../Services/user.service";
import { SharedService } from './../../../../../Services/shared.service';
import { Pagination, Page } from './../../../../../models/pagination.model';
import { environment } from './../../../../../../environments/environment';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

@Component({
  selector: 'app-a-monitoreo',
  templateUrl: './a-monitoreo.component.html',
  styleUrls: ['./a-monitoreo.component.css'],
  providers: [MonitoringService, UserService, SharedService, Pagination]
})
export class AMonitoreoComponent implements OnInit {

  pagination: Pagination;
  currentPage:number = 1;
  commitments: any;
  monitoringModel : any = {};
  form: FormGroup;
  options;
  max:number = 100;
  current:number = 0;
  indicator:number;
  minimunScore:number = 60;
  public fields:any = [];
  public modalRef: BsModalRef;
  clubs: any = [];
  idClub = 0;
  promise: string;
  promiseTitle: string;

  constructor(
    private monitoring: MonitoringService,
    private user: UserService,
    private shared: SharedService,
    private fb: FormBuilder,
  ) {
    this.pagination = new Pagination();
    this.pagination.totalItems = 1;
    this.pagination.maxSize = 1;
    this.shared.handlePagination(this.pagination);
   }

  ngOnInit() {
    window.scrollTo(0,0);
    this.form = this.fb.group({});
    this.getClubs();
    
    this.monitoring.commitmentsPagination(this.user.JWTGET())
    .subscribe(response => {
        this.pagination.totalItems = response.Commitments.length * this.pagination.maxSize;
        this.pagination.items = response.Commitments;
        this.shared.handlePagination(this.pagination);
    });

    //    this.render();
    //});
    this.generalProgress();
    
  }


  changePlan = (event) => {
     this.idClub = parseInt(event.target.value);
     this.render();  
     this.generalProgress();
  } 


  getClubs = () => {

    this.monitoring.getClubs(this.user.JWTGET())
    .subscribe( response => {
        this.clubs = response.Clubs;
    });

  }

  render = () => {
    this.monitoring.commitmentStep(this.currentPage + 1, this.user.JWTGET(), this.idClub)
    .subscribe( response => {
        this.getQuestions(response.Commitments);
        this.promise = response.Commitments[0].label;
        this.promiseTitle = response.Commitments[0].title
        this.commitments = response.Commitments;
    });
  }

  generalProgress = () => {
    this.monitoring.generalProgress(this.user.JWTGET(), this.idClub)
    .subscribe( response => {console.log(response);
      this.current = response.response.generalProgress;
    })
  }

  inputChange = ( value, input ) => { 
      let data:any={};
      let key = input.key.split('_');
      data.indicators_id = key[key.length - 1]; 
      data.value = value < 0 ? 0 : value > 100 ? 100 : value ;
      input.valid = this.checkValid(data);
      this.form.controls[input.key].setValue(data.value);
      this.monitoring.saveProgress(data, this.user.JWT())
      .subscribe(response => {
        this.generalProgress();
      });
  }
  
  public pageChanged(event:any):void {
    this.render();
  }

  public openModal( indicator ){
    let key = indicator.split('_');

    this.indicator = key[key.length - 1]; 
    
  }

  getQuestions = ( commitments: any ) => {

    var fields:any = [];
    
    commitments.forEach( commit => {
      
      for( let key in commit.indicators){

          let indicator = commit.indicators[key];
          if(indicator.length > 0){
            if(key == 'priority'){
              
              fields.push({
                className: 'row',
                wrappers: ['monitoring'],
                templateOptions: {
                  title: 'Ação'
                },
                fieldGroup: this.getField(indicator, key)
              });
            
            }
            
            if( key == 'principal' ){
  
              fields.push({
                className: 'row',
                wrappers: ['monitoring'],
                templateOptions: {
                  title: 'Ação Principal'
                },
                fieldGroup: this.getField(indicator, key)
              });
            
            }
            
            if( key == 'complementary' ) {
              fields.push({
                className: 'row',
                wrappers: ['monitoring'],
                templateOptions: {
                  title: 'Ação Complementar'
                },
                fieldGroup: this.getField(indicator, key)
              });
  
            }   
          }
      }
    });
    
    this.form = this.toFormGroup(fields);

    this.options = {
      formState: {
        readOnly: true,
        submitted: false,
      },
    };
    
    this.fields = fields;

  }


  getField = ( indicators:any, type:string ) => {

    var fields: any[] = [];

    indicators.forEach((indicator , key) => {

      if( type == 'priority' ) {
        fields.push({
          key: 'ind_'+type+'_'+indicator.id,
          type: 'input',
          defaultValue: indicator.response && indicator.response.value? indicator.response.value:'',
          valid: this.checkValid( indicator.response),
          templateOptions:{
            label: indicator.action,
            change: (e, f, g) => {
              console.log(f);
            },
          },
        });
      }
    
      if( type=='principal' ){

        fields.push({
          key: 'ind_'+type+'_'+indicator.id,
          type: 'radio',
          defaultValue: indicator.response && indicator.response.value? indicator.response.value:'',
          valid: this.checkValid(indicator.response),
          templateOptions:{
            rows:1,
            cols:2,
            label: indicator.action,
            options: [
              {key:'ind_'+type+'_'+key+'_1', value: "100", label: 'Sim'},
              {key:'ind_'+type+'_'+key+'_2', value: "0", label: "Não"}
            ],
            name:'ind_'+type+'_'+key
          }
        });

      }
      
      if( type == 'complementary' ){

        fields.push({
          key: 'ind_'+type+'_'+indicator.id,
          type: 'input',
          defaultValue: indicator.response && indicator.response.value? indicator.response.value:'',
          valid: this.checkValid(indicator.response),
          templateOptions:{  
            label: indicator.action,
          }
        });

      }

    });

    return fields;
  }

  checkValid = (response) => {

      if( response && (response.value >= this.minimunScore )){
        
          return true;

      }
      
      return false;

  }

  toFormGroup(fields) {
    let group: any = {};
    fields.forEach(template => {
      template.fieldGroup.forEach(question => {
        group[question.key] = question.required ? new FormControl(question.defaultValue || '', Validators.required)
        : new FormControl(question.defaultValue || '');
      });
    });

    return new FormGroup(group);
  }
}
