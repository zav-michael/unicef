import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AMonitoreoComponent } from './a-monitoreo.component';

describe('AMonitoreoComponent', () => {
  let component: AMonitoreoComponent;
  let fixture: ComponentFixture<AMonitoreoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AMonitoreoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AMonitoreoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
