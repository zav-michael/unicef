import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { APrincipalComponent } from './a-principal.component';

describe('APrincipalComponent', () => {
  let component: APrincipalComponent;
  let fixture: ComponentFixture<APrincipalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ APrincipalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(APrincipalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});