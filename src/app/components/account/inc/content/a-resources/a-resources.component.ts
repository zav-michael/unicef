import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { environment } from './../../../../../../environments/environment';
import { AlertService } from './../../../../../Services/alert.service';
import { SharedService } from './../../../../../Services/shared.service';
import { AccountService } from './../../../../../Services/account.service';
import { AutodiagnosticService } from './../../../../../Services/autodiagnostic.service';
import { UserService } from './../../../../../Services/user.service';
import { Pagination, Page } from './../../../../../models/pagination.model';
import { Resource, ResourceRequired } from './../../../../../models/resource.model';
import { FileUploaderService } from './../../../../../Services/file-uploader.service';
import { ContentService } from './../../../../../Services/content.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-a-resources',
  templateUrl: './a-resources.component.html',
  styleUrls: ['./a-resources.component.css'],
  providers: [AccountService, UserService, AutodiagnosticService, Pagination, FileUploaderService, ContentService]
})
export class AResourcesComponent implements OnInit {


  data: any;
  categories: any;
  subcategories: any;
  resourceForm:any = [];
  required: any;
  content: any;
  titleCategory: string;
  idTemp: string;
  isLoading = false;
  titleContent:any;

  constructor(private service: AccountService, private user: UserService,
    private sharedService:SharedService, private autodiagnosticService: AutodiagnosticService,
    private elem: ElementRef, private file: FileUploaderService, private alertService: AlertService, private contentService: ContentService) { 
      
      this.data = [];
      this.categories = [];
      this.subcategories = [];
      this.content = [];

  }

  ngOnInit() {

    this.resourceForm = new Resource();
    this.required = new ResourceRequired();

    let data = this.contentService.getTaxonomy(this.user.JWTGET({ parent: "recursos" })).subscribe(response => {
      if (response.status) { 
        this.data  = response.taxonomy;
        this.categories = response.taxonomy;
      }
      
      this.service.contentType(this.user.JWTGET({ slug: "crecursos" })).subscribe(response => {
        if (response.status) {
          this.titleContent = response.Contents[0];
        }
      });

    });

  }


  changeCategoriesBottom(event){
    var id = event.target.value;
    this.idTemp = id;
    this.loadContent(id);
    
  }

  loadContent(id){
    this.contentService.getTaxonomy(this.user.JWTGET({ parent: "recursos", id: id })).subscribe(response => {
      if (response.status) { 
        if(response.taxonomy.length > 0){
          this.content = response.taxonomy[0].content;
          this.titleCategory = response.taxonomy[0].name;
        }       
      }
    });
  }

  saveAnswEditable(item){
    var data = { id: item.id, title: item.title, fields: [] }; 
    var param = this.user.serialize(data);
    this.autodiagnosticService.put( param, '/Contents/' , this.user.JWTPUT()).subscribe((response) => {
      if(response.status){
        this.alertService.success("Content update.")
      }else{
        this.alertService.error("Content can't be update.")
      }
    });
  }

  saveAnswEditableCustom(field){

    var data = field; 
    var param = this.user.serialize(data);
    this.autodiagnosticService.put( param, '/Contents/fields' , this.user.JWTPUT()).subscribe((response) => {
      if(response.status){
        this.alertService.success("Tags update.")
      }else{
        this.alertService.error("Tags can't be update.")
      }
    });

  }

  delete(id){
    this.service.delete(this.user.JWTGET({ id: id })).subscribe( (response) => {
      if(response.status){
        this.alertService.success("Content delete.");
        this.loadContent(this.idTemp); 
      }else{
        this.alertService.error("The Content can't be delete.")
      }
    }); 
  }

  changeCategories(event){
    this.subcategories = this.categories[event.target.value].term;
  }


  save(){
    let formData = new FormData();
    let files = this.elem.nativeElement.querySelector("#file").files;
    formData.append('title', this.resourceForm.title );
    formData.append('tags', this.resourceForm.tags );
    formData.append('term', this.elem.nativeElement.querySelector("#term").value );
    formData.append('content_type_id', "41");
    formData.append('custom[]', files[0], files[0].name);
    //Hacer visible pregress bar
    this.isLoading = true;
    let data = this.contentService.sendContent('taxonomy/content', formData).subscribe(response => {
      if (response.status) { 
        this.resourceForm = new Resource();
        this.required = new ResourceRequired();
        this.elem.nativeElement.querySelector("#form-ri").reset();
        this.alertService.success("Resource save.");
       }else{
         this.alertService.error("Resource can't be save, try again.");
       }
       //Ocultar progress bar
       this.isLoading = false;
    });
  }

  saveContent(){
    
    var param = this.user.serialize(this.content);
    this.service.put( param, this.user.JWTPUT()).subscribe((response) => {
      if(response.status){
        this.alertService.success("Content update.")
      }else{
        this.alertService.error("Content can't be update.")
      }
    });

  }

  saveTitleContent(){
    var param = this.user.serialize(this.titleContent);
      this.service.putContentType( param, this.user.JWTPUT()).subscribe((response) => {
        if(response.status){
          this.alertService.success("Content update.")
        }else{
          this.alertService.error("Content can't be update.")
        }
    });
  }

}
