import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AResourcesComponent } from './a-resources.component';

describe('AResourcesComponent', () => {
  let component: AResourcesComponent;
  let fixture: ComponentFixture<AResourcesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AResourcesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AResourcesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
