import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { environment } from './../../../../../../environments/environment';
import { AlertService } from './../../../../../Services/alert.service';
import { SharedService } from './../../../../../Services/shared.service';
import { AccountService } from './../../../../../Services/account.service';
import { AutodiagnosticService } from './../../../../../Services/autodiagnostic.service';
import { UserService } from './../../../../../Services/user.service';
import { Pagination, Page } from './../../../../../models/pagination.model';
import { FileUploaderService } from './../../../../../Services/file-uploader.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';


@Component({
  selector: 'app-a-recommendations',
  templateUrl: './a-recommendations.component.html',
  styleUrls: ['./a-recommendations.component.css'],
  providers: [AccountService, UserService, AutodiagnosticService, Pagination, FileUploaderService]
})
export class ARecommendationsComponent implements OnInit {
  
  section: any;
  numberStep: number;
  pagination: Pagination;
  titleSection: string;
  descriptionSection: string;
  autodiagnostic:Array<any>;
  apiUrl = "";
  sizeVideo:number = 3000;
  titleContent:any;
  
  public currentPage:number = 1;
  

  constructor(private service: AccountService, private user: UserService,
    private sharedService:SharedService, private autodiagnosticService: AutodiagnosticService,
    private elem: ElementRef, private file: FileUploaderService, private alertService: AlertService) { 

      this.titleSection = "";
      this.descriptionSection = "";
      this.pagination = new Pagination();
      this.pagination.totalItems = 1;
      this.pagination.maxSize = 10;
      this.apiUrl = environment.api;
      this.autodiagnostic = []
      this.sharedService.handlePagination(this.pagination);

    }

  ngOnInit() {

    this.loadPages();   

    this.service.contentType(this.user.JWTGET({ slug: "recomendaciones" })).subscribe(response => {
      if (response.status) {
        this.titleContent = response.Contents[0];
      }
    });

  }

  saveEditable(model){
    this.autodiagnostic[model.id] = model.question;
 }

 saveAnswEditable(anw){
   var data = anw; 
   var param = this.user.serialize(data);
   this.autodiagnosticService.put( param, '/recommendations/item/' , this.user.JWTPUT()).subscribe((response) => {
     if(response.status){
       this.alertService.success("Item update.")
     }else{
       this.alertService.error("Item can't be update.")
     }
   });
 }

 loadPages(){
   this.autodiagnosticService.getRecommend(this.user.JWTGET()).subscribe( response => {
     if(response.status){
       this.pagination.totalItems = response.questions.length * this.pagination.maxSize;
       this.pagination.items = response.questions;
       this.sharedService.handlePagination(this.pagination);
       if(response.questions.length > 0){
        this.currentPage = response.questions[0].steps_id - 1;
        this.titleSection = response.questions[this.currentPage - 1].name;
        this.descriptionSection = response.questions[this.currentPage - 1].description;
        this.loadSection();
       }
     }
   });  
 }
 
 loadSection()
 {
   this.autodiagnosticService.getRecommendStep(this.currentPage + 1, this.user.JWTGET()).subscribe( response => {
     this.section = [];
     if(response.Recommendations.length > 0){
        this.section = response.Recommendations;
     }      
   });
 }


 public setPage(pageNo:number):void {
   this.currentPage = pageNo;    
   
 }

 public pageChanged(event:any):void {
   this.loadSection();
 }

 public saveAutodiagnostic():void {
   if(this.autodiagnostic.length <= 0){
     this.alertService.error("You must do changes.")
   }else{
     var param = this.user.serialize(this.autodiagnostic);
     this.autodiagnosticService.allPut( param, this.user.JWTPUT()).subscribe((response) => {
       if(response.status){
         this.alertService.success("Section update.")
       }else{
         this.alertService.error("Section can't be update.")
       }
     });
   }

 }

 handleImage(event, idInput, id){

  let formData = new FormData();
  let files = this.elem.nativeElement.querySelector("#guide-"+idInput).files;
  formData.append('id', id);
  formData.append('custom['+id+']', files[0], files[0].name);
  this.file.updateUploadR('/recommendations/guide', formData).subscribe( res => {
    if(res.status){
    formData = new FormData();
    this.alertService.success("Guide change.");
    this.loadSection()
    }else{
    this.alertService.success("Guide can't be change.");
    }
  });

 }

saveTitleContent(){
  var param = this.user.serialize(this.titleContent);
    this.service.putContentType( param, this.user.JWTPUT()).subscribe((response) => {
      if(response.status){
        this.alertService.success("Content update.")
      }else{
        this.alertService.error("Content can't be update.")
      }
  });
}

}
