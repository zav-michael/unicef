import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ARecommendationsComponent } from './a-recommendations.component';

describe('ARecommendationsComponent', () => {
  let component: ARecommendationsComponent;
  let fixture: ComponentFixture<ARecommendationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ARecommendationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ARecommendationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
