import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { environment } from './../../../../../../environments/environment';
import { AlertService } from './../../../../../Services/alert.service';
import { SharedService } from './../../../../../Services/shared.service';
import { AccountService } from './../../../../../Services/account.service';
import { AutodiagnosticService } from './../../../../../Services/autodiagnostic.service';
import { UserService } from './../../../../../Services/user.service';
import { Pagination, Page } from './../../../../../models/pagination.model'
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, Response, RequestOptions } from '@angular/http';
import { FileUploaderService } from './../../../../../Services/file-uploader.service';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';

export class Content{
  id: string;
  title: string;
  body: string;
  fields: any;
  constructor(){
    this.id = "";
    this.title = "";
    this.body = "";
    this.fields = [];
  }
}


@Component({
  selector: 'app-a-bank',
  templateUrl: './a-bank.component.html',
  styleUrls: ['./a-bank.component.css'],
  providers: [AccountService, UserService, AutodiagnosticService, Pagination, FileUploaderService]
})
export class ABankComponent implements OnInit {

  content: Content;
  data:any;
  urlApi = "";
  titleContent:any;

  constructor(private service: AccountService, private user: UserService,
    private sharedService:SharedService, private autodiagnosticService: AutodiagnosticService,
    private alertService: AlertService, private elem: ElementRef, private file: FileUploaderService) { 
      this.content = new Content();   
      this.data = [];
      this.urlApi = environment.api;   
  }

  ngOnInit() {
    this.service.content(this.user.JWTGET({ slug: "banco_practicas" })).subscribe(response => {
      if (response.status) {
        this.data = response.Contents;
      }
    });

    this.service.contentType(this.user.JWTGET({ slug: "banco_practicas" })).subscribe(response => {
      if (response.status) {
        this.titleContent = response.Contents[0];
      }
    });

  }


  saveAnswEditable(anw){
    var data = anw; 
    var param = this.user.serialize(data);
    this.autodiagnosticService.put( param, '/Contents/' , this.user.JWTPUT()).subscribe((response) => {
      if(response.status){
        this.alertService.success("atualização bancária.")
      }else{
        this.alertService.error("O banco não pode ser atualizado.")
      }
    });
   }


   handleImage(event, idInput, item){
    
    let formData = new FormData();
    let files = this.elem.nativeElement.querySelector("#guide-"+idInput).files;
    if(files.length <= 0){

      this.alertService.error("Please, change image.");

    }else{

      var reader = new FileReader();
      reader.readAsDataURL(files[0]);
      reader.onload = () => {
        this.elem.nativeElement.querySelector("#img-"+item.id).setAttribute('src', reader.result);
      };

      formData.append('id', item.id);
      formData.append('title', item.title);
      formData.append('body', item.body);
      formData.append('custom['+item.fields[0].id+']', files[0], files[0].name);
      this.file.updateUploadR('/contents/updateSlider', formData).subscribe( res => {
        if(res.status){ 
         formData = new FormData();
         this.alertService.success("Bank change.");
        }else{
         this.alertService.success("Bank can't be change.");
        }
      });

    }
  }

  saveTitleContent(){
    var param = this.user.serialize(this.titleContent);
      this.service.putContentType( param, this.user.JWTPUT()).subscribe((response) => {
        if(response.status){
          this.alertService.success("Content update.")
        }else{
          this.alertService.error("Content can't be update.")
        }
    });
  }

}
