import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ABankComponent } from './a-bank.component';

describe('ABankComponent', () => {
  let component: ABankComponent;
  let fixture: ComponentFixture<ABankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ABankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ABankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
