import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { APlanComponent } from './a-plan.component';

describe('APlanComponent', () => {
  let component: APlanComponent;
  let fixture: ComponentFixture<APlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ APlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(APlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
