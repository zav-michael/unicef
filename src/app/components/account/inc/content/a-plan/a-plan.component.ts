import { Component, OnInit, Input } from '@angular/core';
import { environment } from './../../../../../../environments/environment';
import { AlertService } from './../../../../../Services/alert.service';
import { SharedService } from './../../../../../Services/shared.service';
import { AccountService } from './../../../../../Services/account.service';
import { AutodiagnosticService } from './../../../../../Services/autodiagnostic.service';
import { UserService } from './../../../../../Services/user.service';
import { Pagination, Page } from './../../../../../models/pagination.model'
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';

export class Content{
  id: string;
  title: string;
  body: string;
  fields: any;
  constructor(){
    this.id = "";
    this.title = "";
    this.body = "";
    this.fields = [];
  }
}

@Component({
  selector: 'app-a-plan',
  templateUrl: './a-plan.component.html',
  styleUrls: ['./a-plan.component.css'],
  providers: [AccountService, UserService, AutodiagnosticService, Pagination]
})



export class APlanComponent implements OnInit {

  content: Content;
  plan: any;

  constructor(private service: AccountService, private user: UserService,
    private sharedService:SharedService, private autodiagnosticService: AutodiagnosticService,
    private alertService: AlertService) { 
      this.content = new Content();   
      this.plan = [];   
    }
    edConfig: Object = {
      toolbarInline: true,
      charCounterCount: false,
    }

  ngOnInit() {

    this.service.content(this.user.JWTGET({ id: 651 })).subscribe(response => {
      if (response.status) { 
        if(response.Contents.length > 0){
          this.content = response.Contents[0];
        }        
      }
    });

    this.loadSection()

  }

  saveContent(){
    
      var param = this.user.serialize(this.content);
      this.service.put( param, this.user.JWTPUT()).subscribe((response) => {
        if(response.status){
          this.alertService.success("Content update.")
        }else{
          this.alertService.error("Content can't be update.")
        }
      });
  
    }

    loadSection()
    {
      this.autodiagnosticService.getPaso(this.user.JWTGET()).subscribe( response => {

        if(response.status){
          this.plan = response.Commitments;
        }
            
      });
    }

    saveAnswEditable(anw){
      var data = anw; 
      var param = this.user.serialize(data);
      this.autodiagnosticService.put( param, '/Commitments/' , this.user.JWTPUT()).subscribe((response) => {
        if(response.status){
          this.alertService.success("Plan update.")
        }else{
          this.alertService.error("Plan can't be update.")
        }
      });
    }

}
