import { Component, OnInit, AfterViewInit, ElementRef  } from '@angular/core';
import { SharedService } from './../../../../../Services/shared.service';
import { AccountService } from './../../../../../Services/account.service';
import { UserService } from './../../../../../Services/user.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, Response, RequestOptions } from '@angular/http';
import { ModalBannersComponent } from './../../../../modal/modal-component';
import { LogoModalComponent } from './../../../../modal/logo-component';
import { ModalNoticiasComponent } from './../../../../modal/noticia-component';
import { AcceptComponent } from './../../../../modal/accept/accept.component';
import { AutodiagnosticService } from './../../../../../Services/autodiagnostic.service';
import { DialogService } from "ng2-bootstrap-modal";
import { environment } from './../../../../../../environments/environment';
import { AlertService } from './../../../../../Services/alert.service';
import { FileUploaderService } from './../../../../../Services/file-uploader.service';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-a-home',
  templateUrl: './a-home.component.html',
  styleUrls: ['./a-home.component.css'], 
  providers: [AccountService, UserService, AutodiagnosticService, FileUploaderService]  
})

export class AHomeComponent implements OnInit {

  destacados: any = [];
  logos: any = [];
  open: any;
  sliderHome:any;
  urlApi: string;
  simpleDrop: any = null;
  itemsDestacados:any = {}; 
  data:any;
  

  constructor(private service: AccountService, private user: UserService,
   private dialogService: DialogService, private sharedService:SharedService, private file: FileUploaderService,
   private alertService: AlertService, private autodiagnosticService: AutodiagnosticService, private elem: ElementRef
  ) { 
    
    this.urlApi = environment.api;  
  }


  ngOnInit() { 
  

    this.sharedService.currentSliderHome.subscribe( sliderHome => { this.sliderHome = sliderHome; } );
    this.sharedService.currentLogo.subscribe( logo => { this.logos = logo; } );
    
    let slider = this.service.content(this.user.JWTGET({ slug: "slider_home" })).subscribe(response => {
      if (response.status) { 
        this.sharedService.handleSliderHome(response.Contents)
      }
    });

    let destacados = this.service.content(this.user.JWTGET({ slug: "destacados_home" })).subscribe(response => {
      if (response.status) {
        this.destacados = response.Contents;
      }
    });

    let logos = this.service.content(this.user.JWTGET({ slug: "logos" })).subscribe(response =>  {
      if (response.status) {
        this.sharedService.handleLogos(response.Contents);
      }
    });

    this.service.content(this.user.JWTGET({ slug: "noticias" })).subscribe(response => {
      if (response.status) {
        this.data = response.Contents;
      }
    });

  }

  saveAnswEditable(anw){
    var data = anw; 
    var param = this.user.serialize(data);
    this.autodiagnosticService.put( param, '/Contents/' , this.user.JWTPUT()).subscribe((response) => {
      if(response.status){
        this.alertService.success("Atualização Bancária.")
      }else{
        this.alertService.error("O banco não pode ser atualizado.")
      }
    });
   }


  saveAnswEditableCustom(field){
    
        var data = field; 
        var param = this.user.serialize(data);
        this.autodiagnosticService.put( param, '/Contents/fields' , this.user.JWTPUT()).subscribe((response) => {
          if(response.status){
            this.alertService.success("Atualização de Conteúdo.")
          }else{
            this.alertService.error("O conteúdo não pode ser Atualizado.")
          }
        });
    
  }

  handleImage(event, idInput, item){
    
    let formData = new FormData();
    let files = this.elem.nativeElement.querySelector("#guide-"+idInput).files;
    if(files.length <= 0){

      this.alertService.error("Por favor, mude a imagem.");

    }else{

      var reader = new FileReader();
      reader.readAsDataURL(files[0]);
      reader.onload = () => {
        this.elem.nativeElement.querySelector("#img-"+item.id).setAttribute('src', reader.result);
      };

      formData.append('id', item.id);
      formData.append('title', item.title);
      formData.append('body', item.body);
      formData.append('custom['+item.fields[0].id+']', files[0], files[0].name);
      this.file.updateUploadR('/contents/updateSlider', formData).subscribe( res => {
        if(res.status){ 
         formData = new FormData();
         this.alertService.success("Mudança bancária.");
        }else{
         this.alertService.success("O banco não pode ser alterado.");
        }
      });

    }
    
 
  }


  showSure(id){
    let disposable = this.dialogService.addDialog(AcceptComponent, {
      title: 'Excluir Banners',
      message: 'Você tem certeza de executar a seguinte ação.'
    }).subscribe((isConfirmed) => {
        //We get dialog result
        if (isConfirmed) {
          this.deleteContent(id);
        } 
     });

  }
  
  showSureLogos(id){
    let disposable = this.dialogService.addDialog(AcceptComponent, {
      title: 'Excluir logotipo',
      message: 'Você tem certeza de executar a seguinte ação.'
    }).subscribe((isConfirmed) => {
        //We get dialog result
        if (isConfirmed) {
          this.deleteContentLogos(id);
        } 
     });
  }

  showConfirm() {
    let disposable = this.dialogService.addDialog(ModalBannersComponent, {
      title: 'Adicionar Banner',
      message: 'Seção para criar',
      banner : []
    }).subscribe((isConfirmed) => {
        //We get dialog result
        if (isConfirmed) {
        }
        else {
        }
      });
   }

  showConfirmNoticia(){
    let disposable = this.dialogService.addDialog(ModalNoticiasComponent, {
      title: 'Adicionar Noticia',
      message: 'Noticia para criar',
      banner : []
    }).subscribe((isConfirmed) => {
        //We get dialog result
        if (isConfirmed) {
        }
        else {
        }
    });
  }

  showUpdate(id){

    let slider = this.service.content(this.user.JWTGET({ id: id })).subscribe(response => {
      if (response.status) { 
        let disposable = this.dialogService.addDialog(ModalBannersComponent, {
          title: 'Atualização de Banner',
          message: 'Seção para criar',
          banner: response.Contents
        }).subscribe((isConfirmed) => {
            //We get dialog result
            if (isConfirmed) { 
            }
            else {
            }
        });
      }
    });
  }


  deleteContent(id){
    this.service.delete(this.user.JWTGET({ id: id })).subscribe( (response) => {
      if(response.status){
        this.alertService.success("Banner Removido.");
        this.service.content(this.user.JWTGET({ slug: "slider_home" })).subscribe(response => {
          if (response.status) {
            this.sharedService.handleSliderHome(response.Contents);
          }
        });
      }else{
        this.alertService.error("O banner não pode ser excluído.")
      }
    }); 
  }


  deleteContentLogos(id){
    this.service.delete(this.user.JWTGET({ id: id })).subscribe( (response) => {
      if(response.status){
        this.alertService.success("Exclusão do Logotipo.");
        this.service.content(this.user.JWTGET({ slug: "logos" })).subscribe(response => {
          if (response.status) {
            this.sharedService.handleLogos(response.Contents);
          }
        });
      }else{
        this.alertService.error("O logotipo não pode ser excluído.")
      }
    }); 
  }


  DropNewOrder(slider){
    
    let newarray = [];
    for (let i in slider) {
      newarray[parseInt(slider[i].id)] = i; 
    }
    let param = this.user.serialize(newarray);
    this.service.putOrder( param , this.user.JWTPUT()).subscribe((response) => {
      if(response.status){
        this.alertService.success("Atualização do peso da bandeira.");
      }else{
        this.alertService.error("O banner não pode ser atualizado.")
      }
    })

  }

  saveEditable(value) {
    
  }


  saveDest(form){

    if(form.valid){
      var param = this.user.serialize(form.value);
      this.service.put( param, this.user.JWTPUT()).subscribe((response) => {
        if(response.status){
          this.alertService.success("Atualização de recursos.")
        }else{
          this.alertService.error("O recurso não pode ser atualizado.")
        }
      });
    }

  }

  newLogo(){
    
    let disposable = this.dialogService.addDialog(LogoModalComponent, {
      title: 'Adicionar Marca',
      message: 'Seção para criar.',
      logo : []
    }).subscribe((isConfirmed) => {
        //We get dialog result
        if (isConfirmed) {
        } 
        else {
        }
    });

  }

  showLogoUpdate(id){
    
        let logo = this.service.content(this.user.JWTGET({ id: id })).subscribe(response => {
          if (response.status) { 
            let disposable = this.dialogService.addDialog(LogoModalComponent, {
              title: 'Atualizar Marca',
              message: 'Seção para definir mudanças de uma marca.',
              logo: response.Contents
            }).subscribe((isConfirmed) => {
                //We get dialog result
                if (isConfirmed) {      
                }
                else {      
                }
            });
          }
        });
    
    }

    saveContent(item){
    
      var param = this.user.serialize(item);
      this.service.put( param, this.user.JWTPUT()).subscribe((response) => {
        if(response.status){
          this.alertService.success("Content update.")
        }else{
          this.alertService.error("Content can't be update.")
        }
      });
  
    }


}
