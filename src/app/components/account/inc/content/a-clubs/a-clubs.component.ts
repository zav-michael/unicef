import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { environment } from './../../../../../../environments/environment';
import { AlertService } from './../../../../../Services/alert.service';
import { SharedService } from './../../../../../Services/shared.service';
import { AccountService } from './../../../../../Services/account.service';
import { AutodiagnosticService } from './../../../../../Services/autodiagnostic.service';
import { UserService } from './../../../../../Services/user.service';
import { Pagination, Page } from './../../../../../models/pagination.model';
import { Program, ProgramRequired } from './../../../../../models/program.model';
import { FileUploaderService } from './../../../../../Services/file-uploader.service';
import { ContentService } from './../../../../../Services/content.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';

export class Filtro {
  city : string;
  name : string;
  program : string;
  compromiso : string;
  page: number;
}

@Component({
  selector: 'app-a-clubs',
  templateUrl: './a-clubs.component.html',
  styleUrls: ['./a-clubs.component.css'],
  providers: [AccountService, UserService, AutodiagnosticService, Pagination]
})

export class AClubsComponent implements OnInit {


  compromisos: any;
  filters: Filtro;
  clubs: any;
  pagination: Pagination;
  public currentPage:number = 1;
  apiUrl: string;


  constructor(private contentService: ContentService, private service: AccountService, private user: UserService,
    private sharedService:SharedService, private autodiagnosticService: AutodiagnosticService,
    private alertService: AlertService, private elem: ElementRef) 
  {
    this.filters = new Filtro();
    this.clubs = [];
    this.pagination = new Pagination();
    this.pagination.totalItems = 1;
    this.pagination.maxSize = 30;
    this.apiUrl = environment.api;
    this.sharedService.handlePagination(this.pagination);
  }
    
  ngOnInit() {
    this.loadContent();
    this.filters.city = "";
    this.filters.name = "";
    this.filters.compromiso = "";
    this.filters.program = "";
    this.filters.page = 1;
    this.pagination.totalItems = 0;
    this.sharedService.handlePagination(this.pagination);
  }


  loadContent() 
  {
    this.contentService.getUri('clubs/filters', this.user.JWTGET()).subscribe( response => {

      if(response.status){
        this.compromisos = response.filters.compromisso;
             
      }      
    });
  }

  reloadPagination(response)
  {

    this.pagination.maxSize = response.filters.items;
    this.pagination.totalItems = this.pagination.maxSize;
    this.pagination.items = response.filters.pagination;
    this.sharedService.handlePagination(this.pagination);

  }
  

  filterRegion(event)
  {
    this.filters.city = event.target.value;
    this.goFilter();
  } 

  filterName(event)
  {
    this.filters.name = event.target.value;
    this.goFilter();
  } 

  filterPrograma(event)
  {
    this.filters.program = event.target.value;
    this.goFilter();
  }

  filterCompromisos(event)
  {
    this.filters.compromiso = event.target.value;
    this.goFilter();
  }


  save(){
    this.goFilter();
  }

  goFilter(reload:boolean = true)
  {
    var data = JSON.stringify(this.filters);
    this.contentService.sendContent('Clubs/filters', data).subscribe( response => {
        if(response.status)
        {
          this.clubs = response.filters.items;
          if(reload){
            this.reloadPagination(response);
          }   
        }
    });
  }

  public setPage(pageNo:number):void {
    this.currentPage = pageNo;
    
  }

  public pageChanged(event:any):void {
    this.filters.page = this.currentPage;
    this.goFilter(false); 
  }

  public changeCollapse(index){
    let flecha
    flecha = $('#flecha-'+index);
    flecha.toggleClass("arriba abajo");
  }

}
