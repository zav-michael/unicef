import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AClubsComponent } from './a-clubs.component';

describe('AClubsComponent', () => {
  let component: AClubsComponent;
  let fixture: ComponentFixture<AClubsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AClubsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AClubsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
