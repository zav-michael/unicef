import { Component, OnInit } from '@angular/core';

import { Pagination, Page } from './../../models/pagination.model';

import { PagerService } from './../../Services/pager.service';
import * as _ from 'underscore';

@Component({
  selector: 'app-resource-content',
  templateUrl: './resource-content.component.html',
  styleUrls: ['./resource-content.component.css']
})
export class ResourceContentComponent implements OnInit {

   filtered;
    // pager object
    pager: any = {};
    // paged items
    pagedItems: any[];


  documents = [
    {
      'title': 'Ayyyyyrtigo Proteção Integral Crianças Adolescentes Empreendimentos FGV Direito2013',
      'type': 'PDF',
      'size': '130Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    },
    {
      'title': 'Otro documento No 2',
      'type': 'PDF',
      'size': '140Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    },
    {
      'title': 'Btro documento No 3',
      'type': 'PDF',
      'size': '100Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    },
    {
      'title': 'Otro documento No 4',
      'type': 'PDF',
      'size': '230Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    }
    ,
    {
      'title': 'Otro documento No 5',
      'type': 'PDF',
      'size': '140Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    },
    {
      'title': 'Otro documento No 6',
      'type': 'PDF',
      'size': '100Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    },
    {
      'title': 'Ctro documento No 7',
      'type': 'PDF',
      'size': '230Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    }
    ,
    {
      'title': 'Otro documento No 8',
      'type': 'PDF',
      'size': '140Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    },
    {
      'title': 'Otro documento No 9',
      'type': 'PDF',
      'size': '100Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    },
    {
      'title': 'Otro documento No 10',
      'type': 'PDF',
      'size': '230Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    }
    ,
    {
      'title': 'Dtro documento No 5',
      'type': 'PDF',
      'size': '140Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    },
    {
      'title': 'Otro documento No 6',
      'type': 'PDF',
      'size': '100Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    },
    {
      'title': 'Otro documento No 7',
      'type': 'PDF',
      'size': '230Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    }
    ,
    {
      'title': 'Otro documento No 8',
      'type': 'PDF',
      'size': '140Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    },
    {
      'title': 'Otro documento No 9',
      'type': 'PDF',
      'size': '100Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    },
    {
      'title': 'Otro documento No 10',
      'type': 'PDF',
      'size': '230Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    }
    ,
    {
      'title': 'Otro documento No 5',
      'type': 'PDF',
      'size': '140Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    },
    {
      'title': 'DIREITOS DA CRIANÇAS E DO ADOLESCENTE',
      'type': 'PDF',
      'size': '100Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    },
    {
      'title': 'Otro documento No 7',
      'type': 'PDF',
      'size': '230Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    }
    ,
    {
      'title': 'DIREITOS DA CRIANÇAS E DO ADOLESCENTE',
      'type': 'PDF',
      'size': '140Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    },
    {
      'title': 'Otro documento No 9',
      'type': 'PDF',
      'size': '100Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    },
    {
      'title': 'DIREITOS DA CRIANÇAS E DO ADOLESCENTE',
      'type': 'PDF',
      'size': '230Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    }
    ,
    {
      'title': 'Otro documento No 5',
      'type': 'PDF',
      'size': '140Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    },
    {
      'title': 'Otro documento No 6',
      'type': 'PDF',
      'size': '100Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    },
    {
      'title': 'DIREITOS DA CRIANÇAS E DO ADOLESCENTE',
      'type': 'PDF',
      'size': '230Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    }
    ,
    {
      'title': 'Otro documento No 8',
      'type': 'PDF',
      'size': '140Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    },
    {
      'title': 'Otro documento No 9',
      'type': 'PDF',
      'size': '100Kb',
      'link': 'https://www.uam./ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    },
    {
      'title': 'Otro documento No 10',
      'type': 'PDF',
      'size': '230Kb',
      'link': 'https://www.uam.es/personal_pdi/ciencias/fchamizo/asignaturas/quim1314/resumen03.pdf'
    }
  ];




  onChangeSortMethod(key) {
    switch (key) {
        case 1: {

            break;
        }
        case 2: {

            break;
        }
        case 3: {

            break;
        }
        case 4: {

          break;
      }
    }

}





setPage(page: number) {
  if (page < 1 || page > this.pager.totalPages) {
      return;
  }

  // get pager object from service
  this.pager = this.pagerService.getPager(this.documents.length, page);

  // get current page of items
  this.pagedItems = this.documents.slice(this.pager.startIndex, this.pager.endIndex + 1);
}



  //sorting
  key: string = 'DIREITOS DA CRIANÇAS E DO ADOLESCENTE';
  reverse: boolean = false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }
  p: number = 1;


  constructor(private pagerService: PagerService) {




   }

  ngOnInit() {

    this.setPage(1);

  }










}
