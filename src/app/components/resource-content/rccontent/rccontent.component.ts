import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-rccontent',
  templateUrl: './rccontent.component.html',
  styleUrls: ['./rccontent.component.css']
})
export class RccontentComponent implements OnInit {


  documents = [
    {
      'title': 'Artigo Proteção Integral Crianças Adolescentes Empreendimentos FGV Direito2013',
      'type': 'PDF',
      'size': '130Kb'
    },
    {
      'title': 'Otro documento No 2',
      'type': 'PDF',
      'size': '140Kb'
    },
    {
      'title': 'Otro documento No 3',
      'type': 'PDF',
      'size': '100Kb'
    },
    {
      'title': 'Otro documento No 4',
      'type': 'PDF',
      'size': '230Kb'
    }
    ,
    {
      'title': 'Stro documento No 5',
      'type': 'PDF',
      'size': '140Kb'
    },
    {
      'title': 'Otro documento No 6',
      'type': 'PDF',
      'size': '100Kb'
    },
    {
      'title': 'Otro documento No 7',
      'type': 'PDF',
      'size': '230Kb'
    }
    ,
    {
      'title': 'Otro documento No 8',
      'type': 'PDF',
      'size': '140Kb'
    },
    {
      'title': 'Btro documento No 9',
      'type': 'PDF',
      'size': '100Kb'
    },
    {
      'title': 'Otro documento No 10',
      'type': 'PDF',
      'size': '230Kb'
    }
    ,
    {
      'title': 'Rtro documento No 5',
      'type': 'PDF',
      'size': '140Kb'
    },
    {
      'title': 'Otro documento No 6',
      'type': 'PDF',
      'size': '100Kb'
    },
    {
      'title': 'Otro documento No 7',
      'type': 'PDF',
      'size': '230Kb'
    }
    ,
    {
      'title': 'Otro documento No 8',
      'type': 'PDF',
      'size': '140Kb'
    },
    {
      'title': 'Otro documento No 9',
      'type': 'PDF',
      'size': '100Kb'
    },
    {
      'title': 'Otro documento No 10',
      'type': 'PDF',
      'size': '230Kb'
    }
    ,
    {
      'title': 'Otro documento No 5',
      'type': 'PDF',
      'size': '140Kb'
    },
    {
      'title': 'Otro documento No 6',
      'type': 'PDF',
      'size': '100Kb'
    },
    {
      'title': 'Otro documento No 7',
      'type': 'PDF',
      'size': '230Kb'
    }
    ,
    {
      'title': 'Otro documento No 8',
      'type': 'PDF',
      'size': '140Kb'
    },
    {
      'title': 'Otro documento No 9',
      'type': 'PDF',
      'size': '100Kb'
    },
    {
      'title': 'Otro documento No 10',
      'type': 'PDF',
      'size': '230Kb'
    }
    ,
    {
      'title': 'Otro documento No 5',
      'type': 'PDF',
      'size': '140Kb'
    },
    {
      'title': 'Otro documento No 6',
      'type': 'PDF',
      'size': '100Kb'
    },
    {
      'title': 'Otro documento No 7',
      'type': 'PDF',
      'size': '230Kb'
    }
    ,
    {
      'title': 'Otro documento No 8',
      'type': 'PDF',
      'size': '140Kb'
    },
    {
      'title': 'Otro documento No 9',
      'type': 'PDF',
      'size': '100Kb'
    },
    {
      'title': 'Otro documento No 10',
      'type': 'PDF',
      'size': '230Kb'
    }
  ];



  constructor() { }

  ngOnInit() {


  }


  //sorting
  key: string = 'title';
  reverse: boolean = false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }
  p: number = 1;




}
