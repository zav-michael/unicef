import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RccontentComponent } from './rccontent.component';

describe('RccontentComponent', () => {
  let component: RccontentComponent;
  let fixture: ComponentFixture<RccontentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RccontentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RccontentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
