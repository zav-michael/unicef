import { Component, OnInit, ElementRef } from '@angular/core';
import { NgClass } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { environment } from './../../../environments/environment';
import { AccountService } from './../../Services/account.service';
import { UserService } from './../../Services/user.service';

@Component({
  selector: 'app-account-2',
  templateUrl: './account-2.component.html',
  styleUrls: ['./account-2.component.css'],
  providers: [AccountService, UserService]
})
export class Account2Component implements OnInit {


  circle: Array<{name: string, x: number, y: number}> = [];
  copies = [];
  stop = false;
  club:any;
  routerLink;


  constructor(private elem: ElementRef, private account: AccountService, private user:UserService) { }

  ngOnInit() {
    window.scrollTo(0,0);
    
    this.circle.push({name: 'Autodiagnóstico' , x: 20 , y: 150,});
    this.circle.push({name: 'Orientações para o Clube' , x: -30 , y: 130 });
    this.circle.push({name: 'Plano de Ações e Metas' , x: 180 , y: 120 });
    this.circle.push({name: 'Materiais de Apoio' , x: 120 , y: 140 });
    this.circle.push({name: 'Painel de Monitoramento' , x: -10 , y: 110 });
    this.circle.push({name: 'Banco de Boas Práticas' , x: 0 , y: 0 });

    this.copies = [
      { text : ' para visualizar essa seção é necessário estar inscrito no programa' , action: '/autodiagnostic', btn: 'Você está interessado no programa?' },
      { text : ' para visualizar essa seção é necessário estar inscrito no programa' , action: '/autodiagnostic/recommendations', btn: 'Você está interessado no programa?' },
      { text : ' para visualizar essa seção é necessário estar inscrito no programa' , action: '/autodiagnostic/plan', btn: 'Você está interessado no programa?' },
      { text : ' para visualizar essa seção é necessário estar inscrito no programa' , action: '/autodiagnostic/material', btn: 'Você está interessado no programa?' },
      { text : ' para visualizar essa seção é necessário estar inscrito no programa' , action: '/autodiagnostic/monitoring', btn: 'Você está interessado no programa?' },
      { text : ' para visualizar essa seção é necessário estar inscrito no programa' , action: '/autodiagnostic/practices', btn: 'Você está interessado no programa?' }
    ];

    this.account.club(this.user.JWTGET())
    .subscribe( response => {
      this.club = response.Club;
    })

  }

  mouseEnter = function (event) {
    var target = event.target || event.srcElement || event.currentTarget;
    var parent = target.parentNode;
    parent.querySelector('svg').classList.add("change-fill");
    var x = event.pageX - target.getAttribute('data-x');
    var y =  event.pageY - target.getAttribute('data-y');
    if(target.getAttribute('data-pos') != 3){
      var myElements = this.elem.nativeElement.querySelectorAll('.triangulo-s');
      for (var i = 0; i < myElements.length; i++) {
        myElements[i].style.backgroundColor = '#7097c4'
      }
    }
    this.routerLink = target.getAttribute('data-action');
    this.elem.nativeElement.querySelector('.action-circle > div > div').innerHTML = target.getAttribute('data-text');
    this.elem.nativeElement.querySelector('.action-circle > div > a').innerHTML = target.getAttribute('data-button');
    //this.elem.nativeElement.querySelector('.action-circle > div > a').href = '#inscripcion';
    //this.elem.nativeElement.querySelector('.action-circle > div > a').setAttribute("data-toggle", "modal");
    //this.elem.nativeElement.querySelector('.action-circle > div > a').removeAttribute("ng-reflect-router-link");
    if(target.getAttribute('data-pos') != 0 && target.getAttribute('data-pos') != 1){
      this.elem.nativeElement.querySelector('.action-circle').style.display = 'block';
    }
    this.elem.nativeElement.querySelector('.action-circle').style.left = '830px';
    this.elem.nativeElement.querySelector('.action-circle').style.top = '490px';
  }

  mouseLeave = function(event) {
    var target = event.target || event.srcElement || event.currentTarget;
    var parent = target.parentNode;
    parent.querySelector('svg').classList.remove("change-fill");
    this.elem.nativeElement.querySelector('.action-circle').style.display = 'none'    
  }

  mouseActionLeave = function(event) {
    
    this.elem.nativeElement.querySelector('.action-circle').style.display = 'none'

  
  }

  mouseActionEnter = function(event) {
     this.stop = true;
     this.elem.nativeElement.querySelector('.action-circle').style.display = 'block'; 
    
  }

}
