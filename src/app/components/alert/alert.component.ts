import { Component, OnInit } from '@angular/core';
import { AlertService } from "./../../Services/alert.service";
import { NotificationsService } from "angular2-notifications";

import { Alert, AlertType } from './../../models/alerts.model';


@Component({
   moduleId: module.id,
   selector: 'app-alert',
   templateUrl: 'alert.component.html'
})

export class AlertComponent {
   alerts: Alert[] = [];
   options:any = {position: ["bottom", "right"], timeOut: 5000, lastOnBottom: true};

   constructor(private alertService: AlertService, private notification: NotificationsService) { }

   ngOnInit() {
       this.alertService.getAlert().subscribe((alert: Alert) => {
           if (!alert) {
               this.alerts = [];
               return;
           }

          this.show(alert);
       });
   }

   show(alert: Alert) {
       if (!alert) {
           return;
       }
       switch (alert.type) {
           case AlertType.Success:
              this.notification.success(alert.title,alert.message);
              break;
           case AlertType.Error:
             this.notification.error(alert.title,alert.message);
             break;
           case AlertType.Info:
             this.notification.info(alert.title,alert.message);
             break;
           case AlertType.Warning:
             this.notification.warn(alert.title,alert.message);
             break;
       }
   }
}