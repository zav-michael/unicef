import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JogueLimpoComponent } from './jogue-limpo.component';

describe('JogueLimpoComponent', () => {
  let component: JogueLimpoComponent;
  let fixture: ComponentFixture<JogueLimpoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JogueLimpoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JogueLimpoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
