import {Component, ElementRef, Input, ViewChild, OnInit} from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { AlertService } from './../../Services/alert.service';
import { SharedService } from './../../Services/shared.service';
import { AccountService } from './../../Services/account.service';
import { UserService } from './../../Services/user.service';
import { FileUploaderService } from './../../Services/file-uploader.service';
import { environment } from './../../../environments/environment';

export class Logo {
    title: string;
    body: string;
    image1: any;
    idImage1:number;    
    contentId: number;
    constructor(){
        this.idImage1 = 0;
    }
}

export class LogoRequired {
    title: boolean;
    body: boolean;
    image1: boolean;
    constructor(){
        this.title = true;
        this.body = true;
        this.image1 = true;  
    }
}

export class LogoO {
    title: string;
    body: string;
    id:number;
    contentId: number;
    idImage1:number;
    constructor(){

    }
}

export interface ConfirmModel {
    title:string;
    message:string;
    logo: any;
}

  @Component({
    moduleId: module.id,
    selector: 'logomodal',
    templateUrl: './logo-component.html',
    providers : [ AccountService, UserService, FileUploaderService ]
  })

export class LogoModalComponent extends DialogComponent<ConfirmModel,  boolean> implements ConfirmModel, OnInit {  
    
    title: string;
    message: string;
    required: any;
    logoForm: any;
    update:boolean;
    logo: any = [];
  
    constructor(dialogService: DialogService, private account: AccountService, private elem: ElementRef, 
                 private alertService:AlertService, private user: UserService,
                 private sharedService: SharedService, private file: FileUploaderService) {
      super(dialogService);    
  
      
    }

    ngOnInit() {
        
        this.update = false;
        this.logoForm = new Logo();
        this.required = new LogoRequired();
        this.logoForm.contentId = 3;

        if(this.logo.length > 0)
        {

            this.update = true;
            this.logoForm = new LogoO();
            this.logoForm.contentId = this.logo[0].content_type_id;
            this.logoForm.id = this.logo[0].id;      
            this.logoForm.title = this.logo[0].title;
            this.logoForm.body = this.logo[0].body;
            this.logoForm.idImage1 = this.logo[0].fields[0].id;
      
            this.required.image1 = false;
      
            this.elem.nativeElement.querySelector("#imageL1-z .instrucciones").style.opacity = 0;
            this.elem.nativeElement.querySelector("#imageL1-z img").style.display = "block";
            this.elem.nativeElement.querySelector("#imageL1-z img").setAttribute("src", environment.api+this.logo[0].fields[0].value); 
            


        }

    }

    public handleImage(event,id){

        if(this.update)
        {
           this.required.image1 = true;
        }
    
        if(event.target.files.length > 0){
          let file = event.target.files[0];
          var reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = () => {
            this.elem.nativeElement.querySelector(id+" img").setAttribute('src', reader.result); 
            this.elem.nativeElement.querySelector(id+" img").style.display = 'block';
          };
          this.elem.nativeElement.querySelector(id+" .instrucciones").style.opacity = 0;
          
        }else{      
    
          this.elem.nativeElement.querySelector(id+" .instrucciones").style.opacity = 1;
          this.elem.nativeElement.querySelector(id+" img").setAttribute("src", "");
          this.elem.nativeElement.querySelector(id+" img").style.display = "none";
        }
        
      }

      saveLogo(): void{
        
        if(this.update){
            this.updateR();
        }else{
            this.insert();
        }
        

      }

      updateR() : void {
          
        let formData = new FormData();
        let files1 = this.elem.nativeElement.querySelector("#image1_l").files;
    
        formData.append('title', this.elem.nativeElement.querySelector("#title_l").value);
        formData.append('body', this.elem.nativeElement.querySelector("#body_l").value);
        formData.append('id', this.elem.nativeElement.querySelector("#idLogo").value);
        formData.append('content_type_id', this.elem.nativeElement.querySelector("#content_type_l").value);
    
        if(files1.length > 0){
          let id1 = this.elem.nativeElement.querySelector("#image1_l").getAttribute('data-id');
          formData.append('custom['+id1+']', files1[0], files1[0].name);
        }
    
        this.file.updateUpload(formData).subscribe( res => {
          if(res.status){
            formData = new FormData();
            this.elem.nativeElement.querySelector("#logoForm").reset();
            this.close();
            this.alertService.success("Logo Update.");
            this.account.content(this.user.JWTGET({ slug: "logos" })).subscribe(response => {
              if (response.status) {
                this.sharedService.handleLogos(response.Contents);
              }
            });
          }else{
            this.alertService.error("Error Update.");
          }
        })  


      }

      insert() : void {
        let formData = new FormData();
        let files1 = this.elem.nativeElement.querySelector("#image1_l").files;
        formData.append('title', this.elem.nativeElement.querySelector("#title_l").value );
        formData.append('body', this.elem.nativeElement.querySelector("#body_l").value );
        formData.append('content_type_id', this.elem.nativeElement.querySelector("#content_type_l").value);
        formData.append('custom[]', files1[0], files1[0].name);
        this.file.upload(formData).subscribe( res => {
          if(res.status){
            formData = new FormData();
            this.elem.nativeElement.querySelector("#logoForm").reset();
            this.close();
            this.alertService.success("Logo add.");
            this.account.content(this.user.JWTGET({ slug: "logos" })).subscribe(response => {
              if (response.status) {
                this.sharedService.handleLogos(response.Contents);
              };
            });
          }else{
            this.alertService.error("Error.");
          }
        }) 
      }

}