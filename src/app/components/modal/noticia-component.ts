import {Component, ElementRef, Input, ViewChild, OnInit} from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { Noticia, NoticiaRequired } from './../../models/banners.model';
import { AlertService } from './../../Services/alert.service';
import { SharedService } from './../../Services/shared.service';
import { AccountService } from './../../Services/account.service';
import { AutodiagnosticService } from './../../Services/autodiagnostic.service';
import { UserService } from './../../Services/user.service';
import { FileUploaderService } from './../../Services/file-uploader.service';
import { environment } from './../../../environments/environment';

export interface ConfirmModel {
  title:string;
  message:string;
  banner: any;
}

@Component({
    moduleId: module.id,
    selector: 'confirm',
    templateUrl: './noticias-component.html',
    providers : [AccountService, UserService, AutodiagnosticService, FileUploaderService ]
})


export class ModalNoticiasComponent extends DialogComponent<ConfirmModel,  boolean> implements ConfirmModel, OnInit {  

  noticiaForm:any;
  title: string;
  message: string;
  banner: any = [];
  required: any;
  update:boolean;

  constructor(dialogService: DialogService, private account: AccountService, private elem: ElementRef, 
               private alertService:AlertService, private user: UserService,
               private sharedService: SharedService, private file: FileUploaderService, private autodiagnosticService: AutodiagnosticService) {
    super(dialogService);    

    
  }
  
  confirm() {

    this.result = true;
    this.close(); 
  }
  
  ngOnInit() {

    
    this.noticiaForm = new Noticia();
    this.required = new NoticiaRequired();
    this.noticiaForm.contentId = 51;
   
  }


  public handleImage(event,id){
    
    if(this.update){
      if(id == "#image1-z"){
        this.required.image1 = true;
      }
    }

    if(event.target.files.length > 0){
      let file = event.target.files[0];
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.elem.nativeElement.querySelector(id+" img").setAttribute('src', reader.result); 
        this.elem.nativeElement.querySelector(id+" img").style.display = 'block';
      };
      this.elem.nativeElement.querySelector(id+" .instrucciones").style.opacity = 0;
      
    }else{      

      this.elem.nativeElement.querySelector(id+" .instrucciones").style.opacity = 1;
      this.elem.nativeElement.querySelector(id+" img").setAttribute("src", "");
      this.elem.nativeElement.querySelector(id+" img").style.display = "none";
    }
    
  }

  public save() : void {
    this.insert();  
  }


  public insert(): void {
    let formData = new FormData();
    let formDataCustom = new FormData();
    let files1 = this.elem.nativeElement.querySelector("#image1").files;
    formData.append('title', this.elem.nativeElement.querySelector("#title").value );
    formData.append('body', this.elem.nativeElement.querySelector("#body").value );
    var encontro = this.elem.nativeElement.querySelector("#title").value;
    var link = this.elem.nativeElement.querySelector("#body").value;
    formData.append('content_type_id', this.elem.nativeElement.querySelector("#content_type").value);
    formData.append('custom[]', files1[0], files1[0].name);
    this.file.upload(formData).subscribe( res => {console.log(res);
      if(res.status){

        var param:any = this.user.serialize({"custom_fields_id": 4,
        "content_id": res.Contents.id,
        "value": encontro,
        "title": 'Ver mais'
        });
        
        this.autodiagnosticService.post( param, '/Contents/fields' , this.user.JWTPUT()).subscribe((response) => {
          if(response.status){
            
            var param:any = this.user.serialize({"custom_fields_id": 2,
            "content_id": res.Contents.id,
            "value": encontro,
            "title": null
            });
            
            this.autodiagnosticService.post( param, '/Contents/fields' , this.user.JWTPUT()).subscribe((response) => {
              if(response.status){
                
                formData = new FormData();
                this.elem.nativeElement.querySelector("#noticiaForm").reset();
                this.close();
                this.alertService.success("Noticia add.");//Recargar noticias
                this.account.content(this.user.JWTGET({ slug: "noticias" })).subscribe(response => {
                  if (response.status) {
                    //this.sharedService.handleSliderHome(response.Contents);
                  };
                });

              }else{
                this.alertService.error("O conteúdo não pode ser Atualizado.")
              }
            });

          }else{
            this.alertService.error("O conteúdo não pode ser Atualizado.")
          }
        });
      }else{
        this.alertService.error("Error.");
      }
    });  
  }

}