import {Component, ElementRef, Input, ViewChild, OnInit} from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { Banners, BannersO, BannersRequired } from './../../models/banners.model';
import { AlertService } from './../../Services/alert.service';
import { SharedService } from './../../Services/shared.service';
import { AccountService } from './../../Services/account.service';
import { UserService } from './../../Services/user.service';
import { FileUploaderService } from './../../Services/file-uploader.service';
import { environment } from './../../../environments/environment';

export interface ConfirmModel {
  title:string;
  message:string;
  banner: any;
}

@Component({
    moduleId: module.id,
    selector: 'confirm',
    templateUrl: './banners-component.html',
    providers : [Banners, BannersO, AccountService, UserService, FileUploaderService, BannersRequired ]
})


export class ModalBannersComponent extends DialogComponent<ConfirmModel,  boolean> implements ConfirmModel, OnInit {  

  bannersForm:any;
  title: string;
  message: string;
  banner: any = [];
  required: any;
  update:boolean;

  constructor(dialogService: DialogService, private account: AccountService, private elem: ElementRef, 
               private alertService:AlertService, private user: UserService,
               private sharedService: SharedService, private file: FileUploaderService, private bannerRequired: BannersRequired) {
    super(dialogService);    

    
  }
  
  confirm() {

    this.result = true;
    this.close(); 
  }
  
  ngOnInit() {

    
    this.bannersForm = new Banners();
    this.required = new BannersRequired();
    this.update = false;
    this.bannersForm.contentId = 1;

    if(this.banner.length > 0){

      this.update = true;
      
      this.bannersForm = new BannersO();
      this.bannersForm.contentId = this.banner[0].content_type_id;
      this.bannersForm.id = this.banner[0].id;      
      this.bannersForm.title = this.banner[0].title;
      this.bannersForm.body = this.banner[0].body;
      this.bannersForm.idImage1 = this.banner[0].fields[0].id;
      this.bannersForm.idImage2 = this.banner[0].fields[1].id;

      this.required.image1 = false;
      this.required.image2 = false;

      this.elem.nativeElement.querySelector("#image1-z .instrucciones").style.opacity = 0;
      this.elem.nativeElement.querySelector("#image1-z img").style.display = "block";
      this.elem.nativeElement.querySelector("#image1-z img").setAttribute("src", environment.api+this.banner[0].fields[0].value); 
      this.elem.nativeElement.querySelector("#image2-z .instrucciones").style.opacity = 0;
      this.elem.nativeElement.querySelector("#image2-z img").style.display = "block";
      this.elem.nativeElement.querySelector("#image2-z img").setAttribute("src", environment.api+this.banner[0].fields[1].value); 

    }
   
  }


  public handleImage(event,id){
    
    if(this.update){
      if(id == "#image1-z"){
        this.required.image1 = true;
      }
      if(id == "#image2-z"){
        this.required.image2 = true;
      }
    }

    if(event.target.files.length > 0){
      let file = event.target.files[0];
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.elem.nativeElement.querySelector(id+" img").setAttribute('src', reader.result); 
        this.elem.nativeElement.querySelector(id+" img").style.display = 'block';
      };
      this.elem.nativeElement.querySelector(id+" .instrucciones").style.opacity = 0;
      
    }else{      

      this.elem.nativeElement.querySelector(id+" .instrucciones").style.opacity = 1;
      this.elem.nativeElement.querySelector(id+" img").setAttribute("src", "");
      this.elem.nativeElement.querySelector(id+" img").style.display = "none";
    }
    
  }

  public save() : void {
    if(this.update){
      this.updateRe();  
    }else{
      this.insert();  
    }
      
  }

  public updateRe():void{

    let formData = new FormData();
    let files1 = this.elem.nativeElement.querySelector("#image1").files;
    let files2 = this.elem.nativeElement.querySelector("#image2").files;

    formData.append('title', this.elem.nativeElement.querySelector("#title").value);
    formData.append('body', this.elem.nativeElement.querySelector("#body").value);
    formData.append('id', this.elem.nativeElement.querySelector("#idBanner").value);
    formData.append('content_type_id', this.elem.nativeElement.querySelector("#content_type").value);

    if(files1.length > 0){
      let id1 = this.elem.nativeElement.querySelector("#image1").getAttribute('data-id');
      formData.append('custom['+id1+']', files1[0], files1[0].name);
    }
    if(files2.length > 0){
      let id2 = this.elem.nativeElement.querySelector("#image2").getAttribute('data-id');
      formData.append('custom['+id2+']', files2[0], files2[0].name);
    }

    this.file.updateUpload(formData).subscribe( res => {
      if(res.status){
        formData = new FormData();
        this.elem.nativeElement.querySelector("#bannerForm").reset();
        this.close();
        this.alertService.success("Banner Update.");
        this.account.content(this.user.JWTGET({ slug: "slider_home" })).subscribe(response => {
          if (response.status) {
            this.sharedService.handleSliderHome(response.Contents);
          };
        });
      }else{
        this.alertService.error("Error Update.");
      }
    })  


  }

  public insert(): void {
    let formData = new FormData();
    let files1 = this.elem.nativeElement.querySelector("#image1").files;
    let files2 = this.elem.nativeElement.querySelector("#image2").files;
    formData.append('title', this.elem.nativeElement.querySelector("#title").value );
    formData.append('body', this.elem.nativeElement.querySelector("#body").value );
    formData.append('content_type_id', this.elem.nativeElement.querySelector("#content_type").value);
    formData.append('custom[]', files1[0], files1[0].name);
    formData.append('custom[]', files2[0], files2[0].name);
    this.file.upload(formData).subscribe( res => {
      if(res.status){
        formData = new FormData();
        this.elem.nativeElement.querySelector("#bannerForm").reset();
        this.close();
        this.alertService.success("Banner add.");
        this.account.content(this.user.JWTGET({ slug: "slider_home" })).subscribe(response => {
          if (response.status) {
            this.sharedService.handleSliderHome(response.Contents);
          };
        });
      }else{
        this.alertService.error("Error.");
      }
    })  
  }

}