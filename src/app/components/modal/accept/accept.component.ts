import {Component, ElementRef, Input, ViewChild, OnInit} from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal"; 
import { Banners } from './../../../models/banners.model';
import { AlertService } from './../../../Services/alert.service';
import { SharedService } from './../../../Services/shared.service';
import { AccountService } from './../../../Services/account.service';
import { UserService } from './../../../Services/user.service';
import { FileUploaderService } from './../../../Services/file-uploader.service';

export interface ConfirmModel {
  title:string;
  message:string;
}

@Component({
  moduleId: module.id,
  selector: 'app-accept',
  templateUrl: './accept.component.html',
  styleUrls: ['./accept.component.css'],
  providers : [AccountService, UserService, FileUploaderService]
})

export class AcceptComponent extends DialogComponent<ConfirmModel,  boolean> implements ConfirmModel, OnInit {  

  title: string;
  message: string;

  constructor(dialogService: DialogService, private account: AccountService, private elem: ElementRef, 
               private alertService:AlertService, private user: UserService,
               private sharedService: SharedService, private file: FileUploaderService) {
    super(dialogService);
    
  }

  confirm() {
    
    this.result = true;
    this.close();
    
  }
      
  ngOnInit() {
        
       
  }

}
