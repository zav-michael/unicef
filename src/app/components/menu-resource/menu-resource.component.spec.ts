import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuResourceComponent } from './menu-resource.component';

describe('MenuResourceComponent', () => {
  let component: MenuResourceComponent;
  let fixture: ComponentFixture<MenuResourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuResourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuResourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
