import { Component, OnInit } from '@angular/core';
import { SenhaService } from './../../../Services/senha.service';
import { UserService } from "./../../../Services/user.service";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AlertService } from './../../../Services/alert.service';
import { AuthenticationService } from './../../../Services/authentication.service';
import { Http, Response } from '@angular/http';
import { SharedService } from './../../../Services/shared.service';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-senha-create',
  templateUrl: './senha-create.component.html',
  styleUrls: ['./senha-create.component.css'],
  providers: [SenhaService,UserService,AuthenticationService]
})
export class SenhaCreateComponent implements OnInit {

  returnUrl: string;
  model: any = {};
  matchE= false;
  token:string;
  passwordfields: boolean = false;
  closeRestore: boolean;


  
  constructor(    
    private http: Http,
    private userService: UserService,
    private senhaService: SenhaService,
    private route: ActivatedRoute,
    private alertService: AlertService,
    private router: Router,
    private authenticationService: AuthenticationService,
    private sharedService: SharedService) {
   }

  ngOnInit() {
    this.sharedService.currentcloseRestore.subscribe(closeRestore => this.closeRestore = closeRestore);
    this.sharedService.handleCloseRestore(true);
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }


  ngAfterViewInit () {
    
    this.route.queryParams.subscribe( 
      params => { 
        if(params['assign']){
          this.authenticationService.verifyRestoreToken( params['assign'] )
          .subscribe(
            data => {
              if( data && data.id){
                this.model.id = data.id;
                this.model.email = data.email;
                this.model.passwordToken = params['assign'];
                this.passwordfields = !this.passwordfields;
              }
            }
          )
        }
                  
      }
    );
    
  }

  regSenha()
  {

    console.log(this.model.optradio);
    if(this.model.csenha == this.model.senha)
    {
      this.authenticationService.createPasss(this.model.id, this.model.senha, this.model.csenha, this.model.passwordToken)

      .subscribe(
        data => {

          console.log("Contrasenña cambiada");

          this.alertService.success("Senha criada corretamente");

          this.authenticationService.login(this.model.email, this.model.senha)
            .subscribe(
              data => {
                if(data.contact_name){
                  //this.user = data;
                  this.sharedService.handleLogin( true );
                  //this.model.email = "";this.model.senha = "";
                  //window.location.href = '/jogue_limpo/minha-conta';
                  setTimeout(() => {
                    window.location.href = '/jogue_limpo/minha-conta';
                  }, 100);
                }
              },
              error => {
                let errorR = JSON.parse(error._body);
                this.alertService.error(errorR.error.message);
              }
            );
   
        },
        error => {
          //let data = JSON.parse(error._body);

          this.alertService.error("Erro, tente novamente")
          
        }
      );
    }else{
      this.matchE= true;
    }

  }

}
