import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SenhaCreateComponent } from './senha-create.component';

describe('SenhaCreateComponent', () => {
  let component: SenhaCreateComponent;
  let fixture: ComponentFixture<SenhaCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SenhaCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenhaCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
