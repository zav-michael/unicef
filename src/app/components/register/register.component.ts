import { Component, OnInit ,Input, Output} from '@angular/core';
import { SharedService } from './../../Services/shared.service';
import { RegistrationService } from './../../Services/registration.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService } from './../../Services/alert.service';

import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';


// tslint:disable-next-line:import-spacing
import { Club } from  './../../models/clubs.model';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  providers: [RegistrationService]
})


export class RegisterComponent implements OnInit {


  public loading: boolean = false;
  public maskCPE = [ /\d/, /\d/, /\d/,/\d/,/\d/,'-',/\d/, /\d/, /\d/]
  public maskCNPJ = [ /\d/, /\d/, '.', /\d/,/\d/,/\d/,'.',/\d/,/\d/,/\d/,'/', /\d/, /\d/, /\d/, /\d/,'-', /\d/, /\d/]
  public maskTelefone = ['(', /\d/, /\d/,')', /\d/,/\d/,/\d/,/\d/, /\d/,/\d/, /\d/, /\d/, /\d/]
  public maskCelular = ['(', /\d/, /\d/,')', /\d/,/\d/,/\d/,/\d/, /\d/,/\d/, /\d/, /\d/, /\d/]

  loggedIn: boolean = true;
  ufOptions = 
  ['AC','AM','AL',
   'AP','BA','CE',
   'DF','ES','GO',
   'MA','MG','MS',
   'MT','PA','PB',
   'PE','PI','PR',
   'RN','RJ','RO',
   'RR','RS','SC',
   'SE','SP','TO'];

  isCollapsed:boolean= false;
  model: any = {};
  registerClub: boolean;
  registerUser: boolean;
  registerFinish: boolean;
  imgsrc: string;
  imgSend: string;
  imgsrc2: string;
  stepsModel: any = [];
  dataString: string;
  Simage=false;
  SimageAlert=false;
  idClub: number;
  imgClub: string;
  matchE= false;
  public selectedOption: number = 1;

  filed; 
  constructor(

    private sharedService: SharedService,
    private http: Http,
    private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
    private registrationService: RegistrationService
  // tslint:disable-next-line:one-line
  ){}

  ngOnInit() {

    this.sharedService.currentCollapse.subscribe( isCollapsed => this.isCollapsed = isCollapsed );  

  
    this.Simage = false;
    this.imgsrc = 'assets/images/generales/escudo.svg';
    this.registerClub = true;
    this.registerUser = false;
    this.registerFinish = false;


 }

  reg = (nameCl: string, addressCl: string, cityCl: string, ufCl: number, cnpjCl: number, empQuantityCl: number, websiteCl: string) => {


    this.loading = true;
    this.registrationService.registrationClub(this.model.nameCl, this.model.addressCl, this.model.cityCl ,
                                              this.model.ufCl, this.model.cnpjCl,
                                              this.model.empQuantityCl, this.model.websiteCl,this.imgSend)
        .subscribe(
          data => {
            if (data.id) {
              console.log('id Club Obtenido ' + data.id);
              this.idClub = data.id;
              this.registerClub = false;
              this.registerUser = true;
              this.loading = false;
            }
            // tslint:disable-next-line:one-line
            else
            // tslint:disable-next-line:one-line
            {
              this.loading = false;
            }
          },
          error => {
            console.log(error);
            let er = JSON.parse(error._body);
            console.log(er.error);
            console.log(er.error.empQuantityCl);
            if(er.error != undefined && er.error.message != undefined && er.error.message.empQuantityCl != undefined){
              //this.empQuantityCl.invalid = true;
              //this.empQuantityCl.errors.noUnique = true;
              //console.log(this.empQuantityCl);
              alert(er.error.message.empQuantityCl);
            }
            this.loading = false;
          }
        );
    
  }





onChange(event) {
    // this.shield = event.target.files[0];
    console.log('onChange');
    this.filed = event.target.files[0];
    console.log(this.filed );
    this.stepsModel.push(this.filed[0]);
    const reader = new FileReader();
    reader.readAsDataURL(this.filed);
    reader.onloadend = (e) => {
      console.log("IMAGEN "+reader.result);
      this.imgsrc = reader.result;
      this.imgSend=reader.result;
      this.Simage=true;
      this.SimageAlert=false;
   };
}

// tslint:disable-next-line:max-line-length
regUser = (persona: string, cargo: string, area: string, telefono: string, celular: string, correo: string, optradio: number, correoValidar:string ) => {
  console.log(this.model.optradio);




  if(this.model.correoValidar == this.model.correo)
  {
    console.log("register == here");
    this.loading = true;
    this.matchE= false;
    // tslint:disable-next-line:max-line-length
          this.registrationService.registrationUser(this.model.persona, this.model.cargo, this.model.area , this.model.telefono, this.model.celular,
            this.selectedOption, this.model.correo, this.idClub, 3 )
              .subscribe(
                data => {

                  console.log("register =="+data);
                  if (data.contact_name) {

                    this.loading = false;

                  
                    this.registerUser = false;
                    this.registerFinish = true;
                  }
                
                },

              
              error => {
                console.log(error);
               // alert(error);
               let errorR = JSON.parse(error._body);
               this.alertService.error(errorR.error.message);
               this.loading = false;
              }
            );
          }
          else
          {

            this.loading = false;
            this.matchE= true;

          }


      }

  public goBack(){
    this.registerClub = true;
    this.registerUser = false;
  }

}
