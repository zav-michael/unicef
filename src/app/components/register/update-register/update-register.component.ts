import { Component, OnInit ,Input, Output} from '@angular/core';
import { SharedService } from './../../../Services/shared.service';
import { RegistrationService } from './../../../Services/registration.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService } from './../../../Services/alert.service';

import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';


// tslint:disable-next-line:import-spacing
import { Club } from  './../../../models/clubs.model';
import { User } from  './../../../models/users.model';
import { UserService } from '../../../Services/user.service';


@Component({
  selector: 'app-update-register',
  templateUrl: './update-register.component.html',
  providers: [RegistrationService, UserService]
})


export class UpdateRegisterComponent implements OnInit {


  public loading: boolean = false;
  public maskCPE = [ /\d/, /\d/, /\d/,/\d/,/\d/,'-',/\d/, /\d/, /\d/]
  public maskCNPJ = [ /\d/, /\d/, '.', /\d/,/\d/,/\d/,'.',/\d/,/\d/,/\d/,'/', /\d/, /\d/, /\d/, /\d/,'-', /\d/, /\d/]
  public maskTelefone = ['(', /\d/, /\d/,')', /\d/,/\d/,/\d/,/\d/, /\d/,/\d/, /\d/, /\d/, /\d/]
  public maskCelular = ['(', /\d/, /\d/,')', /\d/,/\d/,/\d/,/\d/, /\d/,/\d/, /\d/, /\d/, /\d/]

  loggedIn: boolean = true;
  ufOptions = 
  ['AC','AM','AL',
   'AP','BA','CE',
   'DF','ES','GO',
   'MA','MG','MS',
   'MT','PA','PB',
   'PE','PI','PR',
   'RN','RJ','RO',
   'RR','RS','SC',
   'SE','SP','TO'];

  isCollapsed:boolean= false;
  model: any = {};
  registerClub: boolean;
  registerUser: boolean;
  registerFinish: boolean;
  imgsrc: string;
  imgSend: string;
  imgsrc2: string;
  stepsModel: any = [];
  dataString: string;
  Simage=false;
  SimageAlert=false;
  idClub: number;
  imgClub: string;
  matchE= false;
  public selectedOption: number = 1;
  public newUsuario; 
  public newClub;
  public token;

  filed; 
  constructor(

    private sharedService: SharedService,
    private http: Http,
    private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
    private registrationService: RegistrationService,
    private userService: UserService
  // tslint:disable-next-line:one-line
  ){}

  ngOnInit() {

    this.sharedService.currentCollapse.subscribe( isCollapsed => this.isCollapsed = isCollapsed );  

    this.imgsrc = 'assets/images/generales/escudo.svg';
    /*
    this.Simage = false;
    this.registerClub = true;
    this.registerUser = false;
    this.registerFinish = false;
    */
    //Obtener club, esta en sesion y usuario
    this.newUsuario = JSON.parse(localStorage.getItem('currentUser'));
    this.model.persona = this.newUsuario.contact_name;
    this.model.cargo = this.newUsuario.role;
    this.model.area = this.newUsuario.area;
    this.model.telefono = this.newUsuario.phoneNumber;
    this.model.celular = this.newUsuario.mobileNumber;
    this.model.correo = this.newUsuario.email;
    this.model.correoValidar = this.newUsuario.email;

    this.token = this.userService.JWTGET();

    this.registrationService.getClub(this.newUsuario.Clubs_id, this.token)
    .subscribe(
      data => {
        console.log("register =="+JSON.stringify(data));
        this.newClub = data;
        for(let i in data){
          if(i=='logo'){
            if(data[i] != null){console.log(data[i]);
              this.imgsrc = data[i];
            }
          }else{
            this.model[i+'Cl']=data[i];
          }
        }
      });
 }

  reg = () => {
    
    this.newClub.name=this.model.nameCl; 
    this.newClub.address=this.model.addressCl; 
    this.newClub.city=this.model.cityCl;
    this.newClub.uf=this.model.ufCl;
    this.newClub.cnpj=this.model.cnpjCl; 
    this.newClub.employeesQuantity=(this.model.employeesQuantityCl).replace(/-/g , "");
    this.newClub.website=this.model.websiteCl;
    if(this.Simage){
      this.newClub.logo=this.imgsrc;
    }else{
      delete this.newClub.logo;
    }
    this.newUsuario.contact_name=this.model.persona;
    this.newUsuario.role=this.model.cargo;
    this.newUsuario.area=this.model.area;
    this.newUsuario.phoneNumber=this.model.telefono;
    this.newUsuario.mobileNumber=this.model.celular;
    this.newUsuario.email=this.model.correo;
    this.newUsuario.optradio=this.model.optradio;

    this.loading = true;

    if(this.model.correoValidar == this.model.correo)
    {
      console.log("register == here");
      this.loading = true;
      this.matchE= false;
      // tslint:disable-next-line:max-line-length
      this.registrationService.updateUser(this.newUsuario, this.token)
          .subscribe(
            data => {

              console.log("register =="+data);
              
              this.registrationService.updateClub(this.newClub, this.token)
              .subscribe(
                data => {
                  console.log(JSON.stringify(data));
                  this.alertService.success('Success');
                  this.loading = false;

                  setTimeout(() => {
                    this.router.navigate(['jogue_limpo/minha-conta']);
                  
                  }, 100);

                },
                error => {
                  console.log(error);
                  //La mayoria de veces es que los datos no cambiaron y se dio salvar
                  this.alertService.success('Success');
                  this.loading = false;

                  setTimeout(() => {
                    this.router.navigate(['jogue_limpo/minha-conta']);
                  
                  }, 100);
                }
              );
            },

            error => {
              console.log(error);
              // alert(error);
              let errorR = JSON.parse(error._body);
              this.alertService.error(errorR.error.message);
              this.loading = false;
            }
          );
    }else{

      this.loading = false;
      this.matchE= true;

    }
  }


  onChange(event) {
      // this.shield = event.target.files[0];
      console.log('onChange');
      this.filed = event.target.files[0];
      console.log(this.filed );
      this.stepsModel.push(this.filed[0]);
      const reader = new FileReader();
      reader.readAsDataURL(this.filed);
      reader.onloadend = (e) => {
        console.log("IMAGEN "+reader.result);
        this.imgsrc = reader.result;
        this.imgSend=reader.result;
        this.Simage=true;
        this.SimageAlert=false;
    };
  }

}
