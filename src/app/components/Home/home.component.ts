import { Component,OnInit,ChangeDetectorRef } from '@angular/core';
import { SharedService } from './../../Services/shared.service';
import {ContentService} from './../../Services/content.service';
import { environment } from './../../../environments/environment';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})



export class HomeComponent implements OnInit{

  isCollapsed: boolean;
  loggedIn: boolean;
  closeLogin: boolean;
  banner_1: string;
  banner_2: string;
  hisNameService = 'destacados_home';
  sldNameService = 'slider_home';
  noticesService = 'noticias';
  high_cont: any[] = [];
  slide_cont: any[] = [];
  notices_cont: any[] = [];

  programTitle: string;
  programBody: string;
  authTitle: string;
  authBody: string;
  resourceTitle: string;
  resourceBody: string;

  sad:boolean;
  environmentYT:string;
  loading:boolean = true;


  constructor(private contentService: ContentService, private sharedService: SharedService ){

    this.environmentYT=environment.api;
    
  }



  ngOnInit(){
    this.sharedService.currentCloseLogin.subscribe(closeLogin => this.closeLogin = closeLogin);

    
    this.sharedService.handleCloseLogin(false);
    this.loading = true;
    this.contentService.get(this.sldNameService).subscribe( data => {   
      var aa = JSON.stringify(data.Contents);
      //console.log("slider=="+aa); 
      this.slide_cont = data.Contents[0];  

      this.banner_1 = data.Contents[0].fields[0].value;

    });

 
    this.contentService.get(this.hisNameService).subscribe( data => {

      var aa = JSON.stringify(data.Contents[0]);
      //console.log("destacados=="+aa); 

      this.programTitle = data.Contents[2].title;
      this.programBody = data.Contents[2].body;


      this.authTitle =data.Contents[1].title;
      this.authBody = data.Contents[1].body;

      this.resourceTitle =data.Contents[0].title;
      this.resourceBody = data.Contents[0].body;

      //console.log("program Title ==" + this.programTitle); 

      this.contentService.get(this.hisNameService).subscribe( data => {
        
          var aa = JSON.stringify(data.Contents[0]);
          //console.log("destacados=="+aa); 
    
          this.programTitle = data.Contents[2].title;
          this.programBody = data.Contents[2].body;
    
    
          this.authTitle =data.Contents[1].title;
          this.authBody = data.Contents[1].body;
    
          this.resourceTitle =data.Contents[0].title;
          this.resourceBody = data.Contents[0].body;
    
          console.log("program Title ==" + this.programTitle); 
    
          this.high_cont = data.Contents;

          this.contentService.get(this.noticesService).subscribe( data => {
            
              var aa = JSON.stringify(data.Contents);
              //console.log("noticias=="+aa); 
              this.notices_cont = data.Contents;
              this.loading = false;
            
          });
    
      });

    });

    this.sharedService.currentCollapse.subscribe( isCollapsed => this.isCollapsed = isCollapsed );  
    this.sharedService.currentLogin.subscribe( loggedIn => this.loggedIn = loggedIn );



  }
  closedLogin(){
    this.sharedService.handleCloseLogin(true);
  }

}
