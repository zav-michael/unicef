import { Component, OnInit } from '@angular/core';
import { RouterLinkActive } from '@angular/router';
import { SharedService } from './../../Services/shared.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  loggedIn: boolean;

  constructor(private sharedService: SharedService) { }

  ngOnInit() {


    this.sharedService.currentLogin.subscribe( loggedIn => this.loggedIn = loggedIn );


  }
  
  isLogged = () => {
    if (localStorage.getItem('currentUser')) {
      return true;
    }
    return false;
  }

}
