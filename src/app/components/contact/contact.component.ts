import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';

import { AuthenticationService } from './../../Services/authentication.service';
import { AlertService } from './../../Services/alert.service';

import { ContactForm } from './../../models/contactForm.model';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
  providers:[ContactForm, AuthenticationService]
})
export class ContactComponent implements OnInit {

  loading: boolean = false;
  public maskTelefone = ['(', /\d/, /\d/,')', /\d/,/\d/,/\d/,/\d/, /\d/,/\d/, /\d/, /\d/, /\d/] 
  contactform: ContactForm = new ContactForm();
  @ViewChild('closeBtn') closeBtn: ElementRef;

  constructor(
    private autenticationService: AuthenticationService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    
  }

  send = () => {
    this.loading = !this.loading;
    this.autenticationService.contactForm(this.contactform).subscribe(
      data => {
        this.loading = !this.loading;
        if(data && data.id){
          this.alertService.success("Obrigado pelo seu interesse no Programa Jogue Limpo, Jogue Bem!  Sua mensagem foi enviada com sucesso. Em breve entraremos em contato com você. Qualquer dúvida ou problema, envie um e-mail para: contato@joguelimpojoguebem.com.br Atenciosamente, Equipe Jogue Limpo, Jogue Bem", true);
        }else{
          this.alertService.error("Sua mensagem não pode ser enviada");
        }
        this.contactform = new ContactForm();
        this.closeBtn.nativeElement.click();
      }
    );
  }
}
