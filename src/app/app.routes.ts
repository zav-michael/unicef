import { UpdateRegisterComponent } from './components/register/update-register/update-register.component';
import { RouterModule , Routes}   from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { ProgramComponent } from './components/program/program.component';
import { CenterResourcesComponent } from './components/center-resources/center-resources.component';
import { JogueLimpoComponent } from './components/jogue-limpo/jogue-limpo.component';
import { AccountComponent } from './components/account/account.component';
import { Account2Component } from './components/account-2/account-2.component';
import { AHomeComponent } from './components/account/inc/content/a-home/a-home.component';
import { APrincipalComponent } from './components/account/inc/content/a-principal/a-principal.component';
import { AAutodiagnosticComponent } from './components/account/inc/content/a-autodiagnostic/a-autodiagnostic.component';
import { AAdResultsComponent } from './components/account/inc/content/a-autodiagnostic/a-adresults.component';
import { ARecommendationsComponent } from './components/account/inc/content/a-recommendations/a-recommendations.component';
import { AClubsComponent } from './components/account/inc/content/a-clubs/a-clubs.component';
import { APlanComponent } from './components/account/inc/content/a-plan/a-plan.component';
import { AutodiagnosticComponent } from './components/autodiagnostic/autodiagnostic.component';
import { AdhomeComponent } from './components/autodiagnostic/adhome/adhome.component';
import { AdResultsComponent } from './components/autodiagnostic/adresults/adresults.component';
import { AdRestartComponent } from './components/autodiagnostic/adhome/adrestart.component';
import { AdRecomendationsComponent } from './components/autodiagnostic/adrecomendations/adrecomendations.component';
import { AdMonitoringComponent } from './components/autodiagnostic/admonitoring/admonitoring.component';
import { ResourceContentComponent } from './components/resource-content/resource-content.component';
import { MenuResourceComponent } from './components/menu-resource/menu-resource.component';
import { RccontentComponent } from './components/resource-content/rccontent/rccontent.component';
import { AdmaterialComponent } from './components/autodiagnostic/admaterial/admaterial.component';
import { AdplanComponent } from './components/autodiagnostic/adplan/adplan.component';
import { AdpracticesComponent } from './components/autodiagnostic/adpractices/adpractices.component';
import { ABankComponent } from './components/account/inc/content/a-bank/a-bank.component';
import { AResourcesComponent } from './components/account/inc/content/a-resources/a-resources.component';
import { AMaterialComponent } from './components/account/inc/content/a-material/a-material.component';
import { AProgramasComponent } from './components/account/inc/content/a-programas/a-programas.component';
import { SenhaCreateComponent } from './components/register/senha-create/senha-create.component';
import { AMonitoreoComponent } from './components/account/inc/content/a-monitoreo/a-monitoreo.component';

import { AuthGuard } from './Guard/auth.guard';
import { AdminGuard } from './Guard/admin.guard';

const APP_ROUTES: Routes = [
     { path: 'register', component: RegisterComponent },
     { path: 'home', component: HomeComponent},
     { path: 'edit_profile', component: UpdateRegisterComponent },
      { path: 'autodiagnostic', component: AutodiagnosticComponent, canActivate: [AuthGuard], children:
        [
          {path:'', redirectTo: '/autodiagnostic/test', pathMatch: 'full'},
          {path: 'test', children: 
            [
              {path: '', component: AdhomeComponent},
              {path: 'results', component: AdResultsComponent},
              {path: 'restart', component: AdRestartComponent},
              { path: 'material', component: AdmaterialComponent },
              { path: 'plan', component: AdplanComponent }
            ]
          },
          {path: 'recommendations', component: AdRecomendationsComponent},
          {path: 'monitoring', component: AdMonitoringComponent},
          {path: 'material', component: AdmaterialComponent},
          {path: 'plan', component: AdplanComponent},
          { path: 'practices', component: AdpracticesComponent }
        ]
      },
      { path: 'account',   component: AccountComponent, children :
        [
            { path: '', component: APrincipalComponent },
            { path: 'home', component: AHomeComponent },
            { path: 'autodiagnostic', component: AAutodiagnosticComponent },
            { path: 'autodiagnostic_result', component: AAdResultsComponent },
            { path: 'material', component: AMaterialComponent },
            { path: 'recommendations', component: ARecommendationsComponent },
            { path: 'plan', component: APlanComponent },
            { path: 'bank', component: ABankComponent },
            { path: 'resources', component: AResourcesComponent },
            { path: 'clubs', component: AClubsComponent },
            { path: 'program', component: AProgramasComponent },
            { path: 'monitoring', component: AMonitoreoComponent }
        ]
      },
       { path: 'jogue_limpo', component: JogueLimpoComponent, children:
        [
            { path: 'senha', component: SenhaCreateComponent },
            { path: 'resource-center', component: CenterResourcesComponent},
            { path: 'program', component: ProgramComponent},
            { path: 'minha-conta', component: Account2Component},
            { path: 'resources', component: ResourceContentComponent, children:
             [
                { path: 'rccomponent', component: RccontentComponent }
             ]
            }
        ]
       },
      { path: '', redirectTo: '/home', pathMatch: 'full'},
      { path: '**', redirectTo: '/home', pathMatch: 'full'},
 ];


export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, { useHash : false});
