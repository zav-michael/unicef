import { AdplanComponent } from './components/autodiagnostic/adplan/adplan.component';
import { BrowserModule } from '@angular/platform-browser';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule, Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { TinymceModule } from 'angular2-tinymce';

import { PagerService } from './Services/pager.service';
import { LoaderService } from './Services/loader.service';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/inc/header.component';
import { FooterComponent } from './components/inc/footer.component';
import { HomeComponent } from './components/home/home.component';
import { ModalBannersComponent } from './components/modal/modal-component';
import { LogoModalComponent } from './components/modal/logo-component';
import { ModalNoticiasComponent } from './components/modal/noticia-component';
import { SharedService } from './Services/shared.service';
import { RegisterComponent } from './components/register/register.component';
import { MenuComponent } from './components/menu/menu.component';
import { ProgramComponent } from './components/program/program.component';
import { CommonModule } from '@angular/common';

import { CenterResourcesComponent } from './components/center-resources/center-resources.component';
import { JogueLimpoComponent } from './components/jogue-limpo/jogue-limpo.component';

import { APP_ROUTING } from './app.routes';
import { AccountComponent } from './components/account/account.component';
import { Account2Component } from './components/account-2/account-2.component';
import { SidebarComponent } from './components/account/inc/sidebar/sidebar.component';
import { ContentComponent } from './components/account/inc/content/content.component';
import { AHomeComponent } from './components/account/inc/content/a-home/a-home.component';
import { APrincipalComponent } from './components/account/inc/content/a-principal/a-principal.component';
import { AClubsComponent } from './components/account/inc/content/a-clubs/a-clubs.component';
import { ContactComponent } from './components/contact/contact.component';
import { AlertComponent } from './components/alert/alert.component';
import { AlertService } from './Services/alert.service';
import { SenhaService } from './Services/senha.service';
import { ContentService } from './Services/content.service';
import { RcmenuComponent } from './components/resource-content/rcmenu/rcmenu.component';
import { AdmaterialComponent } from './components/autodiagnostic/admaterial/admaterial.component';
import { TextMaskModule } from 'angular2-text-mask';
import {InlineEditorModule} from '@qontu/ngx-inline-editor';


import {DndModule} from 'ng2-dnd';

import { Ng2FileRequiredModule } from 'ng2-file-required';

// GUARD
import { AuthGuard } from './Guard/auth.guard';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { AutodiagnosticComponent } from './components/autodiagnostic/autodiagnostic.component';
import { AdmenuComponent } from './components/autodiagnostic/admenu/admenu.component';
import { AdhomeComponent } from './components/autodiagnostic/adhome/adhome.component';
import { AdResultsComponent } from './components/autodiagnostic/adresults/adresults.component';
import { AdRestartComponent } from './components/autodiagnostic/adhome/adrestart.component'
import { AdRecomendationsComponent } from './components/autodiagnostic/adrecomendations/adrecomendations.component';
import { AdMonitoringComponent } from './components/autodiagnostic/admonitoring/admonitoring.component';
import { ModalMonitoringComponent as ModalMonitoring } from './components/autodiagnostic/admonitoring/inc/ModalMonitoring/ModalMonitoring.component';
import { DynamicFormQuestionComponent } from './components/autodiagnostic/forms/dynamic-form-question.component';
import { DynamicFormComponent } from './components/autodiagnostic/forms/dynamic-form.component';

import { AcceptComponent } from './components/modal/accept/accept.component';

import { ResourceContentComponent } from './components/resource-content/resource-content.component';
import { MenuResourceComponent } from './components/menu-resource/menu-resource.component';
import { RccontentComponent } from './components/resource-content/rccontent/rccontent.component';
import {SearchFilterPipe} from './pipes/SearchFilterPipe.pipe';
import { EscapeHtmlPipe } from './pipes/keep-html.pipe';
import { AAutodiagnosticComponent } from './components/account/inc/content/a-autodiagnostic/a-autodiagnostic.component';
import { AAdResultsComponent } from './components/account/inc/content/a-autodiagnostic/a-adresults.component';
import {PaginationDirective} from './../directives/pagination.directive';
import {FormlyModule, FormlyBootstrapModule, FormlyFieldConfig} from 'ng-formly';
// WRAPPERS
import { FormlyPanelWrapper } from './components/autodiagnostic/adhome/radio-wrapper';
import { FormlyQuestionWrapper } from './components/autodiagnostic/adhome/question-wrapper';
import { FormlyPanelSectionWrapper } from './components/autodiagnostic/admonitoring/section.wrapper';
import { FormlyFieldInputMonitoring as inputMonitoring } from './components/autodiagnostic/admonitoring/input.wrapper';
import { FormlyFieldRadioMonitoring as radioMonitoring } from './components/autodiagnostic/admonitoring/radio.input';
import { FormlyFieldRadio } from './components/autodiagnostic/adhome/radio.input';
import { FormlyFieldRadioS1 } from './components/autodiagnostic/adhome/radio.step.input';
import { FormlyFieldSelect } from './components/autodiagnostic/adhome/select.input';
import { FormlyFieldInput } from './components/autodiagnostic/adhome/input';
import { FormlyFieldMultiCheckbox } from './components/autodiagnostic/adhome/multicheckbox.input';

import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2OrderModule } from 'ng2-order-pipe';
import {NgxPaginationModule} from 'ngx-pagination';

import {TooltipModule, TabsModule} from 'ngx-bootstrap';

import { ValidateMulticheckbox, required, minLenght, showError } from './components/autodiagnostic/adhome/formly.config';
// CHARTS
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { ARecommendationsComponent } from './components/account/inc/content/a-recommendations/a-recommendations.component';
import { APlanComponent } from './components/account/inc/content/a-plan/a-plan.component';
import { LoaderComponent } from './components/loader/loader.component';
import { ABankComponent } from './components/account/inc/content/a-bank/a-bank.component';
import { AResourcesComponent } from './components/account/inc/content/a-resources/a-resources.component';
import { AdpracticesComponent } from './components/autodiagnostic/adpractices/adpractices.component';
import { AMaterialComponent } from './components/account/inc/content/a-material/a-material.component';
import { AProgramasComponent } from './components/account/inc/content/a-programas/a-programas.component';
import { SenhaCreateComponent } from './components/register/senha-create/senha-create.component';
import { AMonitoreoComponent } from './components/account/inc/content/a-monitoreo/a-monitoreo.component';
import { UpdateRegisterComponent } from './components/register/update-register/update-register.component';
import { OrderByDatePipe } from './pipes/orderByDate.pipe';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { LoadingModule, ANIMATION_TYPES } from 'ngx-loading';
// LOADER

import { HttpClientModule } from '@angular/common/http';
import { NgHttpLoaderModule } from 'ng-http-loader/ng-http-loader.module';

import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { NgxEditorModule } from 'ngx-editor';

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Request, XHRBackend, RequestOptions, Response, Http, RequestOptionsArgs, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


@Injectable()
export class AuthenticatedHttpService extends Http {
  
  constructor(backend: XHRBackend, defaultOptions: RequestOptions, private router:Router, private sharedService: SharedService) {
    super(backend, defaultOptions);
  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    return super.request(url, options).catch((error: Response) => {
            if ((error.status === 401 || error.status === 403) && (window.location.href.match(/\?/g) || []).length < 2) {
                console.log('The authentication session expires or the user is not authorised. Force refresh of the current page.');
                localStorage.removeItem('currentUser');
                this.sharedService.handleLogin( false );
                this.router.navigate(['/']);
                //window.location.href = window.location.href + '?' + new Date().getMilliseconds();
            }
            return Observable.throw(error);
        });
  }
}

@NgModule({
  declarations: [
    AppComponent, PaginationDirective, HeaderComponent, SidebarComponent, ContentComponent, AccountComponent, AHomeComponent, APrincipalComponent, AClubsComponent, HomeComponent, FooterComponent, RegisterComponent, MenuComponent,
    ProgramComponent, CenterResourcesComponent, JogueLimpoComponent, ContactComponent, AlertComponent, ModalBannersComponent, AcceptComponent, AutodiagnosticComponent, AdmenuComponent,
    AdhomeComponent, Account2Component, LogoModalComponent, ModalNoticiasComponent, ResourceContentComponent, MenuResourceComponent,
    RccontentComponent, RcmenuComponent, SearchFilterPipe, AAutodiagnosticComponent, AAdResultsComponent,
    DynamicFormComponent, DynamicFormQuestionComponent, FormlyPanelWrapper, FormlyQuestionWrapper,FormlyPanelSectionWrapper, inputMonitoring, radioMonitoring,
    FormlyFieldRadio, FormlyFieldMultiCheckbox, AdResultsComponent, AdRestartComponent, ARecommendationsComponent, ABankComponent, AResourcesComponent, AdRecomendationsComponent,
    AdMonitoringComponent,AdmaterialComponent, AdplanComponent,AdpracticesComponent,APlanComponent,FormlyFieldInput,FormlyFieldRadioS1,FormlyFieldSelect, AMaterialComponent, AProgramasComponent,
    ModalMonitoring,LoaderComponent,
    SenhaCreateComponent,
    AMonitoreoComponent,
    OrderByDatePipe,
    UpdateRegisterComponent,
    EscapeHtmlPipe
  ],
  imports: [
   
    BrowserModule,
    ReactiveFormsModule,
    BootstrapModalModule.forRoot({container: document.body}),
    APP_ROUTING,
    SimpleNotificationsModule.forRoot(),
    BrowserAnimationsModule,
    HttpModule,
    TooltipModule.forRoot(),
    TabsModule.forRoot(),
    FormsModule,
    Ng2FileRequiredModule,
    DndModule.forRoot(),
    InlineEditorModule,
    ChartsModule,
    HttpClientModule,
  
    TextMaskModule,
    FilterPipeModule,
    RoundProgressModule,
    FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
    NgxEditorModule,
     LoadingModule.forRoot({
        animationType: ANIMATION_TYPES.wanderingCubes,
        backdropBackgroundColour: 'rgba(255, 255, 255, 0.32)', 
        fullScreenBackdrop: true,
        backdropBorderRadius: '4px',
        primaryColour: '#00aded', 
        secondaryColour: '#0f62aa', 
        tertiaryColour: '#0f62aa'
    }),
    NgHttpLoaderModule,
    FormlyModule.forRoot({
      types: [
        { name: 'radioq', component: FormlyFieldRadio },
        { name: 'inputq', component: FormlyFieldInput },
        { name: 'radios1', component: FormlyFieldRadioS1},
        { name: 'selectq', component: FormlyFieldSelect},
        { name: 'multicheckbox', component: FormlyFieldMultiCheckbox },
        { name: 'inputmonitoring', component: inputMonitoring },
        { name: 'radioMonitoring', component: radioMonitoring },
      ],
      validators: [
        { name: 'mcbRequired', validation:  ValidateMulticheckbox }
      ],
      validationMessages: [
        { name: 'required', message: required},
        { name: 'invalidEmailAddress', message: 'Invalid Email Address' },
        { name: 'maxlength', message: 'Maximum Length Exceeded.' },
        { name: 'minlength', message: minLenght},
        { name: 'not_matching', message: 'Password Not Matching' },
        { name: 'zipCode', message: 'ZIP code should be 5 characters'},
        { name: 'uniqueUsername', message: 'This username is already taken.'},
      ],
      wrappers: [
        { name: 'panel', component: FormlyPanelWrapper },
        { name: 'question', component: FormlyQuestionWrapper },
        { name: 'monitoring', component: FormlyPanelSectionWrapper }
      ],
      extras: {
        showError: showError
      }
    }),
    FormlyBootstrapModule,
    TinymceModule.withConfig({}),
    InlineEditorModule,
    Ng2SearchPipeModule, 
    Ng2OrderModule 
  ],
  entryComponents: [
    ModalBannersComponent,
    LogoModalComponent,
    ModalNoticiasComponent,
    AcceptComponent
  ], 
  exports: [BrowserModule, DndModule],
  providers: [SharedService, AuthGuard, AlertService, ContentService,  PagerService, LoaderService, { provide: Http, useClass: AuthenticatedHttpService }],
  bootstrap: [AppComponent, HeaderComponent, FooterComponent]
})
export class AppModule { }
