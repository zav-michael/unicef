import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderByDate'
})
export class OrderByDatePipe implements PipeTransform {

  transform(array: Array<string>, args: string): Array<string> {
    
    if(!array || array === undefined || array.length === 0) return null;
  
      array.sort((a: any, b: any) => {
        a = new Date(a.id);
        b = new Date(b.id);
        return a>b ? -1 : a<b ? 1 : 0;
      });
      console.log(array);
    return array;
  }

}