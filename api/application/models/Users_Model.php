<?php 


class Users_Model extends MY_Model{


	protected $id;
	private $error;

	public function __construct(){
		parent::__construct();
		$this->table = "users";
	}

	public function checkEmail($email){
		return ( $this->db->select('*')->from($this->table)->where('email', $email)->get()->num_rows() )?true:false;
	}

	public function getUserByID($id){

		$result = $this->db->get_where($this->table, array('id' => $id ))->row();
		unset($result->password);

		return $result;

	}

	public function getUserByEmail($email){

		$result = $this->db->get_where($this->table, array('email' => $email ))->row();
		unset($result->password);

		return $result;

	}

	public function getUserByRestoreToken($token){

		$result = $this->db->get_where($this->table, array('restoretoken' => $token ))->row();
		unset($result->password);

		return $result;

	}

	public function getUserByEmailAndPassword($email, $password){
		$query = $this->db->query("SELECT u.*, p.name perfil FROM users u LEFT JOIN user_to_profile up ON(u.id = up.user_id) LEFT JOIN profiles p ON(up.profile_id = p.id)  WHERE email = '{$email}' AND password = '{$password}' LIMIT 1;");
		$result = $query->row();
		unset($result->password);
		
		return $result;

	}


	public function is_profile_allowed($perm_par, $profile_id=FALSE){

		$perm_id = $this->get_perm_id($perm_par);

		// if group par is given
		if($profile_id != FALSE){

			$query = $this->db->where('perm_id', $perm_id);
			$query = $this->db->where('profile_id', $profile_id);
			$query = $this->db->get( 'perm_to_profile' );

			if( $query->num_rows() > 0){
				return TRUE;
			} else {
				return FALSE;
			}
		}

		return $profile_id;
	}

	public function haveAccess($perm,  $id ) {

		$profile_id = $this->get_profile_id_with_user_id( $id );

		if($this->is_profile_allowed($perm, $profile_id)){
			return TRUE;
		}
		
		return FALSE;
	}

	public function get_profile_id ( $profile_par ) {

		if( is_numeric($profile_par) ) { return $profile_par; }

		$query = $this->db->where('name', $profile_par);
		$query = $this->db->get('profiles');

		if ($query->num_rows() == 0)
			return FALSE;

		$row = $query->row();
		return $row->id;
	}

	public function get_profile_id_with_user_id ( $user_id ) {

		$query = $this->db->where('profile_id', $user_id);
		$query = $this->db->get('user_to_profile');

		if ($query->num_rows() == 0)
			return FALSE;

		$row = $query->row();
		return $row->profile_id;
	}

	public function get_perm_id($perm_par) {

		if( is_numeric($perm_par) ) { return $perm_par; }

		$query = $this->db->where('name', $perm_par);
		$query = $this->db->get('perms');

		if ($query->num_rows() == 0)
			return NULL;

		$row = $query->row();
		return $row->id;
	}

	public function is_admin( $user_id = FALSE ) {

		return $this->is_member('Unicef', $user_id);

	}

	public function is_participant( $user_id = FALSE ){

		return $this->is_member('Participants', $user_id);

	}

	public function is_NonParticipant( $user_id = FALSE ){

		return $this->is_member('Non-participants', $user_id);

	}

	public function is_member( $profile_par, $user_id = FALSE ) {

		$profile_id = $this->get_profile_id($profile_par);

		$query = $this->db->where('user_id', $user_id);
		$query = $this->db->where('profile_id', $profile_id);
		$query = $this->db->get('user_to_profile');

		$row = $query->row();

		if ($query->num_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

}