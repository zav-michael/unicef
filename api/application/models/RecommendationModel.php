<?php 


class RecommendationModel extends MY_Model{

	
	public function __construct(){
	    parent::__construct();
		$this->table = "recommendation";
		$this->tableSections = 'sections';
		$this->tableQuestions = 'questions';
	}


	public function getSections($filter = NULL)
    {

		$this->db->select($this->tableSections.'.*');
		$this->db->from($this->tableSections);
		$this->db->join($this->tableQuestions, $this->tableSections.'.id = '.$this->tableQuestions.'.sections_id');
		$this->db->join($this->table, $this->tableQuestions.'.id = '.$this->table.'.questions_id');
		$this->db->group_by("steps_id"); 

		if($filter){
			$this->db->where($filter);
		}
		
		$query = $this->db->get();
		$result = $query->result(); 
                 
        return $result;
	}
	

	public function getRecommendBySection($section_id){

		$this->db->select($this->tableQuestions.'.question, '.$this->table.'.*');
		$this->db->from($this->table);
		$this->db->join($this->tableQuestions, $this->tableQuestions.'.id = '.$this->table.'.questions_id');
		$this->db->join($this->tableSections, $this->tableSections.'.id = '.$this->tableQuestions.'.Sections_id');
		$this->db->where(array('sections_id' => $section_id));
		$query = $this->db->get();
		$result = $query->result();   

		return $result;

	}
	

}