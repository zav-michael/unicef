<?php 


class PracticesModel extends MY_Model{
	
	public function __construct(){
	    parent::__construct();
	    $this->table = "practices";
	}


	public function getAll( $club = NULL ) {
		if($club){
			$this->db->where('P.idClub', $club);
		}
		$commitments = $this->db->select('C.*')->from('commitments C')->join($this->table.' P', 'P.idCommitment=C.id','left')->group_by('P.idCommitment')->get()->result();

		foreach($commitments as $commit){

			$commit->clubes = $this->getClubes( $commit->id, $club );

			foreach($commit->clubes as $clubObj){
				$practicas = $this->getPractices( $commit->id, $clubObj->id );
				$clubObj->practicas = ( count($practicas) > 0 )? $practicas:array();

			}

		}

		return $commitments;

	}

	private function getClubes( $commit, $club=NULL ){
		if($club){
			$this->db->where('C.id', $club);
		}
		return $this->db->select('C.*')
			->from('clubs C')->join($this->table.' P', 'P.idClub=C.id', 'left')
			->where('P.idCommitment', $commit)->get()->result();

	}

	private function getPractices($commit, $club){

		return $this->db->get_where($this->table, array('idCommitment' => $commit,  'idClub' => $club))->result();

	}

}