<?php 


class AnswerModel extends MY_Model{
	private $state = false;
	public function __construct(){
		parent::__construct();
		$this->table = "answers";
		$this->tblAnswUser = "answers_has_users";
	}

	public function getResponse($filter){

		return $this->db->get_where($this->tblAnswUser, $filter)->row();

	}

	public function saveResponse($response, $data){

		$this->db->insert($this->tblAnswUser, $response);
		return ($this->db->affected_rows() != 1) ? $this->state : $this->getResponse($data);

	}

	public function updateResponse($response, $data){
		
		$this->db->where($data)->update($this->tblAnswUser, $response);
		return ($this->db->affected_rows() != 1) ? $this->state : $this->getResponse($data);

	}

	public function deleteResponse( $response ){

		$this->db->delete($this->tblAnswUser, $response);
		return ($this->db->affected_rows() != 1) ? $this->state : $this->getResponse($response);		
	}

	public function checkResponse($data){

		return $this->db->get_where($this->tblAnswUser, $data)->result();

	}
	
	public function getGeneralResults( $user ){
		$this->db->select('A.colors_id, C.name, COUNT(A.colors_id) as quantity, \'2018-01-05 17:13:20\' as ult_fecha')//MAX(AU.modified) as ult_fecha')
		->from($this->tblAnswUser.' AU')
		->join('answers A', 'A.id=AU.Answers_id', 'LEFT')
		->join('questions Q', 'Q.id=A.Questions_id', 'LEFT')
		->join('sections S', 'S.id=Q.Sections_id', 'LEFT')
		->join('colors C', 'C.id=A.Colors_id')
		->where('Q.Sections_id > 1')
		->where('AU.Users_id', $user->sub)
		->group_by('A.Colors_id')
		->order_by('A.Colors_id', 'ASC');

		return $this->db->get()->result();
	}

	public function getGeneralResultsSingle( $id ){
		$this->db->select('A.colors_id, C.name, COUNT(A.colors_id) as quantity, \'2018-01-05 17:13:20\' as ult_fecha')//MAX(AU.modified) as ult_fecha')
		->from($this->tblAnswUser.' AU')
		->join('answers A', 'A.id=AU.Answers_id', 'LEFT')
		->join('questions Q', 'Q.id=A.Questions_id', 'LEFT')
		->join('sections S', 'S.id=Q.Sections_id', 'LEFT')
		->join('colors C', 'C.id=A.Colors_id')
		->where('Q.Sections_id > 1')
		->where('AU.Users_id', $id)
		->group_by('A.Colors_id')
		->order_by('A.Colors_id', 'ASC');

		return $this->db->get()->result();
	}

	public function getColors(){
		return $this->db->get_where('colors')->result();
	}

	public function getSectionsResults($section, $user, $color){

		$this->db->select('COUNT(A.colors_id) as quantity')->from('colors C')
		->join('answers A', 'A.Colors_id=C.id','LEFT')
		->join($this->tblAnswUser.' AU', 'AU.Answers_id=A.id', 'LEFT')
		->join('questions Q', 'Q.id=A.Questions_id', 'LEFT')
		->join('sections S', 'S.id=Q.Sections_id', 'LEFT')
		->where('Q.Sections_id', $section)
		->where('AU.Users_id', $user->sub)
		->where('A.Colors_id', $color)
		->order_by('S.id, A.Colors_id', 'ASC')
		->group_by('A.Colors_id');

		return $this->db->get()->row();

	}

	public function getSectionsResultsSingle($section, $id, $color){

		$this->db->select('COUNT(A.colors_id) as quantity')->from('colors C')
		->join('answers A', 'A.Colors_id=C.id','LEFT')
		->join($this->tblAnswUser.' AU', 'AU.Answers_id=A.id', 'LEFT')
		->join('questions Q', 'Q.id=A.Questions_id', 'LEFT')
		->join('sections S', 'S.id=Q.Sections_id', 'LEFT')
		->where('Q.Sections_id', $section)
		->where('AU.Users_id', $id)
		->where('A.Colors_id', $color)
		->order_by('S.id, A.Colors_id', 'ASC')
		->group_by('A.Colors_id');

		return $this->db->get()->row();

	}

	public function getSectionsResultsWithRecommendation($section, $user){
		$this->db->select('Q.question as question, 
		C.id as color,
		C.name, A.label as answer, 
		R.title as rtitle, R.body as recommendation, R.guide as guide')
		->from($this->tblAnswUser.' AU')
		->join('answers A', 'A.id=AU.Answers_id', 'LEFT')
		->join('questions Q', 'Q.id=A.Questions_id', 'LEFT')
		->join('sections S', 'S.id=Q.Sections_id', 'LEFT')
		->join('colors C', 'C.id=A.Colors_id')
		->join('recommendation R', 'R.questions_id=Q.id', 'LEFT')
		->where('Q.Sections_id', $section)
		->where('AU.Users_id', $user->sub)
		->order_by('Q.index', 'ASC');

		return $this->db->get()->result();
	}

}