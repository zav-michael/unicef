<?php


class ContentsModel extends MY_Model{

	public function __construct(){
		parent::__construct();
        $this->table = "content";
        $this->tableCustom = "custom_fields";
        $this->tableContentType = "content_type";
    }

    public function getAllWithFields($filter = array())
    {
        $order = 'ASC';
				$custom_filter = array();
				if(isset($filter["custom"]))
				{
					$custom_filter = $filter["custom"];
					unset($filter["custom"]);
        }
        if(isset($filter['order'])){
          $order = $filter['order'];
          unset($filter['order']);
        }
        $newarray = [];
				$this->db->select($this->table.'.*');
				$this->db->from($this->table);
				$this->db->where($filter);
        $this->db->join($this->tableContentType, $this->tableContentType.'.idContentType = '.$this->table.'.'.$this->tableContentType.'_id');
        if($order != "ASC"){
          $this->db->order_by("id", $order);
        }else{
          $this->db->order_by("weight", "asc");
        }        
        $temp = $this->db->get();
				$array = $temp->result();
        if(!empty($array)){
            foreach($array as $key => $item):
                 $this->db->select($this->tableCustom.'.*'.','.$this->tableCustom.'_has_'.$this->table.'.*');
                 $this->db->from($this->tableCustom.'_has_'.$this->table);
                 $this->db->where(array('content_id' => $item->id));
								 if(!empty($custom_filter))
				 				 {
                    $this->db->where($custom_filter);
							   }
                 $this->db->join($this->tableCustom, $this->tableCustom.'.id = '.$this->tableCustom.'_has_'.$this->table.'.'.$this->tableCustom.'_id');
                 $query = $this->db->get();
                 $item->fields = $query->result();
                 $newarray[] = $item;
            endforeach;
        }
        return $newarray;
    }

		public function add_fields($data){
			return $this->db->insert($this->tableCustom.'_has_'.$this->table, $data);
    }
    
    public function set_fields($id, $data){

      $this->db->where('id', $id)->update($this->tableCustom.'_has_'.$this->table, $data);
      return ($this->db->affected_rows() != 1) ? $this->state : $this->get(array('id' => $id));

    }

    public function set_fields_content_type($id, $data){

      $this->db->where('idContentType', $id)->update($this->tableContentType, $data);
      return ($this->db->affected_rows() != 1) ? $this->state : $this->getContentType(array('idContentType' => $id));

    }

    public function getContentType($filter = array()){
      $this->db->select($this->tableContentType.'.*');
      $this->db->from($this->tableContentType);
      $this->db->where($filter);
      $temp = $this->db->get();
			return $temp->result();
    }

    public function updateContentType($id, $data){
      $this->db->where('id', $id)->update($this->tableCustom.'_has_'.$this->table, $data);
      return ($this->db->affected_rows() != 1) ? $this->state : $this->get(array('id' => $id));
    }

}
