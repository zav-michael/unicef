<?php


class TaxonomyModel extends MY_Model{

	public function __construct(){
		parent::__construct();
        $this->tableContent = "content";
        $this->tableTerm = "terms";
        $this->tableTaxonomy = "taxonomy";
        $this->tableCustom = "custom_fields";
        $this->tableContentType = "content_type";
        $this->tableContentTerm = "content_has_terms";
        $this->tableCustomContent = "custom_fields_has_content";
    }

    public function getAllWithFields($filter = array())
    {   
        $newarray = array();
        $this->db->select($this->tableTaxonomy.'.*');
        $this->db->from($this->tableTaxonomy);
        if(!empty($filter))
        {
            $this->db->where($filter);
        }
        $this->db->order_by("id", "asc");
        $newarray = $this->db->get();
        $data = array();
        foreach($newarray->result() as $key => $item)
        {   
            $id = (int)$item->id;
            $item->term = $this->getTermByTaxonomy(array('taxonomy_id' => $id));
            $array = $this->getContentByTaxonomy(array('taxonomy.id' => $id));
            $item->content = $array;
            $data[] = $item;
        }
        
        return $data;
    }


    protected function getTermByTaxonomy($filter = array())
    {
        $this->db->select($this->tableTerm.'.*');
        $this->db->from($this->tableTerm);

        if(!empty($filter))
        {
            $this->db->where($filter);
        }

        $this->db->order_by("id", "asc");
        $newarray = $this->db->get();
        $array = array();

    
        return $newarray->result();
    }
    

    protected function getContentByTaxonomy($filter)
    {
        $this->db->select($this->tableContent.'.* ,'.$this->tableContentTerm.'.terms_id, '.$this->tableTerm.'.taxonomy_id');
        $this->db->from($this->tableContent);
        $this->db->join($this->tableContentTerm, $this->tableContent.'.id = '.$this->tableContentTerm.'.'.'content_id');
        $this->db->join($this->tableTerm, $this->tableTerm.'.id = '.$this->tableContentTerm.'.'.'terms_id');
        $this->db->join($this->tableTaxonomy, $this->tableTaxonomy.'.id = '.$this->tableTerm.'.'.'taxonomy_id');
        if(!empty($filter))
        {
            $this->db->where($filter);
        }
        $this->db->order_by($this->tableContent.".id", "asc");
        $newarray = $this->db->get();
        $array = $newarray->result();
        $temp = array();
        if(!empty($array)){
            foreach($array as $key => $item):
                $this->db->select($this->tableCustom.'.*'.','.$this->tableCustomContent.'.*');
                $this->db->from($this->tableCustomContent);
                $this->db->join($this->tableCustom, $this->tableCustom.'.id = '. $this->tableCustomContent.'.'.$this->tableCustom.'_id');
                $this->db->where(array('content_id' => $item->id));                
                $query = $this->db->get();
                $item->fields = $query->result();
                $temp[] = $item;    
            endforeach;
        }
        return $temp;
    }

    public function add_contents_terms($data){
        return $this->db->insert($this->tableContent.'_has_'.$this->tableTerm, $data);
    }

}
