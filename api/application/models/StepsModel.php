<?php 


class StepsModel extends MY_Model{

	private $tableSections = "sections";
	private $tableQuestions = "questions";
	private $tableAnswers = "answers";
	private $tableCustomFields = "custom_fields";
	private $tableResults = "answers_has_users";
	private $tableUsersSteps = "steps_has_users";
	
	public function __construct(){
	    parent::__construct();
	    $this->table = "steps";
	}

	public function getSteps($user, $filter = NULL) {

		$steps = $this->db->get_where($this->table, $filter)->result();
		return $this->__Sections( $steps, $user );
	}

	public function saveProgress($step, $user){
		$_step = $this->checkStep($step, $user);
		if(!$_step){
			$this->db->insert($this->tableUsersSteps, array('steps_id' => $step, 'users_id' => $user ));
		}
		return ($this->db->affected_rows() != 1) ? $_step : $this->checkStep($step, $user);
	}

	public function getCurrentStep( $user ){

		return $this->db->order_by('steps_id', 'DESC')->limit('1')->get_where($this->tableUsersSteps, array('users_id' => $user->sub))->row();

	}

	protected function checkStep($step, $user){
		return $this->db->get_where($this->tableUsersSteps, array('steps_id' => $step, 'users_id' => $user ))->row();
	}

	public function __Sections( $steps, $user ){

		foreach ($steps as $step) {
		 	
			$step->sections = $this->_getSections( $step->id );
			$this->__Questions($step->sections, $user);
		} 

		return $steps;

	}

	private function __Questions( $sections, $user ){

		foreach ($sections as $section) {
			$section->Questions = $this->_getQuestions( $section->id );
			$this->__Answers($section->Questions, $user);
		} 

		return $sections;
	}

	private function __Answers( $questions, $user ){

		foreach ($questions as $question) {
		 	
			$question->answers = $this->_getAnswers( $question->id );
			$question->response = $this->_Responses( $question->id, $user->sub );
		}

		return $questions;

	}

	private function _Responses( $question, $user ){

		return $this->db->get_where($this->tableResults, array('answers_questions_id' => $question, 'users_id' => $user ))->result();

	}

	private function _getSections( $id ){

		return $this->db->get_where($this->tableSections, array('steps_id' => $id))->result();

	}

	private function _getQuestions( $id ){

		return $this->db->get_where($this->tableQuestions, array('sections_id' => $id))->result();

	}

	private function _getAnswers( $id ){

		return $this->db->order_by('Questions_id', 'ASC')->order_by('label', 'ASC')->select('A.*, F.type')
		->from($this->tableAnswers. ' A')
		->join($this->tableCustomFields.' F', 'F.id=A.custom_fields_id', 'left')
		->where(array('questions_id' => $id))->get()->result();

	}

}