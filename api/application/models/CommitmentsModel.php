<?php 


class CommitmentsModel extends MY_Model{


	protected $id;
	private $state = false;

	public function __construct(){
		parent::__construct();
		$this->table = "commitments";
		$this->tableIndicators = "indicators";
		$this->tableIndhasUser = "indicators_has_users"; 
	}

	public function pagination(){

		$pagination = $this->db->select('id, id as name')
		->from($this->table)->get()->result();

		$i=1;
		foreach($pagination as $pag){
			$pag->title = $i;
			$i++;
		}

		return $pagination;

	}

	public function getAllInfo(){

		$commitments = $this->getAll();
		foreach( $commitments as $commitment ){
			$commitment->indicators['priority'] = $this->_getIndicators( array('commitments_id' => $commitment->id));
			//$commitment->indicators['principal'] = $this->_getIndicators( array('commitments_id' => $commitment->id, 'action_types_id' => 2));
			//$commitment->indicators['complementary'] = $this->_getIndicators( array('commitments_id' => $commitment->id, 'action_types_id' => 3));
		}
		
		return $commitments;
	}

	public function _getIndicators( $commit ) {

		return $this->db->get_where($this->tableIndicators, $commit )->result();

	}

	public function getStep( $step = NULL, $user ) {
		
		if($step){
			$commitments = $this->getAll( array('id' => $step) );
		}else{
			$commitments = $this->getAll(  );
		}
		
		foreach( $commitments as $commitment ){
			
			$commitment->indicators['priority'] = $this->_getIndicators( array('commitments_id' => $commitment->id, 'action_types_id' => 1));
			
			

			foreach($commitment->indicators['priority'] as $indicator){
				$indicator->response = $this->getResponse( array( 'indicators_id' => $indicator->id, 'users_id' => $user->sub ) );
			}
			
			$commitment->indicators['principal'] = $this->_getIndicators( array( 'commitments_id' => $commitment->id, 'action_types_id' => 2 ));

			foreach($commitment->indicators['principal'] as $indicator){
				$indicator->response = $this->getResponse( array( 'indicators_id' => $indicator->id, 'users_id' => $user->sub ) );
			}

			$commitment->indicators['complementary'] = $this->_getIndicators( array('commitments_id' => $commitment->id, 'action_types_id' => 3));

			foreach($commitment->indicators['complementary'] as $indicator){
				$indicator->response = $this->getResponse( array( 'indicators_id' => $indicator->id, 'users_id' => $user->sub ) );
			}


			
		}

		return $commitments;
		
	}

	public function checkResponse($data){

		return $this->db->get_where($this->tableIndhasUser, $data)->row();

	}

	public function saveResponse( $data ){

		$this->db->insert($this->tableIndhasUser, $data);
		
		return ($this->db->affected_rows() != 1) ? $this->state : true;

	}

	public function updateResponse($data, $value){
		
		$this->db->where($data)->update($this->tableIndhasUser, $value);
		
		return ($this->db->affected_rows()) ? $this->state : true;

	}

	public function getResponse($filter){
		
		return $this->db->get_where($this->tableIndhasUser, $filter)->row();

	}

	public function getGeneralProgress( $user ){

		$totalPercent = 0;
		$percentByCommitment = array();

		$commitments = $this->getAll();
		$completed = 0;
		foreach( $commitments as $commit){
			
			$indicators =  $this->_getIndicators(array( 'commitments_id' =>  $commit->id ));

			$commitTotal = 0;
			$indicatorsTotal = 0;

			foreach( $indicators as $indicator ) {

				$response  = $this->getResponse( array( 'indicators_id' => $indicator->id, 'users_id' => $user->sub ) );
				$commitTotal += ( count( $response ) > 0) ? $response->value : 0;
				$indicatorsTotal++;

			}
			$indicatorsTotal *= 100;
			$completed += ( $commitTotal * 100)/$indicatorsTotal;

		}

		$completed = ceil( ( $completed * 100 ) / ( count( $commitments ) * 100 ) ) ;

		return array('generalProgress' => $completed );
		
	}

	private function getCommitPercent( $commit, $user ) {

		$commitments = $this->getAll();

		print_r($commitments);

	}

}