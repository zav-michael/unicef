<?php 


class Questions_Model extends CI_Model{


	protected $id;
	private $table = "questions";
	private $answersTable = "answers";
	private $error;

	public function __construct(){
		parent::__construct();
	}

	public function save($data){

		$this->db->insert($this->table, $data);

		return ($this->db->affected_rows() != 1) ? FALSE : TRUE;

	}

	public function getAll(){

		$questions = $this->db->select( '*' )->from( $this->table )->get()->result();

		foreach ($questions as $key => $value) {
			$value->answers = $this->getAswers($value->id);
		}

		return $questions;
	}

	public function getAswers($questionid){

		return $this->db->get_where($this->answersTable, array('questions_id' => $questionid))->result();

	}

	public function update($id, $data){

		$this->db->where('id', $id)->update($this->table, $data);

		return ($this->db->affected_rows() != 1) ? FALSE : TRUE;

	}

	public function delete($id){

		$this->db->where('id', $id);
   		$this->db->delete($this->table);

   		return ($this->db->affected_rows() != 1) ? FALSE : TRUE; 

	}

}