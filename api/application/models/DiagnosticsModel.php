<?php 


class DiagnosticsModel extends MY_Model{

	protected $id;
	private $SHSTable = 'schoolsstates';
	private $HASTable = 'hostedathletesstates';
	private $HAARTable = 'hostedathletesageranges';

	public function __construct(){
		parent::__construct();
		$this->table = "diagnostic";
	}

	public function exists($club_id){

		$query = $this->db->select('*')->from($this->table)->where('clubs_id', $club_id)->get();

		if( $query->num_rows() > 0){
			return $this->get(array('id' => $query->row()->id));
		} else {
			return FALSE;
		}

	}

	public function get($filter = NULL){

		$diagnostic = $this->db->get_where($this->table, $filter)->row();

		return $this->__CompleteDiagnosticData( $diagnostic );

	}

	private function getSchoolsStates($diagnostic){

		return $this->db->select('s.*')->from('states S')
		->join('schoolsstates SS', 'SS.states_id = S.id', 'LEFT')
		->where('SS.diagnostic_id', $diagnostic)->get()->result();

	}

	private function hostedAtlhetesStates($diagnostic){

		return $this->db->select('S.*')->from('states S')
		->join('hostedathletesstates HS', 'HS.states_id = S.id', 'LEFT')
		->where('HS.diagnostic_id', $diagnostic)->get()->result();

	}

	private function baseTeamsAgeCategories($diagnostic){

		return $this->db->select('A.*')->from('ageranges A')
		->join('baseteamagecategories BA', 'BA.AgeRanges_id = A.id', 'LEFT')
		->where('BA.diagnostic_id', $diagnostic)->get()->result();

	}

	private function hostedathletesageranges($diagnostic){

		return $this->db->select('A.*')->from('ageranges A')
		->join('hostedathletesageranges HA', 'HA.AgeRanges_id = A.id', 'LEFT')
		->where('HA.diagnostic_id', $diagnostic)->get()->result();

	}

	private function updateSHS( $SHS, $diagnostic_id ) {

		$this->db->delete($this->SHSTable, array('diagnostic_id' => $diagnostic_id)); # Se eliminan registros anteriores

		foreach ($SHS as $key => $value) {
			
			$this->db->insert($this->SHSTable, array('diagnostic_id' => $diagnostic_id, 'States_id' => $value));	

		}
		return ($this->db->affected_rows() != 1) ? FALSE : $this->get(array('id' => $diagnostic_id));
	}

	private function updateHAS( $HAS, $diagnostic_id ) {

		$this->db->delete($this->HASTable, array('diagnostic_id' => $diagnostic_id)); # Se eliminan registros anteriores

		foreach ($HAS as $key => $value) {
			
			$this->db->insert($this->HASTable, array('Diagnostic_id' => $diagnostic_id, 'States_id' => $value));	

		}
		return ($this->db->affected_rows() != 1) ? FALSE : $this->get(array('id' => $diagnostic_id));
	}

	private function updateHAAR( $HAAR, $diagnostic_id ) {

		$this->db->delete($this->HAARTable, array('diagnostic_id' => $diagnostic_id)); # Se eliminan registros anteriores

		foreach ($HAAR as $key => $value) {
			
			$this->db->insert($this->HAARTable, array('Diagnostic_id' => $diagnostic_id, 'States_id' => $value));	

		}
		return ($this->db->affected_rows() != 1) ? FALSE : $this->get(array('id' => $diagnostic_id));
	}

	public function getAll($filter = NULL){

		$diagnostics = $this->db->get($this->table)->result();

		foreach ($diagnostics as $diagnostic) {
			
			$diagnostic = $this->__CompleteDiagnosticData( $diagnostic );

		}

		return $diagnostics;

	}

	private function __CompleteDiagnosticData( $diagnostic ){

		$diagnostic->baseTeamsAgeCategories = $this->baseTeamsAgeCategories($diagnostic->id);
		$diagnostic->schoolsstates = $this->getSchoolsStates($diagnostic->id);
		$diagnostic->hostedAtlhetesStates = $this->hostedAtlhetesStates($diagnostic->id);
		$diagnostic->hostedathletesageranges = $this->hostedAtlhetesStates($diagnostic->id);

		return $diagnostic;

	}

}
