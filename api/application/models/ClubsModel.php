<?php 


class ClubsModel extends MY_Model{


	protected $id;	

	public function __construct(){
		parent::__construct();
		$this->table = "clubs";
	}

	public function checkCEP($cep){
		return ( $this->db->select('*')->from($this->table)->where('employeesQuantity', $cep)->get()->num_rows() )?true:false;
	}

	public function getData($table)
	{
		
		return $this->db->select('*')->from($table)->get()->result();

	}

	public function getByUserId( $user ){
		
		return $this->db->select('C.*')->from($this->table.' C')
		->join('users U', 'U.Clubs_id=C.id','left')->where( 'U.id', $user )->get()->row();

	}

	public function getUser( $club ){
		
		return $this->db->select('u.*')->from('users'.' u')->where(array('u.Clubs_id' => $club))->get()->row();		
		
	}

	public function getQuantity()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function getDataFilter($data, $page = 1, $items, $flagPaginate  = true)
	{	
		$city = "";
		$name = "";
		$finish = $page * $items;
		$initial = $finish - $items;
		if(isset($data['city'])){
			$city = $data['city'];
			unset($data['city']);
		}

		if(isset($data['name'])){
			$name = $data['name'];
			unset($data['name']);
		}
		
		if(!isset($data['compromiso'])){


			$select = 'c.*';
			$query = $this->db->select($select)->from('clubs c');							
			// se debe obtener el valor de indicadores de un compromiso y sumarlos, 
			// y dividir por la cantidad de indicadores que tiene el compromiso si el mayor al x numero pertenece
			$query->where($data);
			$query->like('city', $city);
			$query->like('name', $name);
			
		    $query->limit($finish, $initial);

			$result = $query->get();
			$data = $result->result();
			$numer = count($this->getData('clubs'));

		}else{

			$compromiso = $data['compromiso'];
			unset($data['compromiso']);

			$where = '';

			foreach($data as $key => $item){
				$where .= " AND $key = $item";
			}

			
			$string = "SELECT c.*, (sum(iu.value) / (SELECT COUNT(*)  FROM indicators WHERE commitments_id = '$compromiso')) as promedio FROM clubs as c
			 LEFT JOIN users  as u ON c.id = u.clubs_id
			 INNER JOIN indicators_has_users  as iu ON c.id = iu.users_id
			 INNER JOIN indicators as i ON i.id=iu.indicators_id
			 WHERE i.commitments_id = '$compromiso' AND city LIKE '%$city%' $where
			group by iu.users_id LIMIT $initial, $finish";

			$query = $this->db->query($string);

			
			$data = $query->result();

			foreach($data as $key => $item){
				if($item->promedio <= $this->valor){
					unset($data[$key]);
				}
			}

			$numer  = count($data);
			

		}
		
		
		return array('data' => $data , 'count' => $numer);
	}

}