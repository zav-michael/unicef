<?php include(APPPATH.'views/inc/head.php');?>
<script src="<?=$template;?>js/jquery.cycle.all.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?=$template;?>css/prettyPhoto.css" >
<script src="<?=$template;?>js/jquery.prettyPhoto.js" type="text/javascript"></script>
<script src="<?=$template;?>js/jquery.cycle.all.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
    $("a[rel^='video']").prettyPhoto({
    	show_title: false,
        social_tools: "" /* html or false to disable */
    });
  });
<!--
$(function(){
	$('#timeline').cycle({
		fx: 'scrollHorz',
		pager:  '#timelinenav',
		timeout: 0,
		before: function(curr, next, options, forwardFlag){
			$(next).parent().height($(next).height());
		},
		pagerAnchorBuilder: function(idx, slide) { 
			years = $(slide).attr('data-years');
			nav = "";
			years = years.split(',');
			n = 0;
			$(years).each(function(){
				nav += '<div class="timeline-year">'+years[n]+'</div>';
				n++;
			});
	        return '<a id="thumb_'+(idx+1)+'" data-nav="'+(idx)+'" href="#!">'+nav+'</a>'; 
	    }

	});
	
	var btntext = "";
	$('.boton-more').toggle(function(){
		btntext = $(this).text();
		$(this).html('Ocultar Información');
		$(this).parent().find('.hide-content').slideDown(); 
		$(this).addClass('selected');
	},function(){
		$(this).text(btntext);
		$(this).parent().find('.hide-content').slideUp(); 
		$(this).removeClass('selected');
	});
	
});
$(window).load(function(){
	anch = 0;
	anchor = $('#timelinenav a');
	anchor.each(function(){
		anch += $(this).width()+2;
	});
	$('#timelinenav').width(anch);
	//
	$('#timelinenav').parent().mousemove(function(e){
		mar = parseInt($('#timelinenav').css('margin-left').replace('px',''));
		pos = e.pageX - ($(this).position().left);
		ancho = $('#timelinenav').parent().width();
		tot = $('#timelinenav').width();
		sobra = tot-ancho;
		porcent = parseFloat(sobra/ancho);
		mg = porcent*pos;
		$('#timelinenav').css({
			marginLeft: '-'+mg+'px'
		});		
		$('.timeline-cursor .dot').css({
			marginLeft: pos+'px'
		});	
	});
});
//-->	
</script>
</head><!--fin head-->
<body id="<?=$body_id?>">
	<div id="wrapper">
    	<?php include(APPPATH.'views/inc/header.php');?>
        <div id="content">
			<div class="web-wrapper">
				<div class="layer-cont web-content margin-top-30">
					<section class="start-side">
							<div class="migas-content"><a href="<?=$site_url;?>">Inicio</a> <span class="separarmiga"></span> Quiénes Somos</div>
						<div class="web-content margin-top-40">
							<div class="margin-vert-20">
								<div class="web-title">¿Quiénes Somos?</div>
							</div>
							<div class="web-content">
								<p class="resaltar">Ya son más de 40 años haciendo empresa, prestando servicios a la comunidad, pero sobretodo 40 años guardando recuerdos en un mismo lugar, a miles de familias que nos han encomendado esta misión.</p>
								<p>Son 40 años de experiencia en el mercado colombiano como pioneros en el desarrollo y construcción de parques cementerios y funerarias en nuestro país.</p>
								<p>40 años reflejados en la experiencia en prestación de:</p>
								<ul>
									<li>Más de <span class="resaltar">110.000</span> servicios de inhumación prestados directamente en nuestros campos santos.</li>
									<li>Más de <span class="resaltar">16.000</span> servicios de velación prestados directamente en nuestras funerarias.</li>
									<li>Más de <span class="resaltar">15.000</span> servicios de cremación prestados en nuestros hornos crematorios.</li>
								</ul>
								<p>Una trayectoria que día a día nos compromete más en prestar un servicio con altos estándares de calidad, con compromiso social y enfoque a nuestros clientes.</p>
								
							</div>
							<div class="margin-vert-20">
								<div class="c-green font-20">Nuestro Negocio es...</div>
							
								<p>Apoyar a las familias en el momento inicial del Duelo y a perpetuar el recuerdo de sus seres queridos con un digno manejo de la pérdida.</p>
																
							</div>
						</div>
						<div>
							<div class="c-blue font-20 margin-vert-20">Nuestro Compromiso:</div>
							<div class="web-content">
								<ul>
									<li>Propender el desarrollo de las competencias de nuestros colaboradores en función de mantener una experta asesoría exequial.</li> 
									<li>Trabajar de la mano con nuestros proveedores, empleados y clientes en la interacción con el medio ambiente dando un manejo apropiado de los recursos. </li>
									<li>Crear valor de forma sostenible que contribuya al crecimiento del país y que permita una retribución justa a nuestros colaboradores, accionistas y a la sociedad en general.</li>  
								</ul>
							</div>
						</div>
					</section>
					<section class="end-side margin-left-15 margin-top-35">
						<div class="margin-top-110">
							<div class='imgwho'></div>
							<div>
								<a href="<?=$site_url;?>main/video/?iframe=true&width=640&height=360" rel="video[2]" title="Grupo Recordar">
								<div class="boton-green f-left" style="width: 194px;">Ver video institucional</div>
								<div class="who_icon"></div>
								</a>
							</div>
						</div>
					</section>
					<section class="web-content">
						<div style="height:30px;">&nbsp;</div>
						<div class="left-side">
							
							<div class="margin-vert-20">
								<div class="web-title">Misión</div>
							</div>
							<div class="web-content">
								<p>Nuestra organización se soporta en un alto sentido de responsabilidad social para inculcar el espíritu de previsión como elemento de tranquilidad para el futuro, ofrecer soluciones exequiales en el manejo de las situaciones angustiantes de dolor ante la pérdida de un ser querido y brindar orientación profesional y humana a las familias en sus procesos de duelo.</p>
								
							</div>
							<?php /*
							<div class="web-content margin-top-5">
								<div class="hide-content">
									<p>Propender el desarrollo de las competencias de nuestros colaboradores en función de mantener una experta asesoría exequial.</p>
									<p>Trabajar de la mano con nuestros proveedores, empleados y clientes en la interacción con el medio ambiente dando un manejo apropiado de los recursos.</p>
									<p>Crear valor de forma sostenible que contribuya al crecimiento del país y que permita una retribución justa a nuestros colaboradores, accionistas y a la sociedad en general.</p>
								</div>
								<a href="#!" class="boton boton-more f-right">Continuar leyendo</a>
							</div>*/?>
						</div>
						<div class="right-side">
							<div class="margin-vert-20">
								<div class="web-title">Visión</div>
							</div>
							<div class="web-content">
								<p>Consolidar nuestra posición de liderazgo a nivel nacional en el sector exequial basados en lograr la preferencia de las familias, el crecimiento en el n&uacute;mero de personas vinculadas, el volumen de servicios y la infraestructura, la excelencia en los colaboradores, altos estándares de calidad e innovación de productos y servicios para crear valor de forma sostenible.</p>
							</div>
						</div>
						
					</section>

					<section class="web-content margin-vert-20">
						<div style="height:30px;">&nbsp;</div>
						<div class="web-title margin-horz-20">Reseña Histórica</div>					
						<div class="web-int-content timeline">	
							<div class="timeline-nav">
								<div id="timelinenav" class="timeline-container">
									<!--thumbs nav-->
								</div>
							</div>
							<div class="timeline-cursor">
								<div class="line">
									<div class="dot">&nbsp;</div>
								</div>
							</div>
							<div id="timeline" class="timeline-content">
								<!-- ini-data-years  -->
								<div class="timeline-item-content" data-years="1967,1982,1986">
									<article class="timeline-item">
										<div class="timeline-item-title">1967</div>
										<div class="timeline-item-text t-justify">En 1967 la compañía nace como JARDINES DEL RECUERDO en Bogotá.</div>
									</article>
									<article class="timeline-item">
										<div class="timeline-item-title">1982</div>
										<div class="timeline-item-text t-justify">En 1982 adquieren " JARDINES DEL RECUERDO de Cali.</div>
									</article>
									<article class="timeline-item">
										<div class="timeline-item-title">1986</div>
										<div class="timeline-item-text t-justify">En 1986 JARDINES DE LA ETERNIDAD en la ciudad de Barranquilla. Éste con las Capillas de velación.</div>
									</article>
								<!--
									<article class="timeline-item">
										<div class="timeline-item-title">1964</div>
										<div class="timeline-item-text t-justify">La compañía nace como JARDINES DEL RECUERDO en la ciudad de Barranquilla en 1.964, dos años después los accionistas venden la empresa y se trasladan a la ciudad de Bogotá</div>
									</article>
									<article class="timeline-item" style="background: #E9EFF4;">
										<div class="timeline-item-title c-blue">1967</div>
										<div class="timeline-item-text">Se funda JARDINES DEL RECUERDO en Bogotá</div>
									</article>
									<article class="timeline-item">
										<div class="timeline-item-title">1982-1986</div>
										<div class="timeline-item-text t-justify">
											<div>En abril de 2006 se adquiere JARDINES DE CARTAGENA, de la misma ciudad, conservando también el nombre que traía. Igualmente por esta época.</div>
										</div>
									</article>
								-->
								</div>
								
								<!-- ini-data-years  -->
								<div class="timeline-item-content" data-years="1991,1999,2002">
									<article class="timeline-item">
										<div class="timeline-item-title">1991 - 1996</div>
										<div class="timeline-item-text t-justify">En 1.991 se constituye la empresa RECORDAR S.A., cuyo objetivo es el de comercializar la previsión exequial en el ámbito nacional. En 1.993 se ofrece un nuevo servicio a la comunidad, el horno crematorio en la ciudad de Bogotá, en 1995 en Barranquilla y en 1996 en la ciudad de Cali.</div>
									</article>
									<article class="timeline-item" style="background: #E9EFF4;">
										<div class="timeline-item-title c-blue">1999</div>
										<div class="timeline-item-text t-justify">En 1.999, las empresas JARDINES DEL RECUERDO de Bogotá, Cali y JARDINES DE LA ETERNIDAD de Barranquilla se fusionan y se constituye PARQUES Y FUNERARIAS S.A. conservando cada ciudad su identidad comercial.</div>
									</article>
									<article class="timeline-item">
										<div class="timeline-item-title">2002</div>
										<div class="timeline-item-text t-justify">
											<div>Se inicia la prestación directa de servicios funerarios en salas y capillas de velación en el parque JARDINES DEL RECUERDO de Bogotá.</div>
										</div>
									</article>
								</div>
								
								<!-- ini-data-years  -->
								<div class="timeline-item-content" data-years="2004,2006,2007">
									<article class="timeline-item">
										<div class="timeline-item-title">2004</div>
										<div class="timeline-item-text t-justify">En el año 2004, PARQUES Y FUNERARIAS S.A. adquiere el parque cementerio JARDINES DE LA ASUNCIóN de la ciudad de Tunja, conservando el nombre con el que inició operaciones en 1979. También para ese mismo año, la compañía inicia la administración del Cementerio Británico de la ciudad de Bogotá.</div>
									</article>
									<article class="timeline-item" style="background: #E9EFF4">
										<div class="timeline-item-title c-blue">2006</div>
										<div class="timeline-item-text t-justify">En abril de 2006 se adquiere JARDINES DE CARTAGENA, de la misma ciudad, conservando también el nombre que traía. Igualmente por esta época, Parques y Funerarias S.A. recibió a través del Instituto de Normas Técnicas y calidad –ICONTEC- el certificado de gestión de la calidad ISO 9001:2000 en servicio funerario y de parque cementerio, en gestión de traslado y preparación del cuerpo, velación, exequias y destino final en el parque Jardines del recuerdo en Bogotá.</div>
									</article>
									<article class="timeline-item">
										<div class="timeline-item-title">2007</div>
										<div class="timeline-item-text t-justify">
											<div>En el 2007 se da inicio a la construcción de JARDINES DE LA ETERNIDAD DEL SUR en la ciudad de Barranquilla. <br /><br /> Estos &uacute;ltimos parques cementerios tienen la misma infraestructura, es decir, aparte de tener los productos propios de cementerio, también están provistos de sus respectivas salas de velación. <br /><br /> En este mismo año, se adquiere en la ciudad de Cali LA FUNERARIA SAN FERNANDO, quién cambia su nombre por la marca FUNERARIA JARDINES DEL RECUERDO.</div>
										</div>
									</article>
								</div>
								
								<!-- ini-data-years  -->
								<div class="timeline-item-content" data-years="2008,2009">
									<article class="timeline-item" style="width:230px;">
										<div class="timeline-item-title">2008</div>
										<div class="timeline-item-text t-justify">En el 2008 se consolida como Grupo Recordar y sigue proyectándose como la organización más importante del sector con un alto sentido de responsabilidad social y continua ofreciendo a las familias el apoyo y asesoría en el manejo del duelo y además la oportunidad de perpetuar la memoria de los seres queridos fallecidos.</div>
									</article>
									<article class="timeline-item" style="width:700px; background: #E9EFF4;">
										<div class="timeline-item-title">2009</div>
										<div class="timeline-item-text t-justify">
											<div style="width: 360px; float: left; margin: 0 0 10px 0;">Se inauguró la Funeraria Jardines del Recuerdo – Sede Kennedy que cuenta con 5 salas de velación, floristería, cafetería y parqueadero.  Allí se resalta el uso de espacios abiertos y de naturaleza para brindar tranquilidad y paz a las personas que aquí se encuentran.
											<br/><br/>En Valledupar se abrió la Funeraria Recordar que cuenta con 3 salas de velación, cafetería, floristería y un amplio parqueadero. Además, cuenta con el servicio de velación virtual, creado por le necesidad de clientes de mantener contacto directo con familiares y amigos que por distintas circunstancias no logran trasladarse a la ciudad de Bogotá. Esta sede además de prestar un servicio local, presta servicio completo en la Guajira en Villanueva y Fonseca.
											</div> 
											<div style="width: 310px; float: right; ">En noviembre de 2009 en el barrio San Vicente de Cali se dio apertura a la Funeraria Jardines del Recuerdo. La empresa creó un estándar en infraestructura moderno y elegante que se ve reflejado en sus dos salas de velación, con sus atriles y catafalcos elaborados en vidrio y acero inoxidable, sus espacios blancos y bien distribuidos que permiten la comodidad de los acompañantes y la diferenciación de nuestro servicio.</div>
										</div>
									</article>
								</div>
								<!-- ini-data-years  -->
								<div class="timeline-item-content" data-years="2011,2012">
									<article class="timeline-item" style="width:620px;">
										<div class="timeline-item-title">2011</div>
										<div class="timeline-item-text t-justify">
											<div style="width: 305px; float: left; margin: 0 0 10px 0;">
											Se inauguró la nueva funeraria Jardines de Cartagena que está ubicada dentro de las instalaciones del parque cementerio lo que permite que los seres queridos estén acompañados en todas las instancias del servicio y al mismo tiempo se genere un mejor manejo del duelo. 
											<br/><br/>Son 600 mt2 de un proyecto que pretende brindar un excelente servicio a través de la imagen. Tiene como objetivo mejorar el entorno y tener un impacto positivo en ese sector de la ciudad.. El estilo contemporáneo, las 6 modernas y sobrias salas de velación con aire acondicionado, los espacios altos, la ubicación de los asientos, el diseño de los altares, amplios espacios y los recintos privados para los familiares; generan ambientes de recogimiento, reflexión y tranquilidad.
											</div> 
											<div style="width: 285px; float: right;">
											Se cambió la entrada del parque cementerio Jardines del Recuerdo en Bogotá con un nombre hecho en acero inoxidable  incrustado en una piedra negra cubierta de agua.  
											<br/><br/>En septiembre de 2011 entra en operación la nueva Funeraria Jardines del Recuerdo – Sede San Fernando en la ciudad de Bogota. Este diseño fue una remodelación de una antigua funeraria que se compró. Además de ser agradable arquitectónicamente, esta construcción cuenta con la norma sismoresistente (NSR -10) expedida por el Gobierno colombiano en el año 2010. 
											</div>
										</div>
									</article>
									<article class="timeline-item" style="width:310px; background: #E9EFF4;">
										<div class="timeline-item-title c-blue">2012</div>
										<div class="timeline-item-text t-justify">
											<div>En el mes de Abril se inagura en la ciudad de Barranquilla el nuevo edificio de Grupo Recordar ubicado en una de las zonas comerciales mas importantes de la ciudad. Este edificio cuenta con 5 pisos con los que se busca atender a nuestros clientes en un mismo lugar.   
												<br/><br/>Se da inicio a la construcción del Parque Cementerio Jardines de Valledupar, ubicado en el Kilometro 4 via Valencia de Jes&uacute;s.
											</div> 
										</div>
									</article>
								</div>
								<div class="timeline-item-content" data-years="2013">
									<article class="timeline-item" style="width:310px;">
										<div class="timeline-item-title c-blue">2013</div>
										<div class="timeline-item-text t-justify">
											<div>Bajo conceptos de paisajismo y de preservación del medio ambiente, y respaldados en 40 años de experiencia en este negocio; se entregó el nuevo parque cementerio “Jardines de Valledupar”.  Un diseño de paisaje de conservación y renovación en casi 14 hectáreas de extensión. Este proyecto es un homenaje al pueblo vallenato, a su tierra y el respeto que expresamos por su raza.</div> 
										</div>
									</article>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
	    </div>	
        <!-- End Content --> 
       <div id="push">&nbsp;</div>
    </div>
    <!--End wrapper--> 
   <?php include(APPPATH.'views/inc/footer.php');?>
</body>
</html>