<?php
class MY_Model extends CI_Model {

	protected $table;
	private $state = FALSE;
	public $valor = 50;

	public function save($data){

		$this->db->insert($this->table, $data);

		return ($this->db->affected_rows() != 1) ? $this->state : $this->get(array('id'=> $this->db->insert_id()));

	}

	public function update($id, $data){

		$this->db->where('id', $id)->update($this->table, $data);

		return ($this->db->affected_rows() != 1) ? $this->state : $this->get(array('id' => $id));

	}

	public function delete($id, $key = 'id', $table  = ""){

			$this->db->where(array($key => $id));

			if($table == ""){
				$this->db->delete($this->table);
			}else{
				$this->db->delete($table);
			}


   		return ($this->db->affected_rows() != 1) ? $this->state : !$this->state;

	}



	public function get($filter){

		return $this->db->get_where($this->table, $filter)->row();

	}

	public function getAll($filter = NULL){
		if(!is_null($filter)){
			return $this->db->get($this->table)->result();
		}else{
			return $this->db->get_where($this->table, $filter)->result();
		}
	    //return !$filter?$this->db->get($this->table)->result():$this->db->get_where($this->table, $filter)->result();

	}

}
