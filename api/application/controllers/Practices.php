<?php

defined('BASEPATH') || exit('No direct script access allowed');

require APPPATH . 'controllers/Auth.php';


class Practices extends Auth
{
	private $requiredFields = array(
	    'title' => array(
	        Auth::REQUIRED_TEXT => TRUE
	    ),
	    'idCommitment' => array(
	        Auth::REQUIRED_TEXT => TRUE
		),
	    'description' => array(
	        Auth::REQUIRED_TEXT => TRUE
		),
	    'benefits' => array(
	        Auth::REQUIRED_TEXT => TRUE
		),
	    'challenges' => array(
	        Auth::REQUIRED_TEXT => TRUE
		),
	    'extraInfo' => array(
	        Auth::REQUIRED_TEXT => TRUE
		)
	);
	private $singleText = "Practice";
	private $pluralText = "Practices";

	public function __construct(){
		parent::__construct();
		$this->load->Model('PracticesModel');
		$this->load->Model('ClubsModel');
		
	}

	public function index_post(){
	    $user = (object) $this->checkSession();
	    if( $user->status ){
	        
	        $data = $this->input->post(null, true);
			
			$club = $this->ClubsModel->getByUserId($user->response->sub);
			
			$this->validateData( $data, $this->requiredFields );
			$data['idClub'] = $club->id;
	        $response = $this->PracticesModel->save($data);
	        
	        if($response) {
	            $this->__ResponseSuccess($this->singleText, $response);
	        }else{
	            $this->__ResponseError(Auth::SAVE_ERROR);
	        }
	        
	    }
	}

	public function index_put(){
	    
	    if( $this->checkAdmin() ){
	        
	        $data = $this->put(null, true);
	        $required[] = 'id';
	        $this->validateData( $data, $required );
	        
	        $id = $data['id'];
	        unset($data['id']);
	        
	        $response = $this->PracticesModel->update($id, $data);
	        
	        if( $response ){
	            $this->__ResponseSuccess($this->singleText, $response);
	        }
	        else{
	            $this->__ResponseError(Auth::UPDATE_ERROR);
	        }
	        
	    }
	    
	}
	
	public function index_delete(){
	    
	    if( $this->checkAdmin() ){
	        $id = $this->delete('id');
	        if( $id ){
	            
	            if($this->PracticesModel->delete($id)) {
	                $this->__ResponseSuccess("Success", "success");
	            }else{
	                $this->__ResponseError(Auth::DELETE_ERROR);
	            }
	            
	        }
	        
	    }
	    
	}
	
	public function index_get(){
	    
	   // if( $this->checkAdmin() ){
	        
	        $id = $this->get('id');
	        if( $id ){

				$this->__ResponseSuccess( $this->singleText, $this->PracticesModel->getAll( $id )  );
				
	        }else{

				$this->__ResponseSuccess( $this->pluralText, $this->PracticesModel->getAll() );
				
	        }
	        
	   // }
	    
	}

}