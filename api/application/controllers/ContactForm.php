<?php

defined('BASEPATH') || exit('No direct script access allowed');

require APPPATH . 'controllers/Auth.php';


class ContactForm extends Auth
{
	private $requiredFields = array();
	private $singleText = "ContactForm";
	private $pluralText = "ContactsForms";

	public function __construct(){
		parent::__construct();
		$this->load->Model('ContactFormModel');
		$this->subject = "Contact Form";
		$this->to = "diana@zavgroup.com, katollara@hotmail.com";
	}

	public function index_post(){
	    
		$data = $this->input->post(null, true);
		
		$email = array(
			'msg' => $this->load->view('mail/contact', $data , true),
			'to' => $this->to,
			'subject' => $this->subject
		); 

		$this->sendMail($email);

		
        
        $this->validateData( $data, $this->requiredFields );
        $response = $this->ContactFormModel->save($data);
        
        if($response) {
			
			$email = array(
				'msg' => $this->load->view('mail/contact_user', $data , true),
				'to' => $data->email,
				'subject' => $this->subject
			); 

			$this->sendMail($email);
            $this->__ResponseSuccess($this->singleText, $response);
        }else{
            $this->__ResponseError(Auth::SAVE_ERROR);
        }
	        
	}

	public function index_put(){
	    
	    if( $this->checkAdmin() ){
	        
	        $data = $this->put(null, true);
	        $required[] = 'id';
	        $this->validateData( $data, $required );
	        
	        $id = $data['id'];
	        unset($data['id']);
	        
	        $response = $this->ContactFormModel->update($id, $data);
	        
	        if( $response ){
	            $this->__ResponseSuccess($this->singleText, $response);
	        }
	        else{
	            $this->__ResponseError(Auth::UPDATE_ERROR);
	        }
	        
	    }
	    
	}
	
	public function index_delete(){
	    
	    if( $this->checkAdmin() ){
	        $id = $this->delete('id');
	        if( $id ){
	            
	            if($this->ContactFormModel->delete($id)) {
	                $this->__ResponseSuccess("Success", "success");
	            }else{
	                $this->__ResponseError(Auth::DELETE_ERROR);
	            }
	            
	        }
	        
	    }
	    
	}
	
	public function index_get(){
	    
	    // Everybody can view
        $id = $this->get('id');
        if( $id ){
            $this->__ResponseSuccess($this->singleText, $this->ContactFormModel->get(array('id' => $id)));
        }else{
            $this->__ResponseSuccess($this->pluralText, $this->ContactFormModel->getAll());
        }
	    
	}

}