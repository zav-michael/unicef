<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/Auth.php';


class Recommendations extends Auth
{

	private $table = "Recommendation";

	public function __construct(){

		parent::__construct();
		$this->load->Model('RecommendationModel');
		$this->localPath = 'application/upload/';
		$this->singleText = 'Recommendation';
		$this->load->model('SectionsModel');
		$this->load->model('AnswerModel');

	}


	/**
	 * @api {post} /questions/question Nueva pregunta
	 * @apiVersion 0.1.0
	 * @apiName Nueva Pregunta
	 * @apiGroup Preguntas
	 *
	 * @apiParam {String} index No recuerdo para que se usa.
	 * @apiParam {String} description La pregunta formulada.
	 * @apiParam {String} section Sección a la que pertenece la pregunta (Tema 1, Tema 2 ...).
	 *
	 * @apiSuccess {Bool} status  true.
	 * @apiSuccess {string} success  Mensaje.
	 *
	 * @apiSuccessExample Success Response:
	 *     HTTP/1.1 200 OK
	 *      {
	 *        "status": true,
	 *        "success": "Success"
	 *     }
	 * @apiError {Bool} status False Ha Ocurrido un Error.
	 * @apiError {JSON} error Objeto Json que contiene la descripción del error.
	 *
	 * @apiErrorExample Error Response:
	 *     HTTP/1.1 400 Not Found
	 *      {
	 *        "status": false,
	 *        "error": {
	 *          "message": "Could not Save"
	 *        }
	 *     }
	 */


	public function index_post(){

		if( $this->checkAdmin() ){

			$data = $this->post();

			$this->validateData( $data, $this->requiredFields );

			if($this->RecommendationModel->save($data)) {
				$this->response(array('status' => TRUE, 'successs' => 'Success'));
			}else{
				$this->response(array('status' => FALSE, 'error' => array( 'message' => 'Could not Save' )), 400);
			}
			
		}

	}


	/**
	 * @api {put} /questions/question Actualizar Pregunta
	 * @apiVersion 0.1.0
	 * @apiName Actualizar Pregunta
	 * @apiGroup Preguntas
	 *
	 * @apiParam {String} index No recuerdo para que se usa.
	 * @apiParam {String} description La pregunta formulada.
	 * @apiParam {String} section Sección a la que pertenece la pregunta (Tema 1, Tema 2 ...).
	 * @apiParam {id} id Identificador único de la pregunta.
	 *
	 * @apiSuccess {Bool} status  true.
	 * @apiSuccess {string} success  Mensaje.
	 *
	 * @apiSuccessExample Success Response:
	 *     HTTP/1.1 200 OK
	 *      {
	 *        "status": true,
	 *        "success": "Success"
	 *     }
	 * @apiError {Bool} status False Ha Ocurrido un Error.
	 * @apiError {JSON} error Objeto Json que contiene la descripción del error.
	 *
	 * @apiErrorExample Error Servidor:
	 *     HTTP/1.1 400 Not Found
	 *      {
	 *        "status": false,
	 *        "error": {
	 *          "message": "Could not Update"
	 *        }
	 *     }
	 *  @apiErrorExample Error Autenticación
	 *     HTTP/1.1 403 Forbidden
	 *     {
	 *      	"status":false,
	 *      	"error":{
	 *      		"message":"Unauthorized"
	 *      	 }
	 *     }
	 */

    public function all_put(){
		
		if( $this->checkAdmin() ){
			
			$data = $this->put(null, true);
			
			if(!empty($data)){

				foreach($data as $key => $item):
					$response = $this->RecommendationModel->update($key, array('question' => $item));
				endforeach;

				$this->__ResponseSuccess("Success", "success");

			}else{
			  $this->__ResponseError(Auth::UPDATE_ERROR);
			}
					
			
			if( $response ){
				$this->__ResponseSuccess($this->singleText, $response);
			}
			else{
				$this->__ResponseError(Auth::UPDATE_ERROR);
			}
			
		}			
		
    }

	public function index_put(){

		if( $this->checkAdmin() ){

			$data = $this->put(null, true);

			$required = $this->requiredFields;
			$required[] = 'id';
			$this->validateData( $data, $required );

			$id = $data['id'];
			unset($data['id']);

			if($this->RecommendationModel->update($id, $data)){

				$this->response(array('status' => TRUE, 'successs' => 'Success'));

			}
			else{
				$this->response(array('status' => TRUE, 'error' => array('message' => 'Could not Update' )));
			}

		}

	}


	public function item_put(){
		
		if( $this->checkAdmin() ){
			
			$data = $this->put(null, true);
			
			$id = $data['id'];
			unset($data['id']);
			
			if(isset($data['question'])){
				unset($data['question']);
			}

			if($this->RecommendationModel->update($id, $data)){

				$this->response(array('status' => TRUE, 'successs' => 'Success'));

			}
			else{
				$this->response(array('status' => TRUE, 'error' => array('message' => 'Could not Update' )));
			}			
			
		}						
		
	}


	public function guide_post(){

		$data = $this->post(null, true);
		$response = false;
		$id = $data['id'];
		unset($data['id']);
		if(!empty($_FILES)){
			$this->set_custom_fields_files($_FILES, $id);
			$response = true;
		}
		if($response) {
			$this->__ResponseSuccess($this->singleText, $response);
		}else{
			$this->__ResponseError(Auth::SAVE_ERROR);
		  } 

	}


	private function set_custom_fields_files($files, $id){
		
		foreach ($_FILES["custom"]["error"] as $clave => $error) {
			if ($error == UPLOAD_ERR_OK) {
						$ext = pathinfo($_FILES["custom"]["name"][$clave], PATHINFO_EXTENSION);
				$nombre_tmp = $_FILES["custom"]["tmp_name"][$clave];
				$nombre = md5($_FILES["custom"]["tmp_name"][$clave].date("Y-m-d H:i:s")).'.'.$ext;
				$response = $this->RecommendationModel->update($id, array('guide' => $this->localPath."/$nombre"));
				move_uploaded_file($nombre_tmp, $this->localPath."/$nombre");
			}
		}			
		
	}

	/**
	 * @api {delete} /questions/question/:id Eliminar Pregunta
	 * @apiVersion 0.1.0
	 * @apiName Eliminar Pregunta
	 * @apiGroup Preguntas
	 *
	 * @apiParam {number} id identificador único de la pregunta.
	 *
	 * @apiSuccess {Bool} status  true.
	 * @apiSuccess {string} success  Mensaje.
	 *
	 * @apiSuccessExample Success Response:
	 *     HTTP/1.1 200 OK
	 *      {
	 *        "status": true,
	 *        "success": "Success"
	 *     }
	 * @apiError {Bool} status False Ha Ocurrido un Error.
	 * @apiError {JSON} error Objeto Json que contiene la descripción del error.
	 *
	 * @apiErrorExample Error Servidor:
	 *     HTTP/1.1 400 Not Found
	 *      {
	 *        "status": false,
	 *        "error": {
	 *          "message": "Could not delete"
	 *        }
	 *     }
	 *  @apiErrorExample Error Autenticación
	 *     HTTP/1.1 403 Forbidden
	 *     {
	 *      	"status":false,
	 *      	"error":{
	 *      		"message":"Unauthorized"
	 *      	 }
	 *     }
	 */

	public function index_delete(){

		if( $this->checkAdmin() ){
			$id = $this->query('id');
			if( $id ){

				if($this->RecommendationModel->delete($id)) {
					$this->response(array('status' => TRUE, 'successs' => 'Success'));
				}else{
					$this->response(array('status' => FALSE, 'error' => array( 'message' => 'Could not Delete' )), 400);
				}

			} 

		}

	}

	/**
	 * @api {get} /questions/questions Obtener las preguntas
	 * @apiVersion 0.1.0
	 * @apiName Obtener las preguntas
	 * @apiGroup Preguntas
	 *
	 *
	 * @apiSuccess {Bool} status  true.
	 * @apiSuccess {string} success  Mensaje.
	 *
	 * @apiSuccessExample Success Response:
	 *     HTTP/1.1 200 OK
 	 * {
	 *  "status": true,
	 *  "questions": [
	 *    {
	 *      "id": "4",
	 *      "index": "1",
	 *      "description": "¿how works the real world?-2",
	 *      "section": "Tema 1",
	 *      "answers": [
	 *        {
	 *          "id": "1",
	 *          "description": "Preguntinha 2",
	 *          "Questions_id": "4",
	 *          "Colors_id": "1"
	 *        }
	 *      ]
	 *    },
	 *    {
	 *      "id": "5",
	 *      "index": "1",
	 *      "description": "Preguntinha 2",
	 *      "section": "Tema 2",
	 *      "answers": []
	 *    }
	 *  ]
	 *}
	 * @apiError {Bool} status False Ha Ocurrido un Error.
	 * @apiError {JSON} error Objeto Json que contiene la descripción del error.
	 *
	 *  @apiErrorExample Error Autenticación
	 *     HTTP/1.1 403 Forbidden
	 *     {
	 *      	"status":false,
	 *      	"error":{
	 *      		"message":"Unauthorized"
	 *      	 }
	 *     }
	 */

	public function index_get(){

		$session = $this->checkSession();
		if( $session['status'] ){

			$questions = $this->RecommendationModel->getSections();

			$this->response(array('status' => TRUE, 'questions' => $questions));

		}else{
			
			$this->response($session);

		}

	}


	public function steps_get(){
		
		$session = $this->checkSession();

		if( $session['status'] ){

			$id = $this->get('id');
			
			if( $id ){
				
				$this->__ResponseSuccess('Recommendations', $this->RecommendationModel->getRecommendBySection($id));

			}

		 }else{
			
		   	$this->response($session);

		 }			
		
	}


	public function stepsWithTitle_get(){
		
		$session = (object)$this->checkSession();

		if( $session->status ){

			$id = $this->get('id');
			
			if( $id ){
				
				$this->__ResponseSuccess('Recommendations', array(
					'section' => $this->SectionsModel->get(array('id' => $id)), 
					'recommendations' =>$this->AnswerModel->getSectionsResultsWithRecommendation($id, $session->response)));

			}

		}		
		
	}

}
