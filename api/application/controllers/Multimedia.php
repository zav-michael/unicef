<?php

defined('BASEPATH') || exit('No direct script access allowed');

require APPPATH . 'controllers/Files.php';


class Multimedia extends Files
{
	private $requiredFields = array('name', 'type', 'indicators_id');
	private $singleText = "Multimedia";

	public function __construct(){
	    parent::__construct();
	    $this->load->Model('MultimediaModel');
	    $this->load->Model('ClubsModel');
	}
	
	public function index_post(){
	    $user = (object)$this->checkSession();
	    if( $user->status ){
	       
			$data = $this->post();

			$data['Users_id'] = $user->response->sub;

	        $uploadInfo = $this->__UploadFile();
	        
	        $data = $this->__CompleteData($data, $uploadInfo);

	        $response = $this->MultimediaModel->save( $data );
	        
	        if($response) {
	            $this->__ResponseSuccess( $this->singleText, $response);
	        }else{
	            $this->__ResponseError( Auth::SAVE_ERROR );
	        }
	        
	    }
	}
	 
	public function update_post(){
	    
	    if( $this->checkAdmin() ){
	        
	        $data = $this->put(null, true);
	        $required[] = 'id';
	        
	        $uploadInfo = $this->__UploadFile();
	        $data = $this->__CompleteData($data, $uploadInfo);
	        
	        $this->validateData( $data, $required );
	        
	        $id = $data['id'];
	        unset($data['id']);
	        
	        $response = $this->MultimediaModel->update($id, $data);
	        
	        if( $response ){
	            $this->__ResponseSuccess($this->singleText, $response);
	        }
	        else{
	            $this->__ResponseError(Auth::UPDATE_ERROR);
	        }
	        
	    }
	    
	}
	
	public function index_delete(){
	    
	    if( $this->checkAdmin() ){
	        $id = $this->delete('id');
	        if( $id ){
	            
	            if($this->MultimediaModel->delete($id)) {
	                $this->__ResponseSuccess("Success", "success");
	            }else{
	                $this->__ResponseError(Auth::DELETE_ERROR);
	            }
	            
	        }
	        
	    }
	    
	}
	
	public function index_get(){

		$user = (object)$this->checkSession();
		$data = $this->get();

		if(!isset($data['club'])){

			$user = (object)$this->checkSession();

		}else{

			$user = new stdClass();
			$user->status = true;
			$userData = $this->ClubsModel->getUser($data['club']);
			$user->response = new stdClass();
			$user->response->sub = $userData->id;
			
		}

	    if( $user->status ){
	        
			$id = $this->get('indicator');
			if($id){
				$res = $this->MultimediaModel->getAll(array('indicators_id' => $id, 'users_id' => $user->response->sub));
				
				$res = $this->filter($res);
			}else{
				$res = array();
			}
		
			$this->__ResponseSuccess( $this->singleText, $res );
	        
	    }
	    
	}

	protected function filter( $response ){

		$filter = array('images' => array(), 'videos' => array(), 'documents' => array());

		foreach($response as $item){

			$item->path = base_url('application/upload/'.$item->name);

			if( $this->isImage($item->type) ){

				$filter['images'][] = $item;

			}

			if( $this->isVideo($item->type) ){
				$filter['videos'][] = $item;
			}

			if( $this->isDocument($item->type) ){
				$filter['documents'][] = $item;
			}
			
		}

		return $filter;
	}

	private function isImage( $type ){
		return in_array($type, explode('|',$this->imagesTypes));
	}

	private function isVideo( $type ){
		return in_array($type, explode('|',$this->videoTypes));
	}

	private function isDocument( $type ){
		return in_array($type, explode('|',$this->filesTypes));
	}
	
}