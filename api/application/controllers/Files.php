<?php

defined('BASEPATH') || exit('No direct script access allowed');

require APPPATH . 'controllers/Auth.php';

class Files extends Auth
{
    
    protected $imagesTypes = "jpg|gif|png|jpeg";
    protected $filesTypes = "pdf|xls|xlsx|doc|docx|ppt|pptx";
    protected $videoTypes = "mp4|mpeg";
    
    protected function __fileConfig( $filetype ){
        
        $config['upload_path']          = './application/upload';
        $config['allowed_types']        = $this->__allowedTypes( $filetype );
        $config['max_size']             =  100000;
        //$config['max_width']            = 1024;
        //$config['max_height']           = 768;
        
        return $config;
        
    }
    
    protected function __allowedTypes( $filetype ){
        
        $filetype = explode('/', $filetype);
        
        switch ( $filetype[0] ) {
            case 'image':
                return $this->imagesTypes;
                break;
            case 'application':
                return $this->filesTypes;
                break;
            case 'video':
                return $this->videoTypes;
                break;
            default:
                return $this->imagesTypes.'|'.$this->filesTypes.'|'.$this->videoTypes;
                break;
        }
        
    }
    
    protected function __getExtension( $file ){
        
        return pathinfo( $file )['extension']?:'unknown';
        
    }
    
    protected function __UploadFile( $restrictedType = NULL ){
        
        $config = empty($restrictedtype)?$this->__fileConfig( $restrictedType ) : $this->__fileConfig( $_FILES['file']['type'] );
        $this->load->library( 'upload', $config );
        
        if ( ! $this->upload->do_upload('file') )
        {
            $this->__ResponseError( $this->upload->display_errors() );
        }else{
            $uploadInfo = $this->upload->data();
        }
        
        return $uploadInfo;
        
    }
    
    public function __CompleteData( $data, $uploadInfo ){
        
        $data['name'] = $uploadInfo['file_name'];
        $data['type'] = $this->__getExtension( $uploadInfo['full_path'] );
        
        if( ! isset( $data['id'] ) ){
            $data['created'] = date('Y/m/d');
        }
        
        return $data;
        
    }

    public function __UploadB64File($data){

        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $image = base64_decode($data);
        $name = 'application/upload/'.$this->randomName().'.png';
        file_put_contents(FCPATH.$name, $image);
        return $name;

    }
    
    private function randomName(){
        return md5(microtime());
    }
}
