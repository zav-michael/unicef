<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/Auth.php';


class Indicators extends Auth
{
	private $requiredFields = array(
			'action', 'goal', 'goal_one_year',
			'indicator', 'formula', 'means_of_verification', 'source_of_information',
			'period', 'action_types_id', 'Commitments_id'
		);
	
	private $singleText = "Indicator";
	private $pluralText = "Indicators";
	
	public function __construct(){
		parent::__construct();
		$this->load->Model('IndicatorsModel');
	}

	public function index_post(){

		if( $this->checkAdmin() ){

			$data = $this->post(null, true);

			$this->validateData( $data, $this->requiredFields );
			$response = $this->IndicatorsModel->save( $data );

			if($response) {
				$this->__ResponseSuccess( $this->singleText, $response );
			}else{
				$this->__ResponseError( Auth::SAVE_ERROR );
			}

		}
	}

	public function index_put(){

		if( $this->checkAdmin() ){

			$data = $this->put(null, true);

			$required = $this->requiredFields;
			$required[] = 'id';
			$this->validateData( $data, $required );

			$id = $data['id'];
			unset($data['id']);

			$response = $this->IndicatorsModel->update($id, $data);

			if($response){

				$this->__ResponseSuccess( $this->singleText, $response );

			}
			else{
				$this->__ResponseError( Auth::UPDATE_ERROR );
			}

		}

	}

	public function index_delete(){

		if( $this->checkAdmin() ){
			$id = $this->delete('id');
			if( $id ){

				if($this->IndicatorsModel->delete($id)) {
					$this->__ResponseSuccess( 'successs', 'Success' );
				}else{
					$this->__ResponseError(Auth::DELETE_ERROR, 400);
				}

			} 

		}

	}

	public function index_get(){

		if( $this->checkAdmin() ){
			$id = $this->get('id');
			if( $id ){
				
			    $this->__ResponseSuccess( $this->singleText, $this->IndicatorsModel->get(array('id' => $id) ));

			}else{
			    
			    $this->__ResponseSuccess( $this->pluralText, $this->IndicatorsModel->getAll() );
			    
			}

		}

	}

}