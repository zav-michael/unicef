<?php

defined('BASEPATH') || exit('No direct script access allowed');

// REST Controller 
require APPPATH . 'libraries/REST_Controller.php'; 
require APPPATH . 'controllers/Validator.php'; 
require APPPATH . 'controllers/Email.php'; 

class Auth extends REST_Controller
{
    
    private $accessKey = "Authorization";
    // Messages 
    const HTTP_UNAUTHORIZED_MESSAGE = "Access denied, You Don't have permission";
    const AUTH_INVALID_TOKEN = "Invalid Token";
    const UNEXPECTED_ERROR = "An unexpected error has occurred";
    const UPDATE_ERROR = "Could not Update";
    const DELETE_ERROR = "Could not Delete";
    const SAVE_ERROR = "Could not Save";
    const REQUIRED_TEXT = "required";
    const TYPE_TEXT = "type";
    const FORMAT_TEXT = "format";
    const MINLENGHT_TEXT = "minlenght";
    const MAXLENGHT_TEXT = "maxlenght";
    
    const STATUS_TEXT = "status";

    protected $emailSender = array(
        'protocol' => 'smtp'
    );
    

    public function __construct(){
        parent::__construct();
        $this->load->Model('Users_Model');
        $this->_rawData();
    }

    /**
     * 
     * Se comprueba que la session sea v�lida
     * 
     */
    protected function checkSession( $responsedata = FALSE )
    {
        $headers = $this->input->request_headers();
        
        if (array_key_exists($this->accessKey, $headers) && !empty( $headers[$this->accessKey] )) {
            
            try{

                $decodedToken = AUTHORIZATION::validateToken($headers[$this->accessKey]);

                if ( $decodedToken ) {

                    $decodedToken->token = $headers[$this->accessKey];

                    if( $decodedToken->exp < time() && !$responsedata ){

                        $this->__ResponseError( self::AUTH_INVALID_TOKEN, REST_Controller::HTTP_FORBIDDEN);

                    }elseif( $decodedToken->exp < time() && $responsedata ){
                        
                        return array( self::STATUS_TEXT => FALSE, 'error' => array('message' => 'Invalid Token') );
                        
                    }

                    return array( self::STATUS_TEXT => true, 'response' => $decodedToken);

                }else{
                    
                    if( !$responsedata ){
                        
                        $this->__ResponseError( self::AUTH_INVALID_TOKEN, REST_Controller::HTTP_FORBIDDEN);
                        
                    }else{
                        
                        return array( self::STATUS_TEXT => false);
                        
                    }
                    
                }

            }
            catch(Exception $e){
        
                $this->__ResponseError( $e->getMessage() );

            }
        }
        else{
            if( !$responsedata ){
                
                $this->__ResponseError( self::HTTP_UNAUTHORIZED_MESSAGE, REST_Controller::HTTP_UNAUTHORIZED );
                
            }else{
                
                return array( self::STATUS_TEXT => false);
                
            }
            

        }
    }
    
    protected function checkAdmin( $responseBool = false ){

        $response = $this->checkSession();
        if( $response[self::STATUS_TEXT] ){
            if( !$this->Users_Model->is_admin( $response['response']->sub ) ){
                return (!$responseBool)?$this->__ResponseError( self::HTTP_UNAUTHORIZED_MESSAGE, REST_Controller::HTTP_UNAUTHORIZED ): false;
            }
            else{
                return true;
            }

        }

    }

    protected function checkParticipant( $responseBool = false ){

        $response = $this->checkSession();
        if( $response[self::STATUS_TEXT] ){

            if( !$this->Users_Model->is_participant( $response['response']->sub ) ){
                return (!$responseBool)?$this->__ResponseError( self::HTTP_UNAUTHORIZED_MESSAGE, REST_Controller::HTTP_UNAUTHORIZED ): false;
            }
            else{
                return true;
            }

        }

    }

    protected function checkNonParticipant( $responseBool = false ){

        $response = $this->checkSession();
        if( $response[self::STATUS_TEXT] ){

            if( !$this->Users_Model->is_NonParticipant( $response['response']->sub ) ){
                return (!$responseBool)?$this->__ResponseError( self::HTTP_UNAUTHORIZED_MESSAGE, REST_Controller::HTTP_UNAUTHORIZED ): false;
            }
            else{
                return true;
            }

        }

    }

    protected function validateData( $data, $requiredFields ) {
        
        $validator = new Validator($requiredFields);
        $validator->validate($data);
        
        $errors = $validator->__getErrors();
        
        if( count($errors) > 0 ){
            $this->__ResponseError($errors);
        }

    }

    protected function sendMail($config){

        $conf = array_merge($config, $this->emailSender);
        $email = new Email( $conf );
        return $email->send();

    }

    protected function __ResponseError( $error, $HTTPCode = REST_Controller::HTTP_INTERNAL_SERVER_ERROR ) {

        $this->response( array( self::STATUS_TEXT => false, 'error' => array('message' => $error ) ), $HTTPCode );

    }
    
    protected function __ResponseSuccess( $key, $response, $HTTPCode = REST_Controller::HTTP_OK ){
        
        $this->response( array( self::STATUS_TEXT => true, $key => $response ), $HTTPCode );
        
    }

    public function _rawData(){

        $rest_json = file_get_contents("php://input");
        if($rest_json){
           $_POST = json_decode($rest_json, true);
           $_GET = json_decode($rest_json, true);
        }
    
    }

}