<?php

defined('BASEPATH') || exit('No direct script access allowed');

require APPPATH . 'controllers/Auth.php';


class Taxonomy extends Auth
{

	private $requiredFields = array(
	    'title' => array(
	        Auth::REQUIRED_TEXT => TRUE
	    ),
	    'body' => array(
	        Auth::REQUIRED_TEXT => TRUE,
	    ),
        'colors_id' => array( Auth::TYPE_TEXT => "INT" )
	);

	private $required = array(
		'title' => array(
	        Auth::REQUIRED_TEXT => TRUE
		),
		'tags' => array(
	        Auth::REQUIRED_TEXT => TRUE
	    )
	);


	private $singleText = "taxonomy";
	private $pluralText = "taxonomy";

	public function __construct(){
		parent::__construct();
		$this->load->Model('TaxonomyModel');
		$this->load->Model('ContentsModel');
		$this->localPath = 'application/upload/';
	}

    public function index_get()
    {
        $data = $this->input->get(NULL, true);
        if(!isset($data)){
            $data = array();
        }
        $this->__ResponseSuccess($this->pluralText, $this->TaxonomyModel->getAllWithFields($data));
	}


	private function custom_fields_files($files, $id)
	{
		foreach ($files["custom"]["error"] as $clave => $error) 
		{
			if ($error == UPLOAD_ERR_OK) {
				$ext = pathinfo($files["custom"]["name"][$clave], PATHINFO_EXTENSION);
				$nombre_tmp = $files["custom"]["tmp_name"][$clave];
				$nombre = md5($files["custom"]["tmp_name"][$clave].date("Y-m-d H:i:s")).'.'.$ext;
				$this->ContentsModel->add_fields(array('value' => $this->localPath.$nombre, 'content_id' => $id, 'custom_fields_id' => 11));
				move_uploaded_file($nombre_tmp, $this->localPath."/$nombre");
			}
		}
	}
	

	public function content_post()
	{
		$data = $this->post(null, true);
		$this->validateData( $data, $this->required );
		$tags = $data['tags'];
		unset($data['tags']);
		$term = $data['term'];
		unset($data['term']);
		$response = $this->ContentsModel->save($data);
		$this->ContentsModel->add_fields(array('value' => $tags, 'content_id' => $response->id, 'custom_fields_id' => 21));
		$this->TaxonomyModel->add_contents_terms(array('content_id' => $response->id, 'terms_id' => $term));
		  if(isset($_FILES)){
		   	$this->custom_fields_files($_FILES, $response->id);
		   }	
		if($response) {
			$this->__ResponseSuccess($this->singleText, $response);
		}else{
			$this->__ResponseError(Auth::SAVE_ERROR);
		}
	}


}
