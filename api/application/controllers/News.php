<?php

defined('BASEPATH') || exit('No direct script access allowed');

require APPPATH . 'controllers/Files.php';

class News extends Files
{
    const IMAGE_NAME = "image";
    private $requiredFields = array(
        SELF::IMAGE_NAME => array(
            Auth::REQUIRED_TEXT => TRUE
        ), 
        'title' => array(
            Auth::REQUIRED_TEXT => TRUE
        ), 
        'body' => array(
            Auth::REQUIRED_TEXT => TRUE
        ), 
        'url' => array(
            Auth::REQUIRED_TEXT => TRUE
        ), 
    );
    private $singleText = "New";
    private $pluralText = "News";
    
    public function __construct(){
        parent::__construct();
        $this->load->Model('NewsModel');
    }
    
    public function index_post(){
        
        if( $this->checkAdmin() ){
            
            $data = $this->input->post(null, true);
            
            $uploadInfo = $this->__UploadFile( SELF::IMAGE_NAME );
            
            $data = $this->__CompleteData($data, $uploadInfo);
            
            $this->validateData( $data, $this->requiredFields );
            $response = $this->NewsModel->save($data);
            
            if($response) {
                $this->__ResponseSuccess($this->singleText, $response);
            }else{
                $this->__ResponseError(Auth::SAVE_ERROR);
            }
            
        }
    }
    
    public function update_post(){
        
        if( $this->checkAdmin() ){
            
            $data = $this->input->post(null, true);

            $uploadInfo = $this->__UploadFile( SELF::IMAGE_NAME );
            
            if( !empty($uploadInfo) ){
                $data = $this->__CompleteData($data, $uploadInfo);
            }
            $required = array('id' => array(Auth::REQUIRED_TEXT => TRUE));
            $this->validateData( $data, $required );
            
            $id = $data['id'];
            unset($data['id']);
            
            $response = $this->NewsModel->update($id, $data);
            
            if( $response ){
                $this->__ResponseSuccess($this->singleText, $response);
            }
            else{
                $this->__ResponseError(Auth::UPDATE_ERROR);
            }
            
        }
        
    }
    
    public function index_delete(){
        
        if( $this->checkAdmin() ){
            $id = $this->delete('id');
            if( $id ){
                
                if($this->NewsModel->delete($id)) {
                    $this->__ResponseSuccess("Success", "success");
                }else{
                    $this->__ResponseError(Auth::DELETE_ERROR);
                }
                
            }
            
        }
        
    }
    
    public function index_get(){
  
        $id = $this->get('id');
        if( $id ){
            $this->__ResponseSuccess($this->singleText, $this->NewsModel->get(array('id' => $id)));
        }else{
            $this->__ResponseSuccess($this->pluralText, $this->NewsModel->getAll());
        }
        
    }
    
    public function __CompleteData($data, $uploadInfo){
        
        $data[SELF::IMAGE_NAME] = $uploadInfo['file_name'];
        
        if( ! isset( $data['id'] ) ){
            $data['created'] = date('Y/m/d');
        }
        
        return $data;
        
    }
    
}