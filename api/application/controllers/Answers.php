<?php

defined('BASEPATH') || exit('No direct script access allowed');

require APPPATH . 'controllers/Auth.php';


class Answers extends Auth
{

	private $requiredFields = array(
	    'questions_id' => array(
	        Auth::REQUIRED_TEXT => TRUE,
	        Auth::TYPE_TEXT => 'INT',
	    ),
	    'value' => array(
	        Auth::REQUIRED_TEXT => TRUE,
	    ),
	    'custom_fields_id' => array(
	        Auth::REQUIRED_TEXT => TRUE,
	    )
	);
	
	private $singleText = "Answer";
	private $pluralText = "Answers";

	public function __construct(){
		parent::__construct();
		$this->load->Model('AnswerModel');
	}

	public function index_get(){
	    
	    if( $this->checkAdmin() ){
	        
	        $id = $this->get('id');
	        if( $id ){
	            $this->__ResponseSuccess($this->singleText, $this->AnswerModel->get(array('id' => $id)));
	        }else{
	            $this->__ResponseSuccess($this->pluralText, $this->AnswerModel->getAll());
	        }
	        
	    }
	    
	}

	public function index_post(){
	    if( $this->checkAdmin() ){
    	    $data = $this->post(null, true);
    	    
    	    $this->validateData( $data, $this->requiredFields );
    	    $response = $this->AnswerModel->save($data);
    	    
    	    if($response) {
    	        $this->__ResponseSuccess($this->singleText, $response);
    	    }else{
    	        $this->__ResponseError(Auth::SAVE_ERROR);
    	    }
	    }
	}

	public function index_put(){
	    
	    if( $this->checkAdmin() ){
	        
	        $data = $this->put(null, true);
	        $required['id'] = array( Auth::REQUIRED_TEXT );
	        $this->validateData( $data, $required );
	        
	        $id = $data['id'];
	        unset($data['id']);
	        
	        $response = $this->AnswerModel->update($id, $data);
	        
	        if( $response ){
	            $this->__ResponseSuccess($this->singleText, $response);
	        }
	        else{
	            $this->__ResponseError(Auth::UPDATE_ERROR);
	        }
	        
	    }
	    
	}

	public function index_delete(){
	    
	    if( $this->checkAdmin() ){
	        $id = $this->delete('id');
	        if( $id ){
	            
	            if($this->AnswerModel->delete($id)) {
	                $this->__ResponseSuccess("Success", "success");
	            }else{
	                $this->__ResponseError(Auth::DELETE_ERROR);
	            }
	            
	        }
	        
	    }
	    
	}

}