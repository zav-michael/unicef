<?php

defined('BASEPATH') || exit('No direct script access allowed');

require APPPATH . 'controllers/Auth.php';


class Users extends Auth
{   

    private $requiredFields = array(
        'contact_name' => array(
            Auth::REQUIRED_TEXT => TRUE,
            //Auth::MINLENGHT_TEXT => 10,
           // Auth::MAXLENGHT_TEXT => 20
        ),
        'role' => array(
            Auth::REQUIRED_TEXT => TRUE,
        ),
        'area' => array(
            Auth::REQUIRED_TEXT => TRUE,
        ),
        'phoneNumber' => array(
            Auth::REQUIRED_TEXT => TRUE,

        ),
        'email' => array(
            Auth::REQUIRED_TEXT => TRUE,
            Auth::TYPE_TEXT => "email"
        ),
        'conditions' => array(
            Auth::REQUIRED_TEXT => TRUE,
            Auth::TYPE_TEXT => "int"
        ),
        'Clubs_id' => array(
            Auth::REQUIRED_TEXT => TRUE,
            Auth::TYPE_TEXT => "int"
        ),
        'Profiles_id' => array(
            Auth::REQUIRED_TEXT => TRUE,
            Auth::TYPE_TEXT => "int"
        )
    );

    const EXISTING_EMAIL = "O e-mail já está registrado";
    const EMAIL_NOT_FOUND = "O correio não existe";

    const AUTH_FAILED = "Falha na autenticação";
    const PARTIAL_CONTENT = "Os dados estão incompletos";
    const RESTORE_PASSWORD_EMAIL = "Restaurar senha";
    const ASSIGN_PASSWORD_EMAIL = "Definir senha";
    const RESTORE_SEND_ERROR = "Restaurar senha ERROR";
    const MESSAGE_TEXT = "message";
    const RESTORE_SEND = "Acesse seu e-mail e clique no link enviado para redefinir a sua senha";
    const INVALID_RESTORE_TOKEN = "O token de restauração não é válido";
    const ERROR_EQUAL_PASSWORD = "As senhas não correspondem";
    const PASSWORD_SUCCESS_CHANGED = "A senha foi alterada com sucesso";
    const USER_SUCCESS_CHANGED = "sucesso";
    const ERROR = "error";

    public function __construct(){
        parent::__construct();
    }
    
    public function index_post(){

        $data = $this->input->post(null, true);
    
        $this->validateData( $data, $this->requiredFields );

        if( !$this->Users_Model->checkEmail($data['email'])) {

            //$data['password'] = md5($data['password']);
            $data['created'] = time();
            $saved = $this->Users_Model->save($data);

            if( $saved ) {

                $data['id'] = $saved;
   
                $saved ->restoreurl = $this->config->item('app_url'). '/jogue_limpo/senha?assign='.$this->generateRestoreToken( $saved );

                $email = array(
                    'msg' => $this->load->view('mail/assign', $saved , true),
                    'to' => $saved ->email,
                    'subject' => SELF::ASSIGN_PASSWORD_EMAIL
                ); 

                $this->sendMail($email);

                $this->response( array( 'status' => true, 'User' => $data ), REST_Controller::HTTP_CREATED );

            }
            else {
                $this->__ResponseError( Auth::UNEXPECTED_ERROR );
            }

        } else {
            $this->__ResponseError( self::EXISTING_EMAIL, REST_Controller::HTTP_FOUND );
        }

    }

    public function login_post(){
    	
    	$response = $this->checkSession(TRUE);
    	
    	if( !$response['status'] ){ 		

    		$this->loginWithData();

    	}else{

    		$response = $response['response'];
    		$User = $this->Users_Model->getUserByID( $response->sub );
    		$User->token = $response->token;
    		$this->response( array( 'status' => true, 'User' => $User ) );

    	}

    }

    public function index_get(){

        if( $this->checkAdmin( true ) ){

            $id = $this->get( 'id' );
            if( $id ){
               $this->__getUser( $id );
            }else{
                $this->__getAllUsers();
            }

        }

        $token = $this->checkSession();
        if( $token ){
            $this->__getUser( $token['response']->sub );
        }

    }

    public function restore_post(){

        $data = (object)$this->input->post(null, true);
        $user = $this->Users_Model->getUserByEmail( $data->email );

        if( $user ){
      
            $user->restoreurl = $this->config->item('app_url'). '/home?restore='.$this->generateRestoreToken( $user );
            
            $email = array(
                'msg' => $this->load->view('mail/restore', $user, true),
                'to' => $user->email,
                'subject' => SELF::RESTORE_PASSWORD_EMAIL
            ); 
            
            if($this->sendMail($email)){

                $this->__ResponseSuccess( SELF::MESSAGE_TEXT, SELF::RESTORE_SEND );

            }else{
                $this->__ResponseError( SELF::RESTORE_SEND_ERROR ) ;
            }

        }else{
            $this->__ResponseError( SELF::EMAIL_NOT_FOUND ) ;
        }

    }

    public function verifyrestore_get(){

        $data = (object)$this->input->get(null, true);

        $user = $this->Users_Model->getUserByRestoreToken($data->token);

        if( $user ){

            $this->__ResponseSuccess( 'User', $user );

        }else{

            $this->__ResponseError( SELF::INVALID_RESTORE_TOKEN ) ;

        }

    }

    public function updatetokenpassword_put(){

        $data = (object)$this->put(null, true);
        if( $data->password ){

            unset($data->verifypassword);

            $user = $this->Users_Model->getUserByID($data->user);

            if( $user ){

                unset($data->id);
                unset($data->user);
                $data->restoretoken = "";
                $data->password = md5($data->password);
                $this->Users_Model->update($user->id, (array)$data );

                //Envio de mail de confirmacion cambio contrasena
                $email = array(
                    'msg' => $this->load->view('mail/assign_success', null , true),
                    'to' => $user->email,
                    'subject' => SELF::PASSWORD_SUCCESS_CHANGED
                ); 
                $this->sendMail($email);

                $this->__ResponseSuccess( 'message',  SELF::PASSWORD_SUCCESS_CHANGED );

            }

        }else{
            $this->__ResponseError( SELF::ERROR_EQUAL_PASSWORD ) ;
        }

    }

    public function update_put(){

        $data = (object)$this->put(null, true);
        
        if( isset($data->password) ){
            unset($data->password);
        }

        $user = $this->Users_Model->getUserByID($data->id);

        if( $user ){
            unset($data->id);
            unset($data->token);
            unset($data->exp);
            $this->Users_Model->update($user->id, (array)$data );
            $this->__ResponseSuccess( 'message',  SELF::USER_SUCCESS_CHANGED );

        }else{
            $this->__ResponseError( SELF::ERROR ) ;
        }

    }

    private function generateRestoreToken( $user ){

        $user->restoretoken = sha1($user->email.'~'.microtime());

        if($this->Users_Model->update($user->id, (array)$user )){

            return $user->restoretoken;

        }

        return false;

    }
    
    protected function loginWithData(){

    	$data = $this->input->post(null, true);

        if(isset($data) && isset($data['email']) && isset($data['password'])){

            $email = $data['email'];
            $password = md5($data['password']);

            if($this->Users_Model->checkEmail($email)){

                $UserData = $this->Users_Model->getUserByEmailAndPassword($email, $password);

                if( !is_null($UserData) && count((array)$UserData) > 0 ){

                    $tokenData = array(
                        'iss' => base_url(),
                        'sub' => $UserData->id,
                        'iat' => time(),
                        'exp' => strtotime('+7 days', time())
                    );

                    $output['status'] = true;
                    $output['User'] = $UserData;
                    $output['User']->token = AUTHORIZATION::generateToken($tokenData);
                    $output['User']->exp = $tokenData['exp'];

                    $this->response( $output, REST_Controller::HTTP_OK );
                }else{

                    $this->__ResponseError( self::AUTH_FAILED, REST_Controller::HTTP_NOT_FOUND ) ;

                }
            }
            else{
                $this->__ResponseError( self::EMAIL_NOT_FOUND, REST_Controller::HTTP_NOT_FOUND);
            }

        }else{
            $this->__ResponseError( self::PARTIAL_CONTENT, REST_Controller::HTTP_PARTIAL_CONTENT);
        }

    }

    private function __getUser( $id ) {
        $this->response(array(
            'status' => TRUE, 
            'User' => $this->Users_Model->get( array( 'id' => $id ) )
        ));
    }

    private function __getAllUsers(){

        $this->response(array('status' => true, 'Users' => $this->Users_Model->getAll() ), REST_Controller::HTTP_OK);

    }


}