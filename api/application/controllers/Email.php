<?php

class Email{
    
    // Error Text
    const EMPTY_FIELD = "The field is required";
    const MIN_LENGHT = "The field must be at least ? characters long";
    const MAX_LENGHT = "The field must be at most ? characters long";
    const INVALID_EMAIL = "The E-mail address is invalid";
    const INVALID_TYPE = "The field require a ? value";
    
    // Structure Text
    const REQUIRED_TEXT = "required";
    const TYPE_TEXT = "type";
    const FORMAT_TEXT = "format";
    const MINLENGHT_TEXT = "minlenght";
    const MAXLENGHT_TEXT = "maxlenght";
    const SMTP_PROTOCOL = "smtp";
    
    private $config = array();
    private $errors = array();
    
    function __construct( $config ){
        
        $this->config = (object)$config;

    }

    public function send(){

        if($this->config->protocol === SELF::SMTP_PROTOCOL){
            return $this->SMTP();
        }else{
            return $this->MAIL();
        }

    }

    private function SMTP(){

        include_once(APPPATH."libraries/phpmailer/class.phpmailer.php");
        include_once(APPPATH."libraries/phpmailer/class.smtp.php");
 
        $mail = new PHPMailer();

        $mail->SMTPOptions = array(
		    'ssl' => array(
		        'verify_peer' => false,
		        'verify_peer_name' => false,
		        'allow_self_signed' => true
		    )
		);

        $mail->IsSMTP();
        $mail->CharSet = 'UTF-8';
        $mail->SMTPDebug = 0;
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 465;
        $mail->SMTPSecure = 'ssl';
        $mail->SMTPAuth = true;
        $mail->Username = "david.vacca@zavgroup.com";
        $mail->Password = "doryusenX83";
        $mail->From = 'david.vacca@zavgroup.com';
        $mail->FromName = 'Jogue Limpo';

        $mail->AddAddress($this->config->to, '');

        if( isset($this->config->cc) ){
            $mail->AddCC($this->config->cc, "");
        }

        $mail->Subject = $this->config->subject;
        $mail->MsgHTML($this->config->msg);

        //$this->__DebugErrors( $mail );

        if ( !$mail->send() ) {
            return 0;
        }else{
            return 1;
        }
        
    }

    private function MAIL(){



    }

    private function __DebugErrors($mail){
        $mail->SMTPDebug = 4;
        $mail->send();
		print_r($mail->ErrorInfo); exit();
    }

}