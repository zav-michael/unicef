<?php

defined('BASEPATH') || exit('No direct script access allowed');

require APPPATH . 'controllers/Files.php';


class Commitments extends Files
{
	private $requiredFields = array('description');
	private $singleText = "Commitment";
	private $pluralText = "Commitments";
	
	public function __construct(){
	    parent::__construct();
	    $this->load->Model('CommitmentsModel');
	    $this->load->Model('CommentsModel');
	    $this->load->Model('ClubsModel');
	    $this->load->Model('IndicatorsModel');
	}
	
	public function index_post(){
	    
	    if( $this->checkAdmin() ){
	        
	        $data = $this->post(null, true);
	        
	        $this->validateData( $data, $this->requiredFields );
	        $response = $this->CommitmentsModel->save($data);
	        
	        if($response) {
	            $this->__ResponseSuccess($this->singleText, $response);
	        }else{
	            $this->__ResponseError(Auth::SAVE_ERROR);
	        }
	        
	    }
	}
	
	public function index_put(){
	    
	    if( $this->checkAdmin() ){
	        
	        $data = $this->put(null, true);
	        $required[] = 'id';
			
			$id = $data['id'];
			
	        unset($data['id']);
			
			if(isset($data['comments'])){
				unset($data['comments']);
			}

	        $response = $this->CommitmentsModel->update($id, $data);
	        
	        if( $response ){
	            $this->__ResponseSuccess($this->singleText, $response);
	        }
	        else{
	            $this->__ResponseError(Auth::UPDATE_ERROR);
	        }
	        
	    }
	    
	}
	
	public function index_delete(){
	    
	    if( $this->checkAdmin() ){
	        $id = $this->delete('id');
	        if( $id ){
	            
	            if($this->CommitmentsModel->delete($id)) {
	                $this->__ResponseSuccess("Success", "success");
	            }else{
	                $this->__ResponseError(Auth::DELETE_ERROR);
	            }
	            
	        }
	        
	    }
	    
	}
	
	public function index_get(){
	    $user = (object)$this->checkSession();
	    if( $user->status ){
	        
	        $id = $this->get('id');
	        if( $id ){
	            
	            $res = $this->__appendComments(
                   $this->CommitmentsModel->get( array( 'id' => $id ) )
                );
	            $this->__ResponseSuccess($this->singleText, $res);
	            
	        }else{
	            $res = $this->CommitmentsModel->getAll();
	            foreach ($res as $key => $value ){
	                $value = $this->__appendComments( $value );
	            }
	            $this->__ResponseSuccess($this->pluralText, $res );
	        }
	        
	    }
	    
	}

	public function pagination_get() {

		$user = (object) $this->checkSession();
		if( $user->status ){

			$this->__ResponseSuccess( $this->pluralText, $this->CommitmentsModel->pagination() );

		}
		

	}

	public function all_get(){
		
		$user = (object) $this->checkSession();
		if( $user->status ){
			$this->__ResponseSuccess( $this->pluralText, $this->CommitmentsModel->getAllInfo());
		}

	}

	public function steps_get( ){

		$user = null;
		$data = $this->get();
		

		if(!isset($data['club'])){
			$session = (object)$this->checkSession();
			$user = $session->response;

		}else{

			$session = new stdClass();
			$user = new stdClass();
			$session->status = true;
			$userData = $this->ClubsModel->getUser($data['club']);
			$user->sub = $userData->id;
			
		}
		
		
		if( $session->status ){

			$id = $this->get('id');
			
			if( $id ){
				
				$this->__ResponseSuccess( $this->pluralText, $this->CommitmentsModel->getStep( $id, $user ));

			}
		}
	}

	public function saveProgress_post(){

		$session = (object)$this->checkSession();
		if( $session->status ){
			
			$data = $this->input->post(null, true);
			$data['users_id'] = $session->response->sub;
			$dataBCK = $data;
			unset($dataBCK['value']);	
			$check = $this->CommitmentsModel->checkResponse($dataBCK);
			
			if( !$check ){
				$date = date('Y-m-d');
				$data['created'] = $date;
				$data['modified'] = $date;
				$this->CommitmentsModel->saveResponse( $data );

			}else{
				$date = date('Y-m-d');
				$dArr = array('value' => $data['value'], 'modified' => $date );
				$this->CommitmentsModel->updateResponse( $dataBCK, $dArr );

			}
			
		}

	}


	
	public function save_practice(){
		
		
		}




	public function addImage_post(){

		$session = (object) $this->checkSession();
		if( $session->status ){

			$data = $this->input->post();

			$data['file'] = $this->__UploadB64File($data['file']);
			
			print_r($data);
		}

	}


	public function indicator_get(){

		$session = (object)$this->checkSession();
		
		if( $session->status ){

			$id = $this->get('indicator');
			
			if( $id ){
				
				$this->__ResponseSuccess( $this->pluralText, $this->IndicatorsModel->getIndicator( $id, $session->response ));

			}

		}

	}
	
	private function __appendComments($res){
	    
	    $res->comments = $this->CommentsModel->getAll(array('commitments_id' => $res->id));
	    return $res;
	    
	}

	public function generalProgress_get(){
		
		

		$data = $this->get();
		

		if(!isset($data['club'])){
			$user = (object)$this->checkSession();

		}else{

			$user = new stdClass();
			$user->status = true;
			$userData = $this->ClubsModel->getUser($data['club']);
			$user->response = new stdClass();
			$user->response->sub = $userData->id;
			
		}

		if($user->status){

			$this->__ResponseSuccess('response', $this->CommitmentsModel->getGeneralProgress($user->response));

		}
		

	}
	
}