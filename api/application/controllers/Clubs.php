<?php

defined('BASEPATH') || exit('No direct script access allowed');

require APPPATH . 'controllers/Files.php';

class Clubs extends Files
{
	private $requiredFields = array(
	    'name' => array(
	        Auth::REQUIRED_TEXT => TRUE,
	        Auth::MAXLENGHT_TEXT => 20
	    ),
	    'address' => array(
	        Auth::REQUIRED_TEXT => TRUE,
	    ),
	    'city' => array(
	        Auth::REQUIRED_TEXT => TRUE,
	    ),
	    'uf' => array(
	        Auth::REQUIRED_TEXT => TRUE,
	    ),
	    'cnpj' => array(
	        Auth::REQUIRED_TEXT => TRUE,
	    ),
	    'employeesQuantity' => array(
	        Auth::REQUIRED_TEXT => TRUE,
	    ),
	    'website' => array(
	        Auth::REQUIRED_TEXT => FALSE,
	        Auth::FORMAT_TEXT => "web"
	    ),
	   
	);
	private $singleText = "Club";
	private $pluralText = "Clubs";

	public function __construct(){
		parent::__construct();
		$this->load->Model('ClubsModel');
		$this->item = 30;
	}

	public function index_post(){

        $data = $this->input->post();

		if(isset($data['logo'])){
			$data['logo'] = $this->__UploadB64File($data['logo']);
		}
		
		$this->validateData( $data, $this->requiredFields );

		if(!$this->ClubsModel->checkCEP($data["employeesQuantity"])){
			$response = $this->ClubsModel->save( $data );
		}else{
			$this->__ResponseError(array("empQuantityCl" => "CEP já existe"));
		}
		
		
        if($response) {
            $this->__ResponseSuccess($this->singleText, $response);
        }else{
            $this->__ResponseError(Auth::SAVE_ERROR);
        }
	}
	
	public function index_put(){
	        
        $data = (array)$this->put(null, true);
		$required = array(
			'id' => array(
				Auth::REQUIRED_TEXT => TRUE
			)
		);
		
		$this->validateData( $data, $required );
        
        $id = $data['id'];
		unset($data['id']);
		unset($data['empQuantity']);

		if(isset($data['logo'])){
		 	$data['logo'] = $this->__UploadB64File($data['logo']);
		}

		$response = $this->ClubsModel->update($id, $data);
        
        if( $response ){
            $this->__ResponseSuccess($this->singleText, $response);
        }
        else{
            $this->__ResponseError(Auth::UPDATE_ERROR);
        }
	    
	}
	
	public function index_delete(){
	    
	    if( $this->checkAdmin() ){
	        $id = $this->delete('id');
	        if( $id ){
	            
	            if($this->ClubsModel->delete($id)) {
	                $this->__ResponseSuccess("Success", "success");
	            }else{
	                $this->__ResponseError(Auth::DELETE_ERROR);
	            }
	            
	        }
	        
	    }
	    
	}
	
	public function index_get(){
	    $user = (object)$this->checkSession();
	    if( $user->status ){
	        
	        $id = $this->get('id');
	        if( $id ){
				$club = $this->ClubsModel->get(array('id' => $id));
				$club->logo = base_url($club->logo);
	            $this->__ResponseSuccess($this->singleText, $club);
	        }else{
	            $this->__ResponseSuccess($this->pluralText, $this->ClubsModel->getAll());
	        }
	        
	    }
	    
	}

	public function my_get(){
	    $user = (object)$this->checkSession();
	    if( $user->status ){

	        $club = $this->ClubsModel->getByUserId( $user->response->sub );
			$club->logo = base_url($club->logo);
		
	        if( $club->id ){
	            $this->__ResponseSuccess($this->singleText, $club);
	        }
	        
	    }
	    
	}

	public function __CompleteData($data, $uploadInfo){

		 $data['logo'] = 'uploads/'.$uploadInfo['file_name'];

		 return $data;

	}


	public function filters_get()
	{


		if( $this->checkAdmin() ){
		
			$commitments = $this->ClubsModel->getData('commitments');	
			$quantityClubs = $this->ClubsModel->getQuantity();		
			$pagination = $this->createPagination($quantityClubs);
			$this->__ResponseSuccess('filters', array('compromisso' => $commitments, 'pagination' => $pagination, 'items' => $this->item , 'total' => $quantityClubs));

		}

	}


	public function createPagination($quantityClubs){
		$pagination = array();
		for($i = 0; $i < ($quantityClubs / $this->item); $i++){
			$pagination[] = array('id' => ($i + 1), 'name' => $i, 'title' => ($i + 1), 'steps_id' => ($i + 1));
		}
		return $pagination;
	}


	public function export_get() {
		
		$page = 1;
		$data = $this->input->get();
		if(isset($data['page'])){
			$page = $data['page'];
			unset($data['page']);
		}
		$query =  $this->ClubsModel->getDataFilter($data, $page, $this->item, false);
		$titles = array('id', 'name', 'address', 'city', 'uf', 'cnpj', 'employeesQuantity', 'website', 'logo', 'program', 'created');
		$content = $query['data'];
		$name = md5('archivo'.date("Y-m-d H:i:s"));
		$this->excel->filename = $name;
		$this->excel->make_from_array($titles, $content);
	}


	public function filters_post(){
		
		$data = $this->input->post();
		$page = 1;
		if(isset($data['page'])){
			$page = $data['page'];
			unset($data['page']);
		}
		foreach($data as $key => $item){
			if($data[$key] == ""){ 
				unset($data[$key]);
			}
		}		
		$content = array();
		$query =  $this->ClubsModel->getDataFilter($data, $page, $this->item);
		$content = $query['data'];
		$response['items'] = $content;
		$cantFilt = $query['count'];
		$pagination = $this->createPagination($cantFilt);
		$response['pagination'] = $pagination;
		$this->__ResponseSuccess('filters', $response);	
	}





}