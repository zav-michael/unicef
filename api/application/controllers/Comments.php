<?php

defined('BASEPATH') || exit('No direct script access allowed');

require APPPATH . 'controllers/Auth.php';


class Comments extends Auth
{
    private $requiredFields = array(
	    'comment' => array(
	        Auth::REQUIRED_TEXT => TRUE
	    ),
	    'indicator_id' => array(
	        Auth::REQUIRED_TEXT => TRUE
	    )
	);
    private $singleText = "Comment";
    private $pluralText = "Comments";
    
    public function __construct(){
        parent::__construct();
        $this->load->Model('CommentsModel');
        $this->load->Model('ClubsModel');
    }
    
    public function index_post(){
        $user = (object)$this->checkSession();
        if( $user->status ){
            
            $data = $this->input->post(null, true);
            
            $data['users_id'] = $user->response->sub;
            $club = $this->ClubsModel->getByUserId($user->response->sub);
            $data['clubs_id'] = $club->id;
            $data['created'] = date('Y-m-d');
            $this->validateData( $data, $this->requiredFields );
            $response = $this->CommentsModel->save($data);
            
            if($response) {
                $this->__ResponseSuccess($this->pluralText, $response);
            }else{
                $this->__ResponseError(Auth::SAVE_ERROR);
            }
            
        }
    }

    
    public function index_put(){
        
        if( $this->checkAdmin() ){
            
            $data = $this->put(null, true);
            $required[] = 'id';
            $this->validateData( $data, $required );
            
            $id = $data['id'];
            unset($data['id']);
            
            $response = $this->CommentsModel->update($id, $data);
            
            if( $response ){
                $this->__ResponseSuccess($this->singleText, $response);
            }
            else{
                $this->__ResponseError(Auth::UPDATE_ERROR);
            }
            
        }
        
    }
    
    public function index_delete(){
        
        if( $this->checkAdmin() ){
            $id = $this->delete('id');
            if( $id ){
                
                if($this->CommentsModel->delete($id)) {
                    $this->__ResponseSuccess("Success", "success");
                }else{
                    $this->__ResponseError(Auth::DELETE_ERROR);
                }
                
            }
            
        }
        
    }
    
    public function index_get(){
        $user = (object)$this->checkSession();
        if( $user->status ){
            $club = $this->ClubsModel->getByUserId($user->response->sub);

            $data = array(
                'C.clubs_id' => $club->id,
                'C.users_id' => $user->response->sub,
                'C.indicator_id' => $this->get('indicator')
            );

            if( !$data ){
                $this->__ResponseSuccess($this->singleText, $this->CommentsModel->get());
            }else{
                $this->__ResponseSuccess($this->pluralText, $this->CommentsModel->getAllComments($data));
            }
            
        }
        
    }
    
}