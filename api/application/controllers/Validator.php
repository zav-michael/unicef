<?php

class Validator{
    
    // Error Text
    const EMPTY_FIELD = "The field is required";
    const MIN_LENGHT = "The field must be at least ? characters long";
    const MAX_LENGHT = "The field must be at most ? characters long";
    const INVALID_EMAIL = "The E-mail address is invalid";
    const INVALID_TYPE = "The field require a ? value";
    
    // Structure Text
    const REQUIRED_TEXT = "required";
    const TYPE_TEXT = "type";
    const FORMAT_TEXT = "format";
    const MINLENGHT_TEXT = "minlenght";
    const MAXLENGHT_TEXT = "maxlenght";
    
    private $config = array();
    private $errors = array();
    
    function __construct( $config ){
        
        $this->config = $config;
        
    }
    
    public function validate( $data ){

        foreach ($this->config as $key => $props){
            
            $value = isset( $data[$key] )? $data[$key] : NULL; 
            
            if( array_key_exists( self::REQUIRED_TEXT, $props ) ){
                
                $this->__Empty( $value, $key );
                
            }
            
            if( array_key_exists( self::TYPE_TEXT, $props ) ){

                $this->__dataType( $value, $key, $props[self::TYPE_TEXT]);
                
            }

            if( array_key_exists( self::MINLENGHT_TEXT, $props ) || array_key_exists( self::MAXLENGHT_TEXT, $props ) ){
                
                $min = isset( $props[self::MINLENGHT_TEXT] )? $props[self::MINLENGHT_TEXT] : NULL;
                $max = isset( $props[self::MAXLENGHT_TEXT] )? $props[self::MAXLENGHT_TEXT] : NULL;
                $this->__Lenght( $value, $key, $min, $max);
                
            }
            
        }
        
    }
    
    private function __Empty( $value, $fieldName ){
        
        if( empty( $value ) && $value != 0){
            
           $this->__AddError($fieldName, self::EMPTY_FIELD);
            
        }
        
    }
    
    private function __Lenght( $value, $fieldName, $min = NULL, $max = NULL ){
        
        if( strlen( $value ) > $max ){
            
            $this->__AddError($fieldName, str_replace('?', $max, self::MAX_LENGHT));
            
        }
        
        if( $min && strlen($value) < $min ){
            
            $this->__AddError($fieldName, str_replace('?', $min, self::MIN_LENGHT));
            
        }
        
    }
    
    private function __Email( $value, $fieldName ){
        
        if( !filter_var($value, FILTER_VALIDATE_EMAIL) ){
            
            return false;
            
        }

        return true;
        
    }
    
    private function __dataType($value, $fieldName, $fieldType) {
        
        if( $fieldType == "int" && !is_numeric( $value ) ){
                          
            $this->__AddError($fieldName, str_replace('?', $fieldType, self::INVALID_TYPE));
            
        }
        
        if( $fieldType == "email" && !$this->__Email( $value, $fieldName ) ){
                          
            $this->__AddError($fieldName, str_replace('?', $fieldType, self::INVALID_TYPE));
            
        }
        
    }
    
    private function __AddError($fieldName, $error){
        
        $this->errors[$fieldName][] = $error;
        
    }
    
    public function __getErrors(){
        
        return $this->errors;
        
    }
    
}