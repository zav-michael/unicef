<?php

defined('BASEPATH') || exit('No direct script access allowed');

require APPPATH . 'controllers/Auth.php';


class ActionTypes extends Auth
{
	private $requiredFields = array(
        'name' => array(
           Auth::REQUIRED_TEXT => TRUE
        )
	);

	private $singleText = "Action_type";
	private $pluralText = "Action_types";
	
	public function __construct(){
	    parent::__construct();
	    $this->load->Model('ActionTypesModel');
	}
	
	public function index_post(){
	    if( $this->checkAdmin() ){
    	    $data = $this->post(null, true);
    	    
    	    $this->validateData( $data, $this->requiredFields );
    	    $response = $this->ActionTypesModel->save($data);
    	    
    	    if($response) {
    	        $this->__ResponseSuccess($this->singleText, $response);
    	    }else{
    	        $this->__ResponseError(Auth::SAVE_ERROR);
    	    }
	    }
	    
	}
	
	public function index_put(){
	    
	    if( $this->checkAdmin() ){
	        
	        $data = $this->put(null, true);
	        $required['id'] = array( Auth::REQUIRED_TEXT );
	        $this->validateData( $data, $required );
	        
	        $id = $data['id'];
	        unset($data['id']);
	        
	        $response = $this->ActionTypesModel->update($id, $data);
	        
	        if( $response ){
	            $this->__ResponseSuccess($this->singleText, $response);
	        }
	        else{
	            $this->__ResponseError(Auth::UPDATE_ERROR);
	        }
	        
	    }
	    
	}
	
	public function index_delete(){
	    
	    if( $this->checkAdmin() ){
	        $id = $this->delete('id');
	        if( $id ){
	            
	            if($this->ActionTypesModel->delete($id)) {
	                $this->__ResponseSuccess("Success", "success");
	            }else{
	                $this->__ResponseError(Auth::DELETE_ERROR);
	            }
	            
	        }
	        
	    }
	    
	}
	
	public function index_get(){
	    
	  //  if( $this->checkAdmin() ){
	        
	        $id = $this->get('id');
	        if( $id ){
	            $this->__ResponseSuccess($this->singleText, $this->ActionTypesModel->get(array('id' => $id)));
	        }else{
	            $this->__ResponseSuccess($this->pluralText, $this->ActionTypesModel->getAll());
	        }
	        
	  //  }
	    
	}
}