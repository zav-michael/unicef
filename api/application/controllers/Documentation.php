<?php 
/**
@api {post} /clubs 1.Agregar
@apiVersion 0.1.2
@apiName Clubs
@apiGroup 1.Clubs

@apiParam {String} name Nombre del Club.
@apiParam {String} Address Dirección.
@apiParam {String} City Ciudad.
@apiParam {Number} uf Documento de identificación
@apiParam {Number} cnpj Registro nacional de personas jurídicas.
@apiParam {number} employeesQuantity Cantidad de empleados.
@apiParam {String} website Sitio Web.
*
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "Club": {
             "id": "3",
             "name": "Club 2",
             "address": "Brasilhao",
             "city": "Sao paulo",
             "uf": "12345679",
             "cnpj": "456445455",
             "employeesQuantity": "10",
             "website": "http://club2.com"
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripciÃ³n del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/


/**
@api {put} /clubs 2.Actualizar
@apiVersion 0.1.2
@apiName Clubs Update
@apiGroup 1.Clubs

@apiParam {Number} id ID del Club.
@apiParam {Any} Field Cualquier campo o campos que se requieran actualizar.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "Club": {
             "id": "3",
             "name": "Club 2",
             "address": "AV Sao Paulo",
             "city": "Sao paulo",
             "uf": "12345679",
             "cnpj": "456445455",
             "employeesQuantity": "10",
             "website": "http://club2.com"
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/



/**
@api {delete} /clubs 3.Eliminar
@apiVersion 0.1.2
@apiName Clubs Delete
@apiGroup 1.Clubs

@apiParam {Number} id ID del Club.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "Success": "Success"
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/

/**
@api {get} /clubs/:id? 4.Obtener
@apiVersion 0.1.2
@apiName Clubs GET
@apiGroup 1.Clubs

@apiParam {Number} id ID del Club.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "Club": {
             "id": "3",
             "name": "Club 2",
             "address": "AV Sao Paulo",
             "city": "Sao paulo",
             "uf": "12345679",
             "cnpj": "456445455",
             "employeesQuantity": "10",
             "website": "http://club2.com"
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/


/*********************USUARIOS ***********************/

/**
@api {post} /users 1.Agregar
@apiVersion 0.1.2
@apiName Usuarios
@apiGroup 2.Usuarios

@apiParam {String} role Rol del Usuario.
@apiParam {String} area Área.
@apiParam {String} phoneNumber Número Teléfonico.
@apiParam {String} mobileNumber Número Celular.
@apiParam {Number} conditions Aceptar términos y condiciones.
@apiParam {string} email Correo electrónico.
@apiParam {string} password Contraseña.
@apiParam {Number} Profiles_id Id del perfil.

@apiSuccessExample Success-Response:
HTTP/1.1 200 OK
{
    "status": true,
    "User": {
        "role": "President",
        "area": "My Area",
        "phoneNumber": "4567896",
        "mobileNumber": "3215698523",
        "email": "vicepresident@whitehouse.com",
        "conditions": "1",
        "Clubs_id": "2",
        "Profiles_id": "1",
        "created": 1501858962
    }
}
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/


/**
@api {put} /clubs 2.Actualizar
@apiVersion 0.1.2
@apiName Usuarios Update
@apiGroup 2.Usuarios

@apiParam {Number} id ID del Usuario.
@apiParam {Any} Field Cualquier campo o campos que se requieran actualizar.

*/



/**
@api {delete} /users 3.Eliminar
@apiVersion 0.1.2
@apiName Usuarios Delete
@apiGroup 2.Usuarios

@apiParam {Number} id ID del Usuario.

@apiSuccessExample Success-Response:
HTTP/1.1 200 OK
{
    "status": true,
    "Success": "Success"
}
*/

/**
@api {get} /user/:id? 4.Obtener
@apiVersion 0.1.2
@apiName Usuarios GET
@apiGroup 2.Usuarios

@apiParam {Number} id ID del Club.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "Club": {
             "id": "3",
             "name": "Club 2",
             "address": "AV Sao Paulo",
             "city": "Sao paulo",
             "uf": "12345679",
             "cnpj": "456445455",
             "employeesQuantity": "10",
             "website": "http://club2.com"
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}

@api {post} /users/login Iniciar Sesion
@apiVersion 0.1.0
@apiName login
@apiGroup 2.Usuarios
@apiHeader (User Headers) {String} authorization Authorization value.
@apiHeaderExample {json} Success Response:
    HTTP/1.1 200 OK
     {
       "status": true,
       "User": {
         "id": "1",
         "role": "CEO",
         "area": "BR",
         "phoneNumber": "4569875",
         "mobileNumber": null,
         "email": "michael.sanchez@zavgroup.com",
         "password": "",
         "conditions": "1",
         "created": "0",
         "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJlbWFpbCI6Im1pY2hhZWwuc2FuY2hlekB6YXZncm91cC5jb20iLCJleHAiOjE0OTY0MTE3OTh9.GuOKzmUTjtHICOpc2Rm0hkh4THQdM5Ihjsq4YKNcrRk",
         "exp": 1496411798
       }
     }
 @apiHeaderExample {json} Error Response:
 {
   "status": false,
   "error": {
     "message": "The data is incomplete"
   }
 }
@apiParam {string} email Correo electrónico.
@apiParam {string} password ContraseÃ±a.
*
@apiSuccess {Bool} status  true.
@apiSuccess {Json} User objeto con toda la informaciÃ³n del Usuario registrado.
*
@apiSuccessExample Success Response:
    HTTP/1.1 200 OK
    {
       "status": true,
       "User": {
         "id": "8",
         "role": "CEO",
         "area": "BR",
         "phoneNumber": "356655566",
         "mobileNumber": null,
         "email": "michael.sanchez1@zavgroup.com",
         "conditions": "1",
         "created": "0",
         "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjgiLCJlbWFpbCI6Im1pY2hhZWwuc2FuY2hlejFAemF2Z3JvdXAuY29tIiwiZXhwIjoxNDk2NzcwNDY2fQ.viRE_0otk6mEi1ctMknXa3-RmiwENoTVlFFmAhoAm80",
         "exp": 1496770466
       }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripciÃ³n del error.
*
@apiErrorExample Error Response:
    HTTP/1.1 400 Not Found
     {
       "status": false,
       "error": {
         "message": "The mail already exist"
       }
    }

*/

/******************** Diagnostico ***********************/

/**
@api {post} /diagnostic 1.Agregar
@apiVersion 0.1.2
@apiName diagnostic
@apiGroup 3.Diagnostico

@apiParam {number} Clubs_id Nombre del Club.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "Diagnostic": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripciÃ³n del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/


/**
@api {put} /diagnostic 2.Actualizar
@apiVersion 0.1.2
@apiName diagnostic Update
@apiGroup 3.Diagnostico

@apiParam {Number} id ID del diagnostico.
@apiParam {Any} Field Cualquier campo o campos que se requieran actualizar.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "Diagnostic": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/



/**
@api {delete} /diagnostic 3.Eliminar
@apiVersion 0.1.2
@apiName diagnostic Delete
@apiGroup 3.Diagnostico

@apiParam {Number} id ID del Diagnóstico.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "Success": "Success"
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/

/**
@api {get} /diagnostic/:id? 4.Obtener
@apiVersion 0.1.2
@apiName diagnostic GET
@apiGroup 3.Diagnostico

@apiParam {Number} id ID del Diagnóstico.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "Diagnostic": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/


/******************** Multimedia ***********************/

/**
@api {post} /multimedia 1.Agregar
@apiVersion 0.1.2
@apiName multimedia
@apiGroup 4.Multimedia

@apiParam {File} file Archivo.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "multimedia": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripciÃ³n del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/


/**
@api {post} /multimedia/update 2.Actualizar
@apiVersion 0.1.2
@apiName multimedia Update
@apiGroup 4.Multimedia

@apiParam {Number} id ID del registro multimedia.
@apiParam {Any} Field Cualquier campo o campos que se requieran actualizar.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "multimedia": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/



/**
@api {delete} /multimedia 3.Eliminar
@apiVersion 0.1.2
@apiName multimedia Delete
@apiGroup 4.Multimedia

@apiParam {Number} id ID del Diagnóstico.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "Success": "Success"
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/

/**
@api {get} /multimedia/:id? 4.Obtener
@apiVersion 0.1.2
@apiName multimedia GET
@apiGroup 4.Multimedia

@apiParam {Number} id ID del Diagnóstico.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "multimedia": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/




/******************** Answers ***********************/

/**
@api {post} /answers 1.Agregar
@apiVersion 0.1.2
@apiName answers
@apiGroup 5.Respuestas

@apiParam {String} Description Respuesta.
@apiParam {Integer} questions_id ID de la pregunta.
@apiParam {Integer} colors_id Id del color.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "answers": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripciÃ³n del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/


/**
@api {put} /answers 2.Actualizar
@apiVersion 0.1.2
@apiName answers Update
@apiGroup 5.Respuestas

@apiParam {Number} id ID del registro answers.
@apiParam {Any} Field Cualquier campo o campos que se requieran actualizar.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "answers": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/



/**
@api {delete} /answers 3.Eliminar
@apiVersion 0.1.2
@apiName answers Delete
@apiGroup 5.Respuestas

@apiParam {Number} id ID del Diagnóstico.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "Success": "Success"
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/

/**
@api {get} /answers/:id? 4.Obtener
@apiVersion 0.1.2
@apiName answers GET
@apiGroup 5.Respuestas

@apiParam {Number} id ID del Diagnóstico.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "answers": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/




/******************** AnswersTypes ***********************/

/**
@api {post} /answerstypes 1.Agregar
@apiVersion 0.1.2
@apiName answersTypes
@apiGroup 6.Tipos de Respuesta

@apiParam {name} nombre Nombre del archivo.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "answersTypes": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripciÃ³n del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/


/**
@api {put} /answerstypes 2.Actualizar
@apiVersion 0.1.2
@apiName answersTypes Update
@apiGroup 6.Tipos de Respuesta

@apiParam {Number} id ID del registro answersTypes.
@apiParam {Any} Field Cualquier campo o campos que se requieran actualizar.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "answersTypes": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/



/**
@api {delete} /answersTypes 3.Eliminar
@apiVersion 0.1.2
@apiName answersTypes Delete
@apiGroup 6.Tipos de Respuesta

@apiParam {Number} id ID del Diagnóstico.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "Success": "Success"
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/

/**
@api {get} /answersTypes/:id? 4.Obtener
@apiVersion 0.1.2
@apiName answersTypes GET
@apiGroup 6.Tipos de Respuesta

@apiParam {Number} id ID del Diagnóstico.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "answersTypes": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/



/******************** Comments ***********************/

/**
@api {post} /comments 1.Agregar
@apiVersion 0.1.2
@apiName comments
@apiGroup 7.Comentarios

@apiParam {String} Comment Comentario.
@apiParam {Integer} commitments_id ID del compromiso.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "comments": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripciÃ³n del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/


/**
@api {put} /comments 2.Actualizar
@apiVersion 0.1.2
@apiName comments Update
@apiGroup 7.Comentarios

@apiParam {Number} id ID del Comentario.
@apiParam {Any} Field Cualquier campo o campos que se requieran actualizar.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "comments": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/



/**
@api {delete} /comments 3.Eliminar
@apiVersion 0.1.2
@apiName comments Delete
@apiGroup 7.Comentarios

@apiParam {Number} id ID del comentario.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "Success": "Success"
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/

/**
@api {get} /comments/:id? 4.Obtener
@apiVersion 0.1.2
@apiName comments GET
@apiGroup 7.Comentarios

@apiParam {Number} id ID del comentario.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "comments": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/



/******************** Commitments ***********************/

/**
@api {post} /commitments 1.Agregar
@apiVersion 0.1.2
@apiName commitments
@apiGroup 7.Comentarios

@apiParam {String} description Descripción del compromiso.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "commitments": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripciÃ³n del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/


/**
@api {put} /commitments 2.Actualizar
@apiVersion 0.1.2
@apiName commitments Update
@apiGroup 7.Comentarios

@apiParam {Number} id ID del Compromiso.
@apiParam {Any} Field Cualquier campo o campos que se requieran actualizar.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "commitments": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/



/**
@api {delete} /commitments 3.Eliminar
@apiVersion 0.1.2
@apiName commitments Delete
@apiGroup 7.Comentarios

@apiParam {Number} id ID del compromiso.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "Success": "Success"
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/

/**
@api {get} /commitments/:id? 4.Obtener
@apiVersion 0.1.2
@apiName commitments GET
@apiGroup 7.Comentarios

@apiParam {Number} id ID del compromiso.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "commitments": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/





/******************** Diagnostics ***********************/

/**
@api {post} /diagnostics 1.Agregar
@apiVersion 0.1.2
@apiName diagnostics
@apiGroup 8.Diagnostico

@apiParam {String} description Descripción del compromiso.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "diagnostics": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripciÃ³n del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/


/**
@api {put} /diagnostics 2.Actualizar
@apiVersion 0.1.2
@apiName diagnostics Update
@apiGroup 8.Diagnostico

@apiParam {Number} id ID del diagnostico.
@apiParam {Any} Field Cualquier campo o campos que se requieran actualizar.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "diagnostics": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/



/**
@api {delete} /diagnostics 3.Eliminar
@apiVersion 0.1.2
@apiName diagnostics Delete
@apiGroup 8.Diagnostico

@apiParam {Number} id ID del diagnostico.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "Success": "Success"
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/

/**
@api {get} /diagnostics/:id? 4.Obtener
@apiVersion 0.1.2
@apiName diagnostics GET
@apiGroup 8.Diagnostico

@apiParam {Number} id ID del diagnostico.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "diagnostics": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/


/******************** Indicators ***********************/

/**
@api {post} /indicators 1.Agregar
@apiVersion 0.1.2
@apiName indicators
@apiGroup 9.Indicadores

@apiParam {String} action Accion.
@apiParam {String} goal Meta.
@apiParam {String} goal_one_year Meta a un año.
@apiParam {String} indicator Indicador.
@apiParam {String} formula Formula.
@apiParam {String} means_of_verification Método de verificación.
@apiParam {String} source_of_information Fuente de información.
@apiParam {String} period Periodo.
@apiParam {Integer} action_types_id ID del tipo de acción.
@apiParam {Integer} Commitments_id ID del compromiso.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "indicators": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/


/**
@api {put} /indicators 2.Actualizar
@apiVersion 0.1.2
@apiName indicators Update
@apiGroup 9.Indicadores

@apiParam {Number} id ID del indicador.
@apiParam {Any} Field Cualquier campo o campos que se requieran actualizar.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "indicators": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/



/**
@api {delete} /indicators 3.Eliminar
@apiVersion 0.1.2
@apiName indicators Delete
@apiGroup 9.Indicadores

@apiParam {Number} id ID del indicador.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "Success": "Success"
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/

/**
@api {get} /indicators/:id? 4.Obtener
@apiVersion 0.1.2
@apiName indicators GET
@apiGroup 9.Indicadores

@apiParam {Number} id ID del indicador.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "indicators": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/



/******************** Multimedia ***********************/

/**
@api {post} /multimedia 1.Agregar
@apiVersion 0.1.2
@apiName multimedia
@apiGroup 10.Multimedia

@apiParam {File} file Archivo.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "multimedia": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/


/**
@api {post} /multimedia/update 2.Actualizar
@apiVersion 0.1.2
@apiName multimedia Update
@apiGroup 10.Multimedia

@apiParam {Number} id ID del archivo multimedia.
@apiParam {File} file archivo.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "multimedia": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/



/**
@api {delete} /multimedia 3.Eliminar
@apiVersion 0.1.2
@apiName multimedia Delete
@apiGroup 10.Multimedia

@apiParam {Number} id ID del archivo multimedia.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "Success": "Success"
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/

/**
@api {get} /multimedia/:id? 4.Obtener
@apiVersion 0.1.2
@apiName multimedia GET
@apiGroup 10.Multimedia

@apiParam {Number} id ID del archivo multimedia.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
     {
         "status": true,
         "multimedia": {
             ...
         }
     }
@apierror (500 Internal Server Error) InternalServerError The server encountered an internal error
@apiError {Bool} status False Ha Ocurrido un Error.
@apiError {JSON} error Objeto Json que contiene el la descripción del error.

@apiErrorExample Error-Response:
HTTP/1.1 400 Not Found
{
  "status": false,
  "error": {
    "message": "message"
  }
}
*/