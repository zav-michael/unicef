<?php

/// DEPRECATED

defined('BASEPATH') || exit('No direct script access allowed');

require APPPATH . 'controllers/Auth.php';


class Sections extends Auth
{
	private $requiredFields = array(
		'name' => array(Auth::REQUIRED_TEXT => TRUE),
		'steps_id' => array(Auth::REQUIRED_TEXT => TRUE, Auth::TYPE_TEXT => 'INT')
	);
	private $singleText = "Section";
	private $pluralText = "Sections";
	
	public function __construct(){
	    parent::__construct();
	    $this->load->Model('SectionsModel');
	}
	
	public function index_post(){
	    
	    if( $this->checkAdmin() ){
	        
	        $data = $this->post(null, true);
	        
	        $this->validateData( $data, $this->requiredFields );
	        $response = $this->SectionsModel->save($data);
	        
	        if($response) {
	            $this->__ResponseSuccess($this->singleText, $response);
	        }else{
	            $this->__ResponseError(Auth::SAVE_ERROR);
	        }
	        
	    }
	}


	
	public function index_put(){
	    
	    if( $this->checkAdmin() ){
	        
			$data = $this->put(null, true);
			
	        
	        $id = $data['id'];
	        unset($data['id']);
	        
	        $response = $this->SectionsModel->update($id, $data);
	        
	        if( $response ){
	            $this->__ResponseSuccess($this->singleText, $response);
	        }
	        else{
	            $this->__ResponseError(Auth::UPDATE_ERROR);
	        }
	        
	    }
	    
	}
	
	public function index_delete(){
	    
	    if( $this->checkAdmin() ){
	        $id = $this->delete('id');
	        if( $id ){
	            
	            if($this->SectionsModel->delete($id)) {
	                $this->__ResponseSuccess("Success", "success");
	            }else{
	                $this->__ResponseError(Auth::DELETE_ERROR);
	            }
	            
	        }
	        
	    }
	    
	}

	public function all_get(){

		if( $this->checkAdmin() ){

			$this->__ResponseSuccess($this->pluralText, $this->SectionsModel->getAll());

		}

	}
	
	public function index_get(){
	    
	    if( $this->checkAdmin() ){
	        
	        $id = $this->get('id');
	        if( $id ){
	            $this->__ResponseSuccess($this->singleText, $this->SectionsModel->get(array('id' => $id)));
	        }else{
	            $this->__ResponseSuccess($this->pluralText, $this->SectionsModel->getAll());
	        }
	        
	    }
	    
	}
}