<?php

/// DEPRECATED

defined('BASEPATH') || exit('No direct script access allowed');

require APPPATH . 'controllers/Auth.php';


class AnswersTypes extends Auth
{
	private $requiredFields = array('type'=>array(Auth::REQUIRED_TEXT => TRUE));
	private $singleText = "State";
	private $pluralText = "States";
	
	public function __construct(){
	    parent::__construct();
	    $this->load->Model('AnswersTypesModel');
	}
	
	public function index_post(){
	    
	    if( $this->checkAdmin() ){
	        
	        $data = $this->post(null, true);
	        
	        $this->validateData( $data, $this->requiredFields );
	        $response = $this->AnswersTypesModel->save($data);
	        
	        if($response) {
	            $this->__ResponseSuccess($this->singleText, $response);
	        }else{
	            $this->__ResponseError(Auth::SAVE_ERROR);
	        }
	        
	    }
	}
	
	public function index_put(){
	    
	    if( $this->checkAdmin() ){
	        
	        $data = $this->put(null, true);
	        $required[] = 'id';
	        $this->validateData( $data, $required );
	        
	        $id = $data['id'];
	        unset($data['id']);
	        
	        $response = $this->AnswersTypesModel->update($id, $data);
	        
	        if( $response ){
	            $this->__ResponseSuccess($this->singleText, $response);
	        }
	        else{
	            $this->__ResponseError(Auth::UPDATE_ERROR);
	        }
	        
	    }
	    
	}
	
	public function index_delete(){
	    
	    if( $this->checkAdmin() ){
	        $id = $this->delete('id');
	        if( $id ){
	            
	            if($this->AnswersTypesModel->delete($id)) {
	                $this->__ResponseSuccess("Success", "success");
	            }else{
	                $this->__ResponseError(Auth::DELETE_ERROR);
	            }
	            
	        }
	        
	    }
	    
	}
	
	public function index_get(){
	    
	    if( $this->checkAdmin() ){
	        
	        $id = $this->get('id');
	        if( $id ){
	            $this->__ResponseSuccess($this->singleText, $this->AnswersTypesModel->get(array('id' => $id)));
	        }else{
	            $this->__ResponseSuccess($this->pluralText, $this->AnswersTypesModel->getAll());
	        }
	        
	    }
	    
	}
}