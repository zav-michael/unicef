<?php

defined('BASEPATH') || exit('No direct script access allowed');

require APPPATH . 'controllers/Auth.php';


class Contact extends Auth
{
	private $requiredFields = array('name', 'email', 'address', 'address_2');
	private $singleText = "Contact";
	private $pluralText = "Contacts";

	public function __construct(){
		parent::__construct();
		$this->load->Model('ContactModel');
	}

	public function index_post(){
	    
	    if( $this->checkAdmin() ){
	        
	        $data = $this->post(null, true);
	        
	        $this->validateData( $data, $this->requiredFields );
	        $response = $this->ContactModel->save($data);
	        
	        if($response) {
	            $this->__ResponseSuccess($this->singleText, $response);
	        }else{
	            $this->__ResponseError(Auth::SAVE_ERROR);
	        }
	        
	    }
	}

	public function index_put(){
	    
	    if( $this->checkAdmin() ){
	        
	        $data = $this->put(null, true);
	        $required[] = 'id';
	        $this->validateData( $data, $required );
	        
	        $id = $data['id'];
	        unset($data['id']);
	        
	        $response = $this->ContactModel->update($id, $data);
	        
	        if( $response ){
	            $this->__ResponseSuccess($this->singleText, $response);
	        }
	        else{
	            $this->__ResponseError(Auth::UPDATE_ERROR);
	        }
	        
	    }
	    
	}
	
	public function index_delete(){
	    
	    if( $this->checkAdmin() ){
	        $id = $this->delete('id');
	        if( $id ){
	            
	            if($this->ContactModel->delete($id)) {
	                $this->__ResponseSuccess("Success", "success");
	            }else{
	                $this->__ResponseError(Auth::DELETE_ERROR);
	            }
	            
	        }
	        
	    }
	    
	}
	
	public function index_get(){
	    
	    // Everybody can view
        $id = $this->get('id');
        if( $id ){
            $this->__ResponseSuccess($this->singleText, $this->ContactModel->get(array('id' => $id)));
        }else{
            $this->__ResponseSuccess($this->pluralText, $this->ContactModel->getAll());
        }
	    
	}

}