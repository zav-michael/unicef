<?php

defined('BASEPATH') || exit('No direct script access allowed');

require APPPATH . 'controllers/Auth.php';


class Contents extends Auth
{

	private $requiredFields = array(
	    'title' => array(
	        Auth::REQUIRED_TEXT => TRUE
	    ),
	    'body' => array(
	        Auth::REQUIRED_TEXT => TRUE,
	    ),
        'colors_id' => array( Auth::TYPE_TEXT => "INT" )
	);

	private $requiredF = array(
	    'title' => array(
	        Auth::REQUIRED_TEXT => TRUE
	    ),
	    'value' => array(
	        Auth::REQUIRED_TEXT => TRUE,
		),
		'content_id' => array(
	        Auth::REQUIRED_TEXT => TRUE,
	    )
	);

	private $singleText = "Contents";
	private $pluralText = "Contents";

	public function __construct(){

		parent::__construct();
		$this->load->Model('ContentsModel');

		$this->localPath = './application/upload/';
		$this->localPathS = 'application/upload/';
		$this->configUpload = array();
		$this->configUpload['upload_path'] = $this->localPath;
		$this->configUpload['allowed_types'] = 'gif|jpg|png';
		$this->configUpload['max_size'] = 3000;

	}

	public function index_get(){

	  //  if( $this->checkAdmin() ){

            $data = $this->input->get(NULL, true);
            if(!isset($data)){
                $data = array();
            }
            $this->__ResponseSuccess($this->pluralText, $this->ContentsModel->getAllWithFields($data));
	 //   }

  }

	public function index_post(){
		// if( $this->checkAdmin() ){
    	    $data = $this->post(null, true);
    	    $this->validateData( $data, $this->requiredFields );
    	    $response = $this->ContentsModel->save($data);
			if(isset($_FILES)){
				$this->custom_fields_files($_FILES, $response->id);
			}
    	    if($response) {
    	        $this->__ResponseSuccess($this->singleText, $response);
    	    }else{
    	        $this->__ResponseError(Auth::SAVE_ERROR);
    	    }
	  // }
	}

	public function updateSlider_post(){
	  //	if( $this->checkAdmin() ){
		   $data = $this->post(null, true);
		   $response = false;
		   $id = $data['id'];
		   unset($data['id']);
		   if(!empty($data))
		   {
			   $response = $this->ContentsModel->update($id, $data); 
			   $response = true;
		   }		  
		   if(!empty($_FILES)){
			   $this->set_custom_fields_files($_FILES, $id);
			   $response = true;
		   }
  	       if($response) {
			   $this->__ResponseSuccess($this->singleText, $response);
	       }else{
	           $this->__ResponseError(Auth::SAVE_ERROR);
   	       } 

		   
	  // }
   }

	private function custom_fields_files($files, $id){

			foreach ($_FILES["custom"]["error"] as $clave => $error) {
			    if ($error == UPLOAD_ERR_OK) {
							$ext = pathinfo($_FILES["custom"]["name"][$clave], PATHINFO_EXTENSION);
			        $nombre_tmp = $_FILES["custom"]["tmp_name"][$clave];
			        $nombre = md5($_FILES["custom"]["tmp_name"][$clave].date("Y-m-d H:i:s")).'.'.$ext;
							$this->ContentsModel->add_fields(array('value' => $this->localPathS.$nombre, 'content_id' => $id, 'custom_fields_id' => 1));
			        move_uploaded_file($nombre_tmp, $this->localPath."/$nombre");
			    }
			}

	}


	private function set_custom_fields_files($files, $id, $custom_value = 1){
		$status = true;
			foreach ($_FILES["custom"]["error"] as $clave => $error) {
			    if ($error == UPLOAD_ERR_OK) {
					$ext = pathinfo($_FILES["custom"]["name"][$clave], PATHINFO_EXTENSION);
			        $nombre_tmp = $_FILES["custom"]["tmp_name"][$clave];
			        $nombre = md5($_FILES["custom"]["tmp_name"][$clave].date("Y-m-d H:i:s")).'.'.$ext;
					$this->ContentsModel->set_fields($clave, array('value' => $this->localPathS.$nombre, 'content_id' => $id, 'custom_fields_id' => $custom_value));
					try {
						move_uploaded_file($nombre_tmp, $this->localPath."/$nombre");
					} catch (Exception $e) {
						die ('File did not upload: ' . $e->getMessage());
					}					
			    }else{
					$status = false;
					break;
				}
			}
	    return $status;
	}

	private function upload_files($files, $id, $custom_value = 1, $type = 'gif|jpg|png')
    {

		$config = $this->configUpload;
		$config['allowed_types'] = $type;

        $this->load->library('upload', $config);

		$result = array('flag' => true, 'message' => '');

		try{
		
        foreach ($_FILES["custom"]["error"] as $key => $image) {

            $_FILES['custom[]']['name']= $_FILES["custom"]["name"][$key];
            $_FILES['custom[]']['type']= $_FILES["custom"]["type"][$key];
            $_FILES['custom[]']['tmp_name']= $_FILES["custom"]["tmp_name"][$key];
            $_FILES['custom[]']['error']= $_FILES["custom"]["error"][$key];
			$_FILES['custom[]']['size']= $_FILES["custom"]["size"][$key];
			
			$ext = pathinfo($_FILES["custom"]["name"][$key], PATHINFO_EXTENSION);
			$nombre = md5($_FILES["custom"]["tmp_name"][$key].date("Y-m-d H:i:s")).'.'.$ext;

            $images[] = $_FILES["custom"]["name"][$key];

            $config['file_name'] = $nombre;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('custom[]')) {
				$this->ContentsModel->set_fields($key, array('value' => $this->localPathS.$nombre, 'content_id' => $id, 'custom_fields_id' => $custom_value));
				$this->upload->data();
				
            } else {
				$result = array('flag' => false, 'message' => $this->upload->display_errors());
            }
		}
		
		}catch(Exception $e){
			echo 'Excepción capturada: ',  $e->getMessage(), "\n";
		}

        return $result;
    }



	public function video_post(){

		$data = $this->post(null, true);	

		$status = true;

		if(isset($_FILES) && !empty($_FILES)){
			//Borrar video anterior si hay
			//$custom = $this->ContentsModel->delete('custom_fields_has_content', array('content_id' => $data['content_id'], 'custom_fields_id' => 3));
			//if($content > 0) {
				$status = $this->upload_files($_FILES, $data['content_id'], 3, 'mp4');

				if( $status['flag'] ){
					$this->__ResponseSuccess($this->singleText, $status['message']);
				}
				else{
					$this->__ResponseError($status['message']);
				}
			// }else{
			// 	$this->__ResponseError($status['message']);
			// }
	
		}else{
			$this->__ResponseError(Auth::UPDATE_ERROR);
		}
		

	}


	public function fields_post(){

		
			$data = $this->post(null, true);	
			$this->validateData( $data, $this->requiredF );

			$status = $this->ContentsModel->add_fields($data);

			if( $status > 0 ){
	            $this->__ResponseSuccess($this->singleText, $status);
	        }
	        else{
	            $this->__ResponseError(Auth::UPDATE_ERROR);
	        }

	}


	public function fields_put(){

		if( $this->checkAdmin() ){

			$status = true;
			$data = $this->put(null, true);
			$id = $data['id'];
			unset($data['id']);

			if(isset($data['type'])){
				unset($data['type']);
			}
			if(isset($data['custom_fields_id'])){
				unset($data['custom_fields_id']);
			}
			
			

			$response = $this->ContentsModel->set_fields($id, $data);



			if( $status  ){
	            $this->__ResponseSuccess($this->singleText, $response);
	        }
	        else{
	            $this->__ResponseError(Auth::UPDATE_ERROR);
	        }
	
		}
	}

	public function index_put(){

	    if( $this->checkAdmin() ){

			$status = true;

			$data = $this->put(null, true);
	        $required['id'] = array( Auth::REQUIRED_TEXT );
	        $this->validateData( $data, $required );

	        $id = $data['id'];
			unset($data['id']);

			if(isset($data['fields'])){
				$fields = $data['fields'];
				unset($data['fields']);
			}			

	        $response = $this->ContentsModel->update($id, $data);

	        if( $status ){  
	            $this->__ResponseSuccess($this->singleText, $response);
	        }
	        else{
	            $this->__ResponseError(Auth::UPDATE_ERROR);
	        }

	    }

	}

	public function order_put(){

		$status = true;

		if( $this->checkAdmin() ){
			$data = $this->put(null, true);
			foreach($data as $key => $item)
			{
				$response = $this->ContentsModel->update((int)$key, array('weight' => (int)$item));
			}
		}

		if( $status ){
			$this->__ResponseSuccess($this->singleText, $response);
		}
		else{
			$this->__ResponseError(Auth::UPDATE_ERROR);
		}

	}

	public function index_delete(){

	    if( $this->checkAdmin() ){
	        $id = $this->delete('id');
	        if( $id ){
				$custom = $this->ContentsModel->delete($id, 'content_id', 'custom_fields_has_content');
				$content = $this->ContentsModel->delete($id, 'id', 'content');
	            if($content > 0) {
							$this->__ResponseSuccess("Success", "success");
					} else{
							$this->__ResponseError(Auth::DELETE_ERROR);
				}
	        }else{
				$id = $this->delete('idCustom');
				$isVideo = $this->delete('video');
				if( $id && $isVideo){
					$this->db->set('action', 'deleted');
					$this->db->where('id', $id);
					$this->db->update('custom_fields_has_content');
					$this->__ResponseSuccess("Success", "success");
				}else{
					$custom = $this->ContentsModel->delete($id, 'id', 'custom_fields_has_content');
					if($custom > 0) {
						$this->__ResponseSuccess("Success", "success");
					} else{
						$this->__ResponseError(Auth::DELETE_ERROR);
					}
				}
			}
	    }

	}

	public function contentType_get(){
		$data = $this->input->get(NULL, true);
		if(!isset($data)){
			$data = array();
		}
		$this->__ResponseSuccess($this->pluralText, $this->ContentsModel->getContentType($data));
	} 

	public function contentType_put(){
		if( $this->checkAdmin() ){

			$status = true;
			$data = $this->put(null, true);
			$id = $data['idContentType'];
			unset($data['idContentType']);
			
			$response = $this->ContentsModel->set_fields_content_type($id, $data);

			if( $status ){
	            $this->__ResponseSuccess($this->singleText, $response);
	        }
	        else{
	            $this->__ResponseError(Auth::UPDATE_ERROR);
	        }
	
		}
	} 

}
