<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/Auth.php';


class Diagnostics extends Auth
{   

	private $requiredFields = array();
	private $requiredStepsFields = array('name' => array(Auth::REQUIRED_TEXT => TRUE));
	private $singleText = "Diagnostic";
	private $pluralText = "Diagnostics";
	private $singleStepText = "Step";
	private $pluralStepText = "Steps";

	function __construct(){
		
		parent::__construct();
		$this->load->Model('DiagnosticsModel');
		$this->load->Model('StepsModel');
		$this->load->Model('AnswerModel');
		$this->load->Model('SectionsModel');
	}

	public function index_post() {

		if($this->checkAdmin()){

			$data = $this->post(null, true);

			$this->validateData( $data, $this->requiredFields );

			$club = $this->DiagnosticsModel->exists($data['clubs_id']);

			if( !$club ){
			    
			    $response = $this->DiagnosticsModel->save($data);
				if($response){
				    $this->__ResponseSuccess($this->singleText, $response);
				}else{
				    $this->__ResponseError(Auth::SAVE_ERROR);
				}
				
			}else{
			    
			    $this->__ResponseSuccess($this->singleText, $club);
			    
			}

		}

	}

	public function index_put(){

		if( $this->checkAdmin() ){

			$data = $this->put(null. true);

			$required = $this->requiredFields;
			$required[] = 'id';
			$this->validateData( $data, $required );

			$id = $data['id'];
			unset($data['id']);

			if(in_array('schoolsstates', $data)){
				$this->DiagnosticsModel->updateSHS($data['schoolsstates'], $id); // School States
			}

			if(in_array('hostedAtlhetesStates', $data)){
				$this->DiagnosticsModel->updateHAS($data['hostedAtlhetesStates'], $id); // Hosted Athletes States
			}

			if(in_array('hostedathletesageranges', $data)){
				$this->DiagnosticsModel->updateHAAR($data['hostedathletesageranges'], $id); // Hosted Athletes Age Ranges
			}

			$response = $this->DiagnosticsModel->update($id, $data); 

			if($response){

				$this->__ResponseSuccess('Indicator', $response);

			}
			else{
				$this->__ResponseError(Auth::UPDATE_ERROR);
			}

		}

	}

	public function index_get(){

		if( $this->checkAdmin() ){
			$id = $this->get('id');
			if( $id ){
				
			    $this->__ResponseSuccess($this->singleText, $this->DiagnosticsModel->get(array('id' => $id)));

			}else{
			    
				$this->__ResponseSuccess($this->pluralText, $this->DiagnosticsModel->get());
				
			}

		}

	}


	// STEPS 

	public function steps_post(){

		if( $this->checkAdmin() ){
	        
	        $data = $this->post(null, true);
	        
	        $this->validateData( $data, $this->requiredStepsFields );
	        $response = $this->StepsModel->save($data);
	        
	        if($response) {
	            $this->__ResponseSuccess($this->singleStepText, $response);
	        }else{
	            $this->__ResponseError(Auth::SAVE_ERROR);
	        }
	        
	    }

	}

	public function steps_get(){
		$user = (object)$this->checkSession();
		
		if( $user->status !== false ){
			
			$id = $this->get('id');
			if( $id ){
				
			    $this->__ResponseSuccess($this->singleStepText, $this->StepsModel->getSteps($user->response, array('id' => $id)));

			}else{
			    
				$this->__ResponseSuccess($this->pluralStepText, $this->StepsModel->getSteps($user->response));
				
			}

		}

	}

	public function graphic_get(){

		$user = (object) $this->checkSession();
		if($user->status !== false){
			$graphicData = array();
			$sections = $this->SectionsModel->getAll();
			array_shift($sections);

			$graphicData[] = array(
				'name' => "Práctica e políticas do clube",
				'results' => $this->AnswerModel->getGeneralResults($user->response)
			);
			foreach($sections as $section){

				$colors = $this->AnswerModel->getColors();
				
				foreach($colors as $color){
					$response = $this->AnswerModel->getSectionsResults($section->id, $user->response, $color->id);

					if( count( $response ) > 0 ){
						$color->quantity = $response->quantity;
					}else{
						$color->quantity = 0;
					}
				}

				$graphicData[] = array(
					'name' => $section->name,
					'results' => $colors
				);
			}
			$this->__ResponseSuccess('data', $graphicData);
		}
	}

	public function graphicSingle_get(){

		$id = $this->get('id');
		if($id){
			$graphicData = array();
			$sections = $this->SectionsModel->getAll();
			array_shift($sections);

			$graphicData[] = array(
				'name' => "Práctica e políticas do clube",
				'results' => $this->AnswerModel->getGeneralResultsSingle($id)
			);
			foreach($sections as $section){

				$colors = $this->AnswerModel->getColors();
				
				foreach($colors as $color){
					$response = $this->AnswerModel->getSectionsResultsSingle($section->id, $id, $color->id);

					if( count( $response ) > 0 ){
						$color->quantity = $response->quantity;
					}else{
						$color->quantity = 0;
					}
				}

				$graphicData[] = array(
					'name' => $section->name,
					'results' => $colors
				);
			}
			$this->__ResponseSuccess('data', $graphicData);
		}
	}

	public function currentStep_get(){
		$user = (object) $this->checkSession();
		$current = new stdClass();
		if($user->status !== false){			
			$current = $this->StepsModel->getCurrentStep($user->response);
			if(is_null($current))
			{			
				$current = new stdClass();
			}
			$current->totalSteps = count($this->StepsModel->getAll());
			$this->__ResponseSuccess($this->singleStepText, $current);
		}
	}

	// SECTIONS

	public function sections_post(){

		if( $this->checkAdmin() ){
	        
	        $data = $this->post(null, true);
	        
	        $this->validateData( $data, $this->requiredsectionsFields );
	        $response = $this->SectionsModel->save($data);
	        
	        if($response) {
	            $this->__ResponseSuccess($this->singleSectionText, $response);
	        }else{
	            $this->__ResponseError(Auth::SAVE_ERROR);
	        }
	        
	    }

	}

	public function sections_get(){
		
		if( $this->checkAdmin() ){
			$id = $this->get('id');
			if( $id ){
				
			    $this->__ResponseSuccess($this->singleSectionText, $this->SectionsModel->get(array('id' => $id)));

			}else{
			    
				$this->__ResponseSuccess($this->pluralSectionText, $this->SectionsModel->get());
				
			}

		}

	}

	// ANSWERS	

	public function saveAnswer_post(){
		
		$user = $this->checkSession();
		if($user['status']){
			$data = (object) $this->input->post();
			
			if($data->type == "checkbox"){
				$i = 0; 
				foreach($data->options as $option){
					$option = (object) $option;
					$i++;
					$this->answerSave($option, $user);
				}
			}else{
				$this->answerSave($data, $user);
			}
			
		}
	}

	public function saveProgress_post(){
		$user = $this->checkSession();
		if( $user['status'] ){

			$data = (object)$this->input->post();
			$step = $this->StepsModel->saveProgress($data->step, $user['response']->sub);
			if($step){
				$this->__ResponseSuccess($this->singleStepText,  $step);
			}

		}
	}
	

	private function answerSave( $data, $user ){
		

		$answerID = array(
			'Users_id' => $user['response']->sub,	
			'Answers_Questions_id' => $data->Questions_id
		);

		if($data->type != "checkbox"){

			$answer = array(
				'Answers_id' => $data->id,
				'Users_id' => $user['response']->sub,
				'Answers_Questions_id' => $data->Questions_id,
				'response' => isset($data->text)?$data->text:$data->value
			);

			$check = $this->AnswerModel->checkResponse($answerID);

			if(!$check){
				$this->AnswerModel->saveResponse( $answer, $answerID );
			}else{
				$this->AnswerModel->updateResponse( $answer, $answerID );
			}
			
		}else{
			$answer = array(
				'Answers_id' => $data->id,
				'Users_id' => $user['response']->sub,
				'Answers_Questions_id' => $data->Questions_id
			);
			$check = $this->AnswerModel->checkResponse($answer);
			
			if( $data->checked===true && !$check ){
				$this->AnswerModel->saveResponse( $answer, $answer );
			}elseif($check && !$data->checked){
				$this->AnswerModel->deleteResponse( $answer );
			}
			
		}
	}

}