<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>Criação bem-sucedida de senha</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
    <meta property="og:title" content="Saucey">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,600,300italic" rel="stylesheet" type="text/css">
 </head>
 <body style="width: 100%;background-color: #ffffff;margin: 0;padding: 0;-webkit-font-smoothing: antialiased;text-align: center;">
    <table width="600" height="100%" border="0" cellspacing="0" cellpadding="10" border="0" align="center">
       <tbody>
          <tr>
             <td bgcolor="#ffffff" align="center" style="padding: 15px 0;">
                <img src="https://joguelimpojoguebem.com.br/assets/images/logo_jogue.png" width="250">
             </td>
          </tr>
          <tr>
             <td align="center">
                <table width="100%" border="0" cellspacing="5" cellpadding="10" align="center" style="max-width:600px;">
                   <tbody>
                      <tr>
                         <td>
                            <div style="Margin-left: 20px; Margin-right: 20px; margin-top:20px">
                               <h1 class="size-36" style="Margin-top: 0;Margin-bottom: 20px;font-style: normal;font-weight: normal;color: #434547;font-size: 30px;line-height: 38px;text-align: center;" lang="x-size-36"><strong><span>“Bem-vindo/a à Plataforma Jogue Limpo Jogue Bem! </span></strong></h1>
                            </div>
                         </td>
                      </tr>
                      <tr>
                         <td style="line-height:150%; font-family:Helvetica; font-size:14px; color:#333333; padding:20px; font-size: 18px;">
                            <center>A senha para acessar a plataforma foi criada com sucesso.</center>
                         </td>
                      </tr>
                      <td style="text-align: center; font-family: arial; font-size: 15px;">Qualquer dúvida ou problema com o seu acesso, envie um e-mail para: <a href="contato@joguelimpojoguebem.com.br" style="text-decoration:none; color:#1764AA;">contato@joguelimpojoguebem.com.br</a>
                         <br> <br>
                      Atenciosamente, </td>
                   </tbody>
                </table>
             </td>
          </tr>
             <td bgcolor="#ffffff">
                <div align="center" style="margin-top: 10px; color:white;  font-size: 12px; margin-bottom: 5px;">  <a style="color:#CCC; text-decoration:none; font-family: 'Lato', sans-serif; border-right:1px solid #CCC; border-left:1px solid #CCC; padding:0 5px; margin: 0 5px;" href="https://joguelimpojoguebem.com.br" target="_blank">Equipe JOGUE LIMPO, JOGUE BEM!</a> <a style="color:#CCC; text-decoration:none; font-family: 'Lato', sans-serif;" href="https://joguelimpojoguebem.com.br" target="_blank"></a></div>
                <div align="center" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:center;display:block;font-family: 'Lato', sans-serif;font-size:10px; color: #AAA; font-weight:300; margin-bottom: 10px;"></div>
             </td>
          </tr>
       </tbody>
    </table>
 </body>
</html>
